#!/usr/bin/env bash

set -eo pipefail

VM_NAME=$1

if [[ -z "$VM_NAME" ]]; then
  echo "Must provide VM name"
  exit 1
fi

REGISTRATION_TOKEN=$2

if [[ -z "$REGISTRATION_TOKEN" ]]; then
  echo "Must provide registration token"
  exit 1
fi

set -u

SCRIPTS_DIR="$(realpath "$(cd "$(dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd)")"

# shellcheck disable=SC1090 # (dont follow, will be checked separately)
source "$SCRIPTS_DIR/script-utils.sh"

### NOTE: This script is not idempotent, and cannot be re-run on an existing instance.
### If the script fails, or has been changed since an instance was created, the best way to proceed is:
### 1. Run the script to create a new instance with a new name
### 2. If the previous instance was registered as a runner, delete the runner in GitLab
### 3. Run `gcloud compute instances delete <INSTANCE_NAME>` to delete the previous instance
### 4. (Optional) Rename the new instance to the use the previous name

export CLOUDSDK_CORE_PROJECT=${DAST_RUNNER_PROJECT:-group-secure-a89fe7}
export CLOUDSDK_COMPUTE_ZONE=${DAST_RUNNER_ZONE:-us-west1-b}


echo "Creating VM ${VM_NAME}..."
gcloud compute instances create "${VM_NAME}" \
    --description="DAST FIPS Runner created by https://gitlab.com/gitlab-org/security-products/dast/-/blob/main/scripts/create-fips-runner.sh" \
    --machine-type=n2-standard-8 \
    --network-interface=network-tier=PREMIUM,subnet=default \
    --maintenance-policy=MIGRATE \
    --provisioning-model=STANDARD \
    --scopes=https://www.googleapis.com/auth/devstorage.read_only,https://www.googleapis.com/auth/logging.write,https://www.googleapis.com/auth/monitoring.write,https://www.googleapis.com/auth/servicecontrol,https://www.googleapis.com/auth/service.management.readonly,https://www.googleapis.com/auth/trace.append \
    --create-disk=auto-delete=yes,boot=yes,device-name="${VM_NAME}",image=projects/rhel-cloud/global/images/rhel-8-v20230306,mode=rw,size=120,type=projects/group-secure-a89fe7/zones/"${CLOUDSDK_COMPUTE_ZONE}"/diskTypes/pd-ssd \
    --no-shielded-secure-boot \
    --shielded-vtpm \
    --shielded-integrity-monitoring \
    --labels=ec-src=vm_add-gcloud \
    --reservation-affinity=any \
    --deletion-protection

# SSH still fails for a few seconds after the instance is created, so retry until we can connect
echo "Connecting to ${VM_NAME}..."
retry "1 minute" "gcloud compute ssh ${VM_NAME} --command 'echo Hello world!'"

# This construct - a multiline string with an embedded heredoc - looks weird, but it has a purpose. The heredoc quotes
# the delimiter ('EOF') which indicates that substitution should not take place within the heredoc. This keeps us from
# having to escape special characters inside the body of the script that will be run on the remote instance, which can
# be error-prone as the script is modified.
# However, there are some variables we need to use in the script, so we do need to be able to substitute those. Doing
# that substitution before the heredoc is the easiest way to accomplish that.
SSH_COMMAND="
export REGISTRATION_TOKEN=${REGISTRATION_TOKEN}
export VM_NAME=${VM_NAME}
$(cat <<-'EOF'
	set -euxo pipefail

	sudo yum install -y yum-utils
	sudo yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
	sudo yum install -y docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
	sudo systemctl start docker
	sudo docker run --rm hello-world
	sudo systemctl enable docker.service
	sudo systemctl enable containerd.service

	# Kind of shocked that the recommended install method from the docs is 'curl | sudo bash', but...
	# https://docs.gitlab.com/runner/install/linux-repository.html#installing-gitlab-runner
	curl --fail -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.rpm.sh" | sudo bash
	sudo dnf install -y gitlab-runner-fips

	sudo gitlab-runner register \
	--non-interactive \
	--url "https://gitlab.com/" \
	--registration-token "${REGISTRATION_TOKEN}" \
	--executor "docker" \
	--docker-image alpine:3.17.3 \
	--docker-privileged \
	--description "DAST FIPS Runner (${VM_NAME})" \
	--tag-list "dast-fips" \
	--run-untagged="false" \
	--locked="true" \
	;

    {
        echo "#!/bin/sh"
        echo "/usr/share/gitlab-runner/clear-docker-cache --prune-volumes"
        echo "docker system prune --force -a --filter \"until=24h\""
    } > gitlab_runner_clear_cache
    sudo mv gitlab_runner_clear_cache /etc/cron.daily/gitlab_runner_clear_cache
    sudo chmod a+rx /etc/cron.daily/gitlab_runner_clear_cache
EOF
)"

gcloud compute ssh "${VM_NAME}" --command "${SSH_COMMAND}"
