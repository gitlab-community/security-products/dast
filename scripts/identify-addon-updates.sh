#!/usr/bin/env bash

set -eu

PROJECT_DIR="$(realpath "$(cd "$(dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd)/..")"
BUILT_IMAGE=${BUILT_IMAGE:-dast}

# shellcheck disable=SC1090 # (dont follow, will be checked separately)
source "$PROJECT_DIR/scripts/script-utils.sh"

zap_addons_that_require_update() {
  docker run --rm -v "${PWD}":/zap/wrk "$BUILT_IMAGE" \
    /analyze --write-addons-to-update-file true >/dev/null

  if [ -s addons.json ]; then
    echo "Updates required"
    invoke dast.parse-addon-json
  else
    echo "No updates necessary."
  fi
}


log "Checking for updates"
zap_addons_that_require_update
