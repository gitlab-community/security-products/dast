# GitLab DAST changelog

## v4.0.31
- Don't run the browserker availability check (!862)
- Support missing environment variable `DAST_AUTH_USERNAME` (!864)
- Upgrade browserker to `5.8.0` (!861)
  - Add configuration to advertise scan in TOML config [browserker!1361](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1361)
  - Checks can be excluded using `DAST_CHECKS_TO_EXCLUDE` [browserker!1355](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1355)
- Upgrade browserker to `5.7.0` (!861)
  - Add target hostname to allowed hosts [browserker!1357](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1357)
  - Add an `/analyze` script for backwards compatibility with the DAST.gitlab-ci.yml template [browserker!1358](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1358)
  - Create `/zap/wrk` directory for backwards compatibility with the DAST.gitlab-ci.yml template [browserker!1358](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1358)
  - Fix value of `DAST_AUTH_DISABLE_CLEAR_FIELDS` in the auth report [browserker!1360](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1360)
- Upgrade browserker to `5.6.0` (!861)
  - Add default value for file log path [browserker!1354](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1354)
  - Add default value for number of browsers setting [browserker!1353](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1353)
  - Add default value for secure report setting [browserker!1349](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1349) [browserker!1352](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1352)
  - Equip the crawler to find new navigations from a sitemap [browserker!1332](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1332)
  - Add default value for data path setting [browserker!1346](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1346) [browserker!1352](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1352)
  - Run availability check by default [browserker!1350](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1350)
  - Update environment variables to configure appropriate timeout values [browserker!1347](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1347)
  - Default browser dimensions to 1300x700 [browserker!1348](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1348)
  - Default maximum actions to crawl to 10,000 [browserker!1348](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1348)
  - Default vulnerability definitions directory to be where they're located in the Docker image [browserker!1348](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1348)
- Upgrade browserker to `5.5.0` (!861)
  - Add support for more environment variables [browserker!1339](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1339,!1331)
  - Add configuration option to advertise a scan to the target [browserker!1310](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1310) [browserker!1329](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1329)
  - Fix issue causing panic with JSON value injection [browserker!1336](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1336)
    - Adds support for more generic JSON bodies like arrays

## v4.0.30
- Upgrade browserker to `5.4.0` (!859)
  - Fix issue where copying navigations does not copy cookies [browserker!1325](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1325)
  - Optimize navigation paths when crawling [browserker!1328](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1328)
  - Initialize browser with local/session storage only on first page navigation [browserker!1328](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1328)
  - Only support checks that are verified to work as expected [browserker!1320](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1320)
  - Add support for more environment variables [browserker!1318](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1318) [browserker!1323](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1323)
  - Log why scope excludes URLs [browserker!1330](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1330)
  - Reduce locking between browsers by using atomic variables instead of mutexes [browserker!1319](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1319)
  - Record local/session storage items in the browser at the end of each navigation [browserker!1317](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1317)
  - Optimize memory usage when building scanned resources in JSON report [browserker!1322](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1322)
  - Add configuration option to advertise a scan to the target [browserker!1310](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1310)
  - Prioritize environment variable settings over TOML [browserker!1327](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1327)

## v4.0.29
- Upgrade browserker to `5.3.0` (!858)
  - Add support for more environment variables [browserker!1308](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1308) [browserker!1309](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1309)
  - Cache DevTools requests for increased performance [browserker!1304](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1304)
  - Fix panic in case availability check finishes unsuccessfully [browserker!1306](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1306)
  - Upgrade dast-chromium to upgrade base image to UBI 8.9 and Chromium to v122 [browserker!1311](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1311)
    - version `22.04-122.0.6261.94-1-20240304220807` for regular image
    - version `8.9-122.0.6261.69-1-20240304220807` for FIPS image
  - Optimize memory usage when printing URLs visited to log [browserker!1316](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1316)
- Don't propagate CI/CD variables to the browserker process (!858)
- Remove memory profiling (!858)

## v4.0.28
- Upgrade browserker to `1.0.27` (!856)
  - Limit the number of goroutines created by the crawler [browserker!1289](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1289)
  - Upgrade GCD to version `2.3.3` for improved JSON performance [browserker!1287](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1287)
  - Add support for configuring the target URL using environment variables [browserker!1286](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1286)
  - Add support for more environment variables [browserker!1291](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1291)
- Upgrade browserker to `5.0.0` (!856)
  - Update major version to release as `dast:5.0.0` [browserker!1298](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1298)
  - Upgrade Go to 1.21 [browserker!1292](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1292)
  - Add support for more environment variables [browserker!1296](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1296),[browserker!1300](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1300)
- Upgrade browserker to `5.1.0` (!856)
  - Restore cookies while optimizing crawl path [browserker!1294](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1294)
  - Stop releasing using `latest` tag [browserker!1303](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1303)
  - Add support for more environment variables [browserker!1302](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1302)
- Upgrade browserker to `5.2.0` (!856)
  - Find interactables by checking for event listeners [browserker!1297](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1297)
  - TOML configuration overrides environment configuration [browserker!1307](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1307)

## v4.0.27
- Resume publishing dast:4 as dast:latest (!855)

## v4.0.26
- Upgrade ZAP to [2.14.0](https://www.zaproxy.org/docs/desktop/releases/2.12.0/) (!851)
- Upgrade ZAP add-on `accessControl` to [9](https://github.com/zaproxy/zap-extensions/releases/accessControl-v9) (!851)
- Upgrade ZAP add-on `alertFilters` to [19](https://github.com/zaproxy/zap-extensions/releases/alertFilters-v19) (!851)
- Upgrade ZAP add-on `ascanrules` to [63](https://github.com/zaproxy/zap-extensions/releases/ascanrules-v63) (!851)
- Upgrade ZAP add-on `ascanrulesBeta` to [50](https://github.com/zaproxy/zap-extensions/releases/ascanrulesBeta-v50) (!851)
- Upgrade ZAP add-on `automation` to [0.35.0](https://github.com/zaproxy/zap-extensions/releases/automation-v0.35.0) (!851)
- Upgrade ZAP add-on `bruteforce` to [15](https://github.com/zaproxy/zap-extensions/releases/bruteforce-v15) (!851)
- Upgrade ZAP add-on `callhome` to [0.10.0](https://github.com/zaproxy/zap-extensions/releases/callhome-v0.10.0) (!851)
- Upgrade ZAP add-on `commonlib` to [1.22.0](https://github.com/zaproxy/zap-extensions/releases/commonlib-v1.22.0) (!851)
- Upgrade ZAP add-on `diff` to [14](https://github.com/zaproxy/zap-extensions/releases/diff-v14) (!851)
- Upgrade ZAP add-on `directorylistv1` to [7](https://github.com/zaproxy/zap-extensions/releases/directorylistv1-v7) (!851)
- Upgrade ZAP add-on `domxss` to [18](https://github.com/zaproxy/zap-extensions/releases/domxss-v18) (!851)
- Upgrade ZAP add-on `encoder` to [1.4.0](https://github.com/zaproxy/zap-extensions/releases/encoder-v1.4.0) (!851)
- Upgrade ZAP add-on `exim` to [0.8.0](https://github.com/zaproxy/zap-extensions/releases/exim-v0.8.0) (!851)
- Upgrade ZAP add-on `formhandler` to [6.5.0](https://github.com/zaproxy/zap-extensions/releases/formhandler-v6.5.0) (!851)
- Upgrade ZAP add-on `fuzz` to [13.12.0](https://github.com/zaproxy/zap-extensions/releases/fuzz-v13.12.0) (!851)
- Upgrade ZAP add-on `fuzzdb` to [9](https://github.com/zaproxy/zap-extensions/releases/fuzzdb-v9) (!851)
- Upgrade ZAP add-on `gettingStarted` to [16](https://github.com/zaproxy/zap-extensions/releases/gettingStarted-v16) (!851)
- Upgrade ZAP add-on `graaljs` to [0.5.0](https://github.com/zaproxy/zap-extensions/releases/graaljs-v0.5.0) (!851)
- Upgrade ZAP add-on `graphql` to [0.22.0](https://github.com/zaproxy/zap-extensions/releases/graphql-v0.22.0) (!851)
- Upgrade ZAP add-on `help` to [17](https://github.com/zaproxy/zap-core-help/releases/help-v17) (!851)
- Upgrade ZAP add-on `hud` to [0.18.0](https://github.com/zaproxy/zap-hud/releases/v0.18.0) (!851)
- Upgrade ZAP add-on `invoke` to [14](https://github.com/zaproxy/zap-extensions/releases/invoke-v14) (!851)
- Upgrade ZAP add-on `network` to [0.13.0](https://github.com/zaproxy/zap-extensions/releases/network-v0.13.0) (!851)
- Upgrade ZAP add-on `oast` to [0.17.0](https://github.com/zaproxy/zap-extensions/releases/oast-v0.17.0) (!851)
- Upgrade ZAP add-on `onlineMenu` to [12](https://github.com/zaproxy/zap-extensions/releases/onlineMenu-v12) (!851)
- Upgrade ZAP add-on `openapi` to [39](https://github.com/zaproxy/zap-extensions/releases/openapi-v39) (!851)
- Upgrade ZAP add-on `plugnhack` to [13](https://github.com/zaproxy/zap-extensions/releases/plugnhack-v13) (!851)
- Upgrade ZAP add-on `portscan` to [10](https://github.com/zaproxy/zap-extensions/releases/portscan-v10) (!851)
- Upgrade ZAP add-on `pscanrules` to [55](https://github.com/zaproxy/zap-extensions/releases/pscanrules-v55) (!851)
- Upgrade ZAP add-on `pscanrulesBeta` to [37](https://github.com/zaproxy/zap-extensions/releases/pscanrulesBeta-v37) (!851)
- Upgrade ZAP add-on `quickstart` to [43](https://github.com/zaproxy/zap-extensions/releases/quickstart-v43) (!851)
- Upgrade ZAP add-on `replacer` to [16](https://github.com/zaproxy/zap-extensions/releases/replacer-v16) (!851)
- Upgrade ZAP add-on `reports` to [0.29.0](https://github.com/zaproxy/zap-extensions/releases/reports-v0.29.0) (!851)
- Upgrade ZAP add-on `retest` to [0.8.0](https://github.com/zaproxy/zap-extensions/releases/retest-v0.8.0) (!851)
- Upgrade ZAP add-on `retire` to [0.31.0](https://github.com/zaproxy/zap-extensions/releases/retire-v0.31.0) (!851)
- Upgrade ZAP add-on `reveal` to [7](https://github.com/zaproxy/zap-extensions/releases/reveal-v7) (!851)
- Upgrade ZAP add-on `scripts` to [45.0.0](https://github.com/zaproxy/zap-extensions/releases/scripts-v45.0.0) (!851)
- Upgrade ZAP add-on `selenium` to [15.18.0](https://github.com/zaproxy/zap-extensions/releases/selenium-v15.18.0) (!851)
- Upgrade ZAP add-on `soap` to [21](https://github.com/zaproxy/zap-extensions/releases/soap-v21) (!851)
- Upgrade ZAP add-on `spiderAjax` to [23.18.0](https://github.com/zaproxy/zap-extensions/releases/spiderAjax-v23.18.0) (!851)
- Upgrade ZAP add-on `tips` to [12](https://github.com/zaproxy/zap-extensions/releases/tips-v12) (!851)
- Upgrade ZAP add-on `webdriverlinux` to [67](https://github.com/zaproxy/zap-extensions/releases/webdriverlinux-v67) (!851)
- Upgrade ZAP add-on `websocket` to [30](https://github.com/zaproxy/zap-extensions/releases/websocket-v30) (!851)
- Upgrade ZAP add-on `zest` to [43](https://github.com/zaproxy/zap-extensions/releases/zest-v43) (!851)
- Add aliases to CI/CD names to support variables renamed in the next major DAST release (!850) (!852)
- Stop releasing DAST image with `latest` tag and `5.0.0-alpha` tag (!853)

## v4.0.25
- Upgrade browserker to `1.0.27` (!848)
  - Wait for the target application to be available before running the scan [browserker!1278](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1278)
  - Optimize the crawl path optimization algorithm [browserker!1284](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1284)

## v4.0.24
- Upgrade browserker to `1.0.24` (!846)
  - Fix issue where DAST crashes when Chromium crashes before logger has been initialized [browserker!1269](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1269)
  - Optimize crawl graph to remove redundant load requests [browserker!1272](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1272)
- Upgrade browserker to `1.0.25` (!847)
  - Fetch response bodies for requests cached by Chromium from the database [browserker!1266](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1266)
  - Fix crawling in parallel [browserker!1267](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1267)
- Upgrade browserker to `1.0.26` (!847)
  - Fix issue where crawler does not crawl the optimized path [browserker!1283](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1283)

## v4.0.23
- Upgrade browserker to `1.0.23` (!845)
  - Enable squid proxy for standard image ([browserker!1253](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1253))
  - Upgrade to dast-chromium v0.0.14 ([browserker!1261](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1261))
    - Add squid proxy to regular DAST image ([dast-chromium!63](https://gitlab.com/gitlab-org/security-products/dast-chromium/-/merge_requests/63))
    - Upgrade Chromium to 120.0.6099.109-1 for standard, 119.0.6045.159-1 for FIPS ([dast-chromium!64](https://gitlab.com/gitlab-org/security-products/dast-chromium/-/merge_requests/64))
  - Active check attacks can upsert request header ([browserker!1250](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1250))
  - Add multipart form file data injection location ([browserker!1239](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1239))
  - Log badger database log entries at TRACE level to remove noise from debug logs ([browserker!1255](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1255))
  - Handle zero results from XPath selector ([browserker!1259](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1259))


## v4.0.22
- Upgrade browserker to `1.0.22` (!843)
  - Upgrade vulnerability checks to version `1.0.62` [browserker!1251](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1251)
    - Update `16.11` to use `upsert_header` [dast-cwe-checks!237](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/237)
    - Update `917.1` to indicate `target_tech` [dast-cwe-checks!229](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/229)
  - Active check attacks can upsert request header [browserker!1250](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1250)
  - Detect login after executing post-login actions [browserker!1233](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1233)
- Enable browser-based active check `1336.1` Server-Side Template Injection and disable ZAP check `90035` (!841)
- Upgrade browserker to `1.0.21` (!842)
  - Modify request operations modify the attack request prior to injecting the attack payload [browserker!1245](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1245)

## v4.0.20
- Enable browser-based active check `918.1` Server Side Request Forgery and disable ZAP check `40046` (!840)
- Enable browser-based active check `98.1` PHP Remote File Inclusion and disable ZAP check `7` (!839)

## v4.0.19
- Enable browser-based active check `89.1` SQL Injection and disable ZAP checks `40018`, `40019`, `40020`, `40021`, `40022`, `40024`, `40027`, `40033`, and `90018` (!836)
- Enable browser-based active check `1336.1` Server-Side Template Injection as an alpha attack (!837)
- Upgrade browserker to `1.0.20` (!837)
  - Implement active check `1336.1` Server-Side Template Injection [browserker!1229](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1229)
  - Parse `modify_http_request` operations [browserker!1241](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1241)

## v4.0.18
- Enable browser-based active check `98.1` PHP Remote File Inclusion as an alpha attack (!835)
- Enable browser-based active check `917.1` and disable ZAP check `90025` (!832)
- Upgrade browserker to `1.0.19` (!834)
  - Implement active check `98.1` Improper control of filename [browserker!1231](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1231)
  - Upgrade vulnerability checks to version `1.0.61` [browserker!1238](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1238)
    - Update `552` checks  to use `modify_http_request` and only attack GET requests [dast-cwe-checks!228](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/228)
    - Add `http_request_method` attack requirement [dast-cwe-checks!228](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/228)
    - Add `requirements` to the schema that determines when attacks should run [dast-cwe-checks!228](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/228)
    - Add `update_path_filename` request modification to the schema [dast-cwe-checks!228](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/228)
    - Add `modify_http_request` to the schema [dast-cwe-checks!227](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/227)
    - Add `update_method` and `add_header` request modifications to the schema [dast-cwe-checks!227](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/227)
    - Remove requirement for `payloads` and `injections` to be present in `match_response_attack` [dast-cwe-checks!227](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/227)
    - Remove requirement for `injection_locations_policy` to be present in `attack` [dast-cwe-checks!227](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/227)
    - Update `16.11` to use `modify_http_request` [dast-cwe-checks!227](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/227)
    - Update `1336.1` to use injection template without affixes for polyglot attack [dast-cwe-checks!230](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/230)
    - Remove redundant affixes property for attack definitions in the schema [dast-cwe-checks!232](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/232)
    - Update `89.1` and `1336.1` to align with new schema [dast-cwe-checks!232](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/232)
    - Update schema to require `affixes` if `prefix` or `suffix` is used in an injection template [dast-cwe-checks!226](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/226)

## v4.0.17
- Enable browser-based active check `918.1` Server-Side Request Forgery as an alpha attack (!831)
- Upgrade browserker to `1.0.18` (!831)
  - Implement active check `918.1` Server-Side Request Forgery [browserker!1230](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1230)
  - Don't clean URL paths when finding request path injection locations [browserker!1230](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1230)
- Add `Via: GitLab DAST` header to target availability probe (!825)
- Upgrade browserker to `1.0.17` (!823)
  - Enable active check 89.1 SQL Injection [browserker!1226](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1226)
- Enable browser-based active check `89.1 SQL Injection` as an alpha attack (!823)
- Upgrade browserker to `1.0.16` (!824)
  - Enable active check `917.1` Expression Language Injection [browserker!1218](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1218)
  - Upgrade vulnerability checks to version `1.0.60` [browserker!1223](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1223)
    - Update `89.1` and `917.1` check descriptions and remediations [dast-cwe-checks!224](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/224)
    - Add more payloads to `917.1` to cover more situations [dast-cwe-checks!225](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/225)

## v4.0.16
- Enable browser-based active check `74.1` and disable ZAP check `90017` (!822)
- Enable browser-based active check `917.1` as an alpha attack (!818)
- Re-enabled the ZAP AJAX Spider (`DAST_USE_AJAX_SPIDER`) after it was accidentally broken (!819)

## v4.0.15
- Upgrade browserker to `1.0.15` (!817)
  - Upgrade dast-chromium to upgrade base image to UBI 8.8 and Chromium to v118 [browserker!1221](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1221)
    - version 22.04-118.0.5993.70-1-20231024135604 for regular image
    - version 8.8-118.0.5993.70-1-20231024135604 for FIPS image
  - Upgrade vulnerability checks to version 1.0.59 [browserker!1220](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1220)
    - Update `611.1` description to use backticks around libraries and features [dast-cwe-checks!218](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/218)
    - Add `16.11` active check for enabled HTTP TRACE method [dast-cwe-checks!179](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/179)
    - Add `98.1` active check for PHP Remote File Inclusion [dast-cwe-checks!217](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/217)
    - Update `1336.1` active check to remove duplicate attack IDs [dast-cwe-checks!222](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/222)
    - Update `917.1` to fix broken affixes [dast-cwe-checks!223](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/223)

## v4.0.14
- Upgrade browserker to `1.0.14` (!807)
  - Add AfterLoginActions to support actions to be taken after logging in [browserker!1208](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1208)
- Add `DAST_AFTER_LOGIN_ACTIONS` to perform actions after a user has logged in (!807)
- Remove unused `DAST_AUTH_AUTO` variable and `--auth-auto` argument (!812)

## v4.0.13
- Enable browser-based active check `94.3` and disable ZAP check `90019` (!813)
- Enable browser-based active check `94.1` and disable ZAP check `90019` (!814)
- Enable browser-based active check `943.1` and disable ZAP check `90019` (!815)

## v4.0.12
- Enable browser-based active check `94.2` and disable ZAP check `90019` (!810)
- Upgrade browserker to `1.0.13` (!811)
  - Upgrade vulnerability checks to version `1.0.58` [browserker!1200](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1200)
    - Add 1336.1 active check for Server-Side Template Injection [dast-cwe-checks!214](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/214)
    - Add aggregate_by to 829.1 passive check to allow it to produce multiple aggregated vulnerabilities [dast-cwe-checks!212](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/204)(!212)
    - Set minLength to 1 for summary and unique_by template properties [dast-cwe-checks!212](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/212)
    - Add 918.1 active check for Server-Side Request Forgery [dast-cwe-checks!213](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/213)
    - Add 917.1 active check for Expression Language injection [dast-cwe-checks!153](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/153)
  - Ignore after-action stability timeout starting in next major version [browserker!1207](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1207)
  - Aggregate findings by specified key [browserker!1185](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1185)

## v4.0.11
- Enable browser-based active check `78.1` and disable ZAP check `90020` (!809)

## v4.0.10
- Enable browser-based active check `94.4` and disable ZAP check `90019` for NodeJS (!806)
- Enable browser-based active check `611.1` and disable ZAP check `90023` (!805)
- Enable browser-based active check `113.1` and disable ZAP check `40003` (!804)
- Upgrade browserker to `1.0.12` (!808)
  - Add non-sensitive authorization configuration to the Authorization Report [browserker!1197](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1197)
  - Timing attack HTTP requests run one at a time [browserker!1202](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1202)
  - Timing requests measure time taken around the HTTP request for increased accuracy [browserker!1203](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1203)
  - Wait for the longer of `WaitAfterAction` and `WaitAfterNavigation` when performing navigation actions that do not result in page transition [browserker!1201](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1201)

## v4.0.9
- Upgrade browserker to `1.0.11` (!803)
  - Unescape HTML on active check HTTP response bodies [browserker!1194](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1194)
- Checks listed `DAST_EXCLUDE_RULES` are excluded in browser-based scans (!803)
- Enable browser-based active check `22.1` and disable ZAP check `6` (!803)

## v4.0.8
- Upgrade Chromium to version 116 (!799)
- Upgrade ZAP WebDriver add-on to [v59](https://github.com/zaproxy/zap-extensions/releases/tag/webdriverlinux-v59) (!799)
- Upgrade browserker to `1.0.10` (!799)
  - Fix sending of additional headers to out-of-scope hosts [browserker!1192](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1192)
  - Exclude session and authorization cookies from injection locations [browserker!1190](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1190)
  - Upgrade dast-chromium to version `22.04-116.0.5845.110-1-20230830075754` and `8.7-116.0.5845.96-1-20230830075754` for standard and FIPS images respectively [browserker!1187](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1187)
  - Don't attack the header used by DAST to advertize the scan [browserker!1182](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1182)
  - Use scope to determine if custom headers should be added to an HTTP request [browserker!1182](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1182)
  - Add cookie value injection location [browserker!1189](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1189)
- Upgrade ZAP add-on `GraphQL Support` to [0.17.0](https://github.com/zaproxy/zap-extensions/releases/graphql-v0.17.0) (!801)

## v4.0.7
- Log the output from browserker process to help diagnose scan failures (!795)
- Upgrade browserker version to `1.0.9` (!796)
  - Log active check progress [browserker!1179](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1179)
  - Page stability doesn't wait for failed requests [browserker!1177](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1177)
  - Use a worker pool to crawl the target application [browserker!1168](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1168)
  - Upgrade to Go 1.20 [browserker!1172](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1172)
  - Crawl jobs don't run when the context is complete [browserker!1176](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1176)

## v4.0.6
- Upgrade browserker version to `1.0.8` (!790)
  - Refresh the page bounding box more often to reduce the likelihood visible elements are skipped during crawling [browserker!1166](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1166)
  - Run passive checks in the background to prevent locking [browserker!1160](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1160)
  - Support GZIP responses in active checks by removing captured `Accept-Encoding` header [browserker!1154](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1154)
  - Add scan mode none that finishes the scan after authenticating [browserker!1159](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1159)
  - Remove the debugging crawl report [browserker!1158](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1158)
- Add `DAST_BROWSER_SCAN_MODE` to allow DAST engineers to override scan mode when debugging scans (!790)

## v4.0.5
- Upgrade browserker version to `1.0.7` (!787)
  - Replace zerolog with the standard logger for concurrency safety [browserker!1155](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1155)

## v4.0.4
- Upgrade the DAST JSON report to schema 15.0.6 (!786)
- Upgrade browserker version to `1.0.6` (!786)
  - Upgrade the DAST JSON report to schema 15.0.6 [browserker!1152](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1152)
  - Idle for a short period prior to checking if the page has transitioned [browserker!1150](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1150)

## v4.0.3
- Export scan options in the `gl-dast-report.json` report (!783)
- Support Mutual TLS when run in FIPS mode (!767)
- Upgrade browserker version to `1.0.4` (!785)
  - Click on centre of the overlapping region of an element and the page [browserker!1138](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1138)
- Upgrade browserker version to `1.0.5` (!785)
  - Rectangles with zero width and height can overlap other rectangles [browserker!1143](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1143)
  - Exclude HTTP/2 pseudo-headers when parsing headers [browserker!1146](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1146)

## v4.0.2
- Upgrade browserker version to `1.0.3` (!781)
  - Upgrade dast-chromium to version `22.04-112.0.5615.138-1-20230525093447` [browserker!1133](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1133)
  - Support modern ciphers in Mutual TLS PKCS12 files [browserker!1135](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1135)
  - Don't passive check Squid forward-proxy error pages [browserker!1130](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1130)
  - Log all requests that Squid cannot forward [browserker!1125](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1125)
  - Handle errors when the Squid forward-proxy errors [browserker!1119](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1119)

## v4.0.1
- Configure Browser-based DAST in FIPS mode when DAST is in FIPS mode (!779)
- Wait for proxy service to start (!780)
- Add tests for new --fips-mode argument (!776)
- Verify Squid starts in DAST FIPS image (!775)
- Upgrade browserker version to `1.0.2` (!779)
  - Log if the browser-based scanner is in FIPS mode [browserker!1115](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1115)
  - Log when page loading selector causes page to be not ready [browserker!1116](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1116)
- Upgrade browserker version to `1.0.1` (!778)
  - Update dast-chromium base image version to v0.9 [browserker!1117](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1117)
  - Enable DAST image ability to add entrypoint logic [browserker!1114](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1114)
- Use Browser-based DAST as scanner name and remove ZAP for FIPS mode (!765)

## v4.0.0
- Upgrade major version to remove deprecated functionality (!774)
- Upgrade browserker to version `1.0.0` (!774)
  - Upgrade major version to remove deprecated functionality [browserker!1112](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1112)

## v3.0.71
- Append the -bas suffix to the scanner ID when `DAST_FF_ENABLE_BAS_ATTACKS` is enabled (!751)
- Enable breach attack simulation attacks with feature flag `DAST_FF_ENABLE_BAS_ATTACKS` (!746)
- Upgrade browserker to version `0.0.148` (!749)
  - Add active check finding summary as `vulnerabilities[].location.param` so findings are not de-duplicated [browserker!1083](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1083)
  - Release browserker FIPS compliant Docker image [browserker!1080](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1080) [browserker!1073](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1073)
  - Update dast-chromium to `22.04-108.0.5359.94-1-20230321081952` [browserker!1080](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1080)
  - Ensure active check findings are not deduplicated [browserker!1082](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1082)
  - Attack IDs can be specified in `OnlyIncludeAttacks` [browserker!1088](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1088)
- Upgrade browserker to version `0.0.149` (!752)
  - Move fips image from `ubi` to `ubi-minimal` [browserker!1092](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1092)
- Upgrade browserker to version `0.0.150` (!755)
  - Enable FIPS mode on the fips image [browserker!1093](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1093)
- Revert removal of `DAST_ZAP_CLI_OPTIONS` and `DAST_ZAP_LOG_CONFIGURATION` environment variables in DAST version 4 (!764)
- Upgrade browserker to version `0.0.151` (!768)
  - Upgrade dast-chromium to version `22.04-112.0.5615.138-1-20230502163251` [browserker!1110](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1110)
  - Upgrade FIPS dast-chromium to version `8.7-112.0.5615.165-1-20230502163251` [browserker!1110](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1110)
  - Log version of Chromium used [browserker!1109](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1109)
  - Fix concurrency issues when using the global logger [browserker!1107](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1107)
  - Use `nosemgrep` code comments for suppressing false positive SAST findings [browserker!1103](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1103)
  - Hide Squid proxy startup warnings [browserker!1100](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1100)
  - Display banner and if FIPS has been enabled in FIPS image [browserker!1101](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1101)
  - Optimize clearing of login fields [browserker!1099](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1099)
  - Remove requirement for selectors to have a type from version `1.0.0`  [browserker!1105](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1105)
- Upgrade ZAP add-on `Linux WebDrivers` to [54](https://github.com/zaproxy/zap-extensions/releases/webdriverlinux-v54) (!768)
- Upgrade ChromeDriver to version `112.0.5615.49` (!768)

## v3.0.70
- Upgrade browserker to version `0.0.147` (!744)
  - Replace usages of MD5 with SHA1 [browserker!1077](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1077)
  - Replace `MaxCrawlMinutes` with `CrawlTimeout` [browserker!1072](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1072)
  - Log when a crawl times out [browserker!1063](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1063)
  - Log when a page is assumed excluded from scope [browserker!1063](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1063)
  - Page stability doesn't wait for excluded document type requests [browserker!1063](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1063)
  - Suppress errors when clicking on an excluded URL [browserker!1063](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1063)
- Configure browser-based crawl timeout using `DAST_BROWSER_CRAWL_TIMEOUT` (!744)

## v3.0.69
- Upgrade browserker to version `0.0.145` (!741)
  - Requests excluded from scope do not prevent pages from being considered stable [browserker!1061](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1061)
  - Log more statistics on completion of scans when log module `STAT` is on `DEBUG` level [browserker!1059](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1059)
  - Improve time taken to detect page stability for pages where the DOM does not keep updating [browserker!1060](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1060)
  - Verify page is stable after a page transition [browserker!1060](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1060)
  - Log more information when waiting for a page to transition and while checking page stability [browserker!1060](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1060)
  - Save and passively check failed navigation entries [browserker!1060](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1060)
  - Use mutexes instead of atomic values for safer parallelism [browserker!1062](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1062)
  - Increase default DOM ready timeout to 500ms [browserker!1062](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1062)
- Configure browser-based DOM ready timeout using `DAST_BROWSER_DOM_READY_AFTER_TIMEOUT` (!741)
- Upgrade browserker to version `0.0.146` (!741)
  - Optimize regular expressions matching by caching previous results [browserker!1069](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1069)
  - Don't run passive checks when scan mode is set to crawl [browserker!1067](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1067)
  - Synchronize frame loading events to help understand if a page is transitioning [browserker!1065](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1065)
  - Passive checks can be run in parallel using configuration `PassiveCheckWorkers` [browserker!1071](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1071)
  - Passive checks can be run in during crawl and active checks with experimental feature flag `PassiveScanInBackground` [browserker!1071](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1071)
  - Copy checks when running attacks instead of holding locks for long periods of time [browserker!1066](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1066)
  - Log pending request IDs when waiting for page to be stable [browserker!1068](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1068)
- Configure number of browser-based parallel passive check workers using `DAST_BROWSER_PASSIVE_CHECK_WORKERS` (!741)

## v3.0.68
- Upgrade browserker to version `0.0.144` (!740)
  - Log memory usage only when `STAT` is logged on `TRACE` level [browserker!1056](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1056)
  - Log a summary at the end of the scan showing time taken for each major scan task [browserker!1057](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1057)
  - Log more statistics to help diagnose slow scans [browserker!1057](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1057)
  - Stop reporting low-value memory metric `mem_total_alloc` [browserker!1058](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1058)

## v3.0.67
- Upgrade browserker to version `0.0.143` (!738)
  - Fix inconsistent references to the basic-digest auth type [browserker!1053](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1053)
  - Log when active checks aren't running the highest priority attacks first [browserker!1046](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1046)
  - Save navigation results using batch writes to avoid large transaction errors [browserker!1051](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1051)
  - The database memory table size is configurable [browserker!1052](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1052)
- Configure browser-based database memory table size using `DAST_BROWSER_DB_MEMORY_TABLE_SIZE` (!738)

## v3.0.66
- Add support for configuring the authentication type `DAST_AUTH_TYPE` (!736)

## v3.0.65
- Upgrade browserker to version `0.0.142` (!733)
  - Run active checks in parallel, run attacks sequentially [browserker!1043](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1043)
  - Add multipart form field name injection location [browserker!1041](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1041)

## v3.0.64
- Upgrade browserker to version `0.0.141` (!732)
  - Improve active check logs to aid debugging [browserker!1036](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1036)
  - Fix rounding error causing clicks to click outside the intended HTML element [browserker!1038](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1038)
- Enable browser-based attacks with feature flag `DAST_FF_ENABLE_BROWSER_BASED_ATTACKS` (!721)
- Add mutual TLS for browser-based active scans (!729)
- Upgrade browserker to version `0.0.140` (!730)
  - Add support for `ClientCertificate` and `ClientCertificatePassword` in the TOML configuration for active check mutual TLS [browserker!1033](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1033)
  - Release Browserker as a multi-architecture Docker image supporting `linux/arm64` and `linux/amd64` [browserker!1028](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1028)
  - Upgrade vulnerability checks to version `1.0.55` [browserker!1023](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1023)
    - Add `request_body` as injection location in `74.1` [dast-cwe-checks!203](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/203)
    - Remove duplicate injection location in `611.1` [dast-cwe-checks!203](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/203)
    - Update capitalization of `URL` in `22.1` description [dast-cwe-checks!202](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/202)
    - Update YAML to address markdown linting issues [dast-cwe-checks!201](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/201)

## v3.0.63
- Aggregate findings found by [CORS Header](https://www.zaproxy.org/docs/alerts/40040/) (!728)
- Upgrade browserker to version `0.0.139` (!727)
  - Add trace level logs for active checks to aid debugging [browserker!1020](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1020)
  - Fix timing attack Timeout [browserker!1024](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1024)
  - Fix incorrect active attack supported locations [browserker!1025](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1025)
  - Parsing attack definitions is more robust to missing supporting locations [browserker!1025](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1025)

## v3.0.62
- Avoid logging custom headers (!724)

## v3.0.61
- Upgrade browserker to version `0.0.138` (!720)
  - Run active attacks in parallel [browserker!1021](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1021)
- Add `DAST_BROWSER_ACTIVE_CHECK_WORKERS` to allow users to set the number of attack to run at a time. (!720)

## v3.0.60
- Add `DAST_BROWSER_ACTIVE_SCAN_TIMEOUT` to allow users to set the maxium time the browser based active scan can take (!723)

## v3.0.59
- Upgrade browserker to version `0.0.137` (!722)
  - Ensure all URLs are scanned on AllowedHosts [browserker!1015](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1015)
  - Add clarity to debug logging when finding a previously found element on a new page [browserker!1016](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1016)

## v3.0.58
- Add `DAST_AUTH_DISABLE_CLEAR_FIELDS` to disable clearing of username and password fields before attempting manual login (!719)
- Upgrade browserker to version `0.0.136` (!719)
  - Clear prefilled input from username and password inputs during manual login [browserker!1013](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1013)
  - Add ClearInputs to disable clearing of prefilled input from username and password inputs during manual login [browserker!1013](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1013)
  - Upgrade vulnerability checks to version 1.0.53 [browserker!1008](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1008)
    - Add injection_locations_policy [dast-cwe-checks!200](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/200)
  - Active check attacks constrains the injection locations to specific locations [browserker!1008](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1008)

## v3.0.57
- Spider hosts set by `DAST_ALLOWED_HOSTS` (!718)

## v3.0.56
- Add `DAST_ALLOWED_HOSTS` to send custom headers to other hosts (!714)

## v3.0.55
- Restrict headers to allowed hosts [CVE-2022-4315](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2022-4315) (!711)

## v3.0.54
- Upgrade browserker to version `0.0.135` (!709)
  Add PageLoadingSelector to allows users to define an element that should not be visible before the scan continues [browserker!1005](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1005)
- Add `DAST_BROWSER_PAGE_LOADING_SELECTOR` to allows users to define an element that should not be visible before the scan continues (!709)
- Add deprecation message for `DAST_HTML_REPORT`, `DAST_XML_REPORT`, `DAST_MARKDOWN_REPORT` environment variables(!703)

## v3.0.53
- Upgrade browserker to version `0.0.134` (!708)
  - Active check attacks can configure the attack HTTP request timeouts [browserker!1003](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1003)

## v3.0.52
- Upgrade browserker to version `0.0.133` (!706)
  - Update dast-chromium to `22.04-108.0.5359.124-1-20230105062330` [browserker!1004](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1004)

## v3.0.51
- Upgrade browserker to version `0.0.132` (!701)
  - Update dast-chromium to `22.04-108.0.5359.124-1-20221220050015` [browserker!1001](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/1001)
  - Exclude custom header values from out-of-scope redirects to CSS and JS [CVE-2022-4317](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2022-4317) [browserker!999](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/999)
- Exclude custom headers from redirects (!701)

## v3.0.50
- Upgrade browserker to version `0.0.131` (!700)
  - Upgrade Golang to v1.19.4 [browserker!996](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/996)

## v3.0.49
- Upgrade browserker to version `0.0.129` (!697)
  - Always fail intercepted requests to excluded URLs [browserker!981](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/981)
- Upgrade browserker to version `0.0.130` (!699)
  - Update dast-chromium to `22.04-108.0.5359.94-1-20221216044055` [browserker!995](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/995)
    - Install avahi from Ubuntu package library to address vulnerability [dast-chromium!25](https://gitlab.com/gitlab-org/security-products/dast-chromium/-/merge_requests/25)
  - Upgrade golang.org/x/crypto [browserker!994](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/994)
  - Improve excluded request handling [browserker!993](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/993)

## v3.0.48
- Upgrade browserker to version `0.0.128` (!693)
  - Upgrade Golang to `v1.19.3` [browserker!989](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/989)
  - Update dast-chromium to `22.04-108.0.5359.94-1-20221213222607` [browserker!983](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/983)
    - Upgrade Chromium to `108.0.5359.94-1~deb11u1` [dast-chromium!19](https://gitlab.com/gitlab-org/security-products/dast-chromium/-/merge_requests/19)
    - Use Debian bullseye package library and Debian bullseye-security package library [dast-chromium!19](https://gitlab.com/gitlab-org/security-products/dast-chromium/-/merge_requests/19)
  - Remove duplicate configuration warnings [browserker!984](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/984)
  - Require selectors to have a type from Browserker `1.0.0` [browserker!985](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/985)
  - Fix reported Browserker version [browserker!987](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/987)
- Compile JPype1 dependency ahead of time (!694)
- Upgrade the DAST JSON report to schema 15.0.2 (!687)
- Upgrade browserker to version `0.0.127` (!687)
  - Remove feature flag `UpdateReportVersion` [browserker!982](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/982)
  - Add `vulnerabilities[].name` to DAST report [browserker!982](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/982)
  - Make excluded URLs always out of scope [browserker!978](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/978)
- Ignore the values of `DAST_ZAP_CLI_OPTIONS` and `DAST_ZAP_LOG_CONFIGURATION` environment variables in DAST version 4 (!695)
- Ignore the values of `DAST_HTML_REPORT`, `DAST_XML_REPORT`, and `DAST_MARKDOWN_REPORT` environment variables in DAST version 4 (!696)

## v3.0.47
- Update so authenticated session tokens are not logged [CVE-2022-4319](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-4319) (!691)

## v3.0.46
- Upgrade browserker to version `0.0.122` (!688)
  - Redact target custom headers from secure report [browserker!949](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/949)
- Upgrade browserker to version `0.0.123` (!688)
  - Check `359.1` nolonger matches credit card numbers found within larger numbers [browserker!971](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/971)
  - Upgrade the DAST JSON report to schema 15.0.2 [browserker!960](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/960)
  - Log regular expression optimizations at trace level [browserker!962](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/962)
  - Bypass searching for response body content when response has no body [browserker!963](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/963)
  - Require selectors to have a type from Browserker `1.0.0` [browserker!955](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/955)
  - Revert redact target custom headers from secure report [browserker!958](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/958)
  - Provide more clarity in log when evaluating authentication success [browserker!967](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/967)
- Upgrade browserker to version `0.0.124` (!688)
  - Log navigation context for GCD messages [browserker!969](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/969)
- Upgrade browserker to version `0.0.125` (!688)
  - Revert require selectors to have a type from Browserker `1.0.0` [browserker!974](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/974)
- Upgrade browserker to version `0.0.126` (!688)
  - Upgrade the DAST JSON report to schema `15.0.2` only when feature flag `UpdateReportVersion` is enabled [browserker!979](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/979)
  - Fix reported Browserker version [browserker!977](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/977)
- Remove support for `DAST_API_SPECIFICATION` environment variable in DAST version 4 (!689)

## v3.0.45
- Always disable ZAP HttpSessionsSite logging (!690)
- Don't run a ZAP URL scan after a browser-based scan (!685)
- Remove passive scan feature flag `--ff-browser-passive-scan-mode` (!685)
- Browser-based scans output scanned resources from the browser-based scan instead of scanned resources from the ZAP scan (!685)
- Suppress ZAP rule output in logs for browser-based scans (!685)
- Suppress ZAP URL output in logs for browser-based scans (!685)

## v3.0.44
- Upgrade ZAP to [2.12.0](https://www.zaproxy.org/docs/desktop/releases/2.12.0/) (!667)
- Upgrade ZAP add-on `Access Control Testing` to [8.0.0](https://github.com/zaproxy/zap-extensions/releases/accessControl-v8) (!667)
- Upgrade ZAP add-on `Alert Filters` to [14.0.0](https://github.com/zaproxy/zap-extensions/releases/alertFilters-v14) (!667)
- Upgrade ZAP add-on `Active scanner rules` to [49.0.0](https://github.com/zaproxy/zap-extensions/releases/ascanrules-v49) (!667)
- Upgrade ZAP add-on `Active scanner rules (beta)` to [43.0.0](https://github.com/zaproxy/zap-extensions/releases/ascanrulesBeta-v43) (!667)
- Upgrade ZAP add-on `Automation Framework` to [0.19.0](https://github.com/zaproxy/zap-extensions/releases/automation-v0.19.0) (!667)
- Upgrade ZAP add-on `Forced Browse` to [12.0.0](https://github.com/zaproxy/zap-extensions/releases/bruteforce-v12) (!667)
- Upgrade ZAP add-on `Call Home` to [0.5.0](https://github.com/zaproxy/zap-extensions/releases/callhome-v0.5.0) (!667)
- Upgrade ZAP add-on `Common Library` to [1.11.0](https://github.com/zaproxy/zap-extensions/releases/commonlib-v1.11.0) (!667)
- Upgrade ZAP add-on `Diff` to [12.0.0](https://github.com/zaproxy/zap-extensions/releases/diff-v12) (!667)
- Upgrade ZAP add-on `DOM XSS Active scanner rule` to [14.0.0](https://github.com/zaproxy/zap-extensions/releases/domxss-v14) (!667)
- Upgrade ZAP add-on `Encoder` to [0.7.0](https://github.com/zaproxy/zap-extensions/releases/encoder-v0.7.0) (!667)
- Upgrade ZAP add-on `Import/Export` to [0.3.0](https://github.com/zaproxy/zap-extensions/releases/exim-v0.3.0) (!667)
- Upgrade ZAP add-on `Form Handler` to [6.1.0](https://github.com/zaproxy/zap-extensions/releases/formhandler-v6.1.0) (!667)
- Upgrade ZAP add-on `Fuzzer` to [13.8.0](https://github.com/zaproxy/zap-extensions/releases/fuzz-v13.8.0) (!667)
- Upgrade ZAP add-on `FuzzDB Files` to [9.0.0](https://github.com/zaproxy/zap-extensions/releases/fuzzdb-v9) (!667)
- Upgrade ZAP add-on `Getting Started with ZAP Guide` to [14.0.0](https://github.com/zaproxy/zap-extensions/releases/gettingStarted-v14) (!667)
- Upgrade ZAP add-on `GraalVM JavaScript` to [0.3.0](https://github.com/zaproxy/zap-extensions/releases/graaljs-v0.3.0) (!667)
- Upgrade ZAP add-on `GraphQL Support` to [0.12.0](https://github.com/zaproxy/zap-extensions/releases/graphql-v0.12.0) (!667)
- Upgrade ZAP add-on `HUD - Heads Up Display` to [0.15.0](https://github.com/zaproxy/zap-hud/releases/v0.15.0) (!667)
- Upgrade ZAP add-on `Invoke Applications` to [12.0.0](https://github.com/zaproxy/zap-extensions/releases/invoke-v12) (!667)
- Upgrade ZAP add-on `Network` to [0.5.0](https://github.com/zaproxy/zap-extensions/releases/network-v0.5.0) (!667)
- Upgrade ZAP add-on `OAST Support` to [0.13.0](https://github.com/zaproxy/zap-extensions/releases/oast-v0.13.0) (!667)
- Upgrade ZAP add-on `Online menus` to [10.0.0](https://github.com/zaproxy/zap-extensions/releases/onlineMenu-v10) (!667)
- Upgrade ZAP add-on `OpenAPI Support` to [30.0.0](https://github.com/zaproxy/zap-extensions/releases/openapi-v30) (!667)
- Upgrade ZAP add-on `Plug-n-Hack Configuration` to [13.0.0](https://github.com/zaproxy/zap-extensions/releases/plugnhack-v13) (!667)
- Upgrade ZAP add-on `Port Scanner` to [10.0.0](https://github.com/zaproxy/zap-extensions/releases/portscan-v10) (!667)
- Upgrade ZAP add-on `Passive scanner rules` to [44.0.0](https://github.com/zaproxy/zap-extensions/releases/pscanrules-v44) (!667)
- Upgrade ZAP add-on `Passive scanner rules (beta)` to [31.0.0](https://github.com/zaproxy/zap-extensions/releases/pscanrulesBeta-v31) (!667)
- Upgrade ZAP add-on `Quick Start` to [35.0.0](https://github.com/zaproxy/zap-extensions/releases/quickstart-v35) (!667)
- Upgrade ZAP add-on `Replacer` to [11.0.0](https://github.com/zaproxy/zap-extensions/releases/replacer-v11) (!667)
- Upgrade ZAP add-on `Report Generation` to [0.17.0](https://github.com/zaproxy/zap-extensions/releases/reports-v0.17.0) (!667)
- Upgrade ZAP add-on `Retest` to [0.4.0](https://github.com/zaproxy/zap-extensions/releases/retest-v0.4.0) (!667)
- Upgrade ZAP add-on `Retire.js` to [0.17.0](https://github.com/zaproxy/zap-extensions/releases/retire-v0.17.0) (!667)
- Upgrade ZAP add-on `Reveal` to [5.0.0](https://github.com/zaproxy/zap-extensions/releases/reveal-v5) (!667)
- Upgrade ZAP add-on `Script Console` to [33.0.0](https://github.com/zaproxy/zap-extensions/releases/scripts-v33) (!667)
- Upgrade ZAP add-on `Selenium` to [15.11.0](https://github.com/zaproxy/zap-extensions/releases/selenium-v15.11.0) (!667)
- Upgrade ZAP add-on `SOAP Support` to [16.0.0](https://github.com/zaproxy/zap-extensions/releases/soap-v16) (!667)
- Upgrade ZAP add-on `Ajax Spider` to [23.10.0](https://github.com/zaproxy/zap-extensions/releases/spiderAjax-v23.10.0) (!667)
- Upgrade ZAP add-on `Tips and Tricks` to [10.0.0](https://github.com/zaproxy/zap-extensions/releases/tips-v10) (!667)
- Upgrade ZAP add-on `Linux WebDrivers` to [46.0.0](https://github.com/zaproxy/zap-extensions/releases/webdriverlinux-v46) (!667)
- Upgrade ZAP add-on `WebSockets` to [27.0.0](https://github.com/zaproxy/zap-extensions/releases/websocket-v27) (!667)
- Upgrade ZAP add-on `Zest - Graphical Security Scripting Language` to [37.0.0](https://github.com/zaproxy/zap-extensions/releases/zest-v37) (!667)
- Filter `org.zaproxy.addon.network.internal.handlers.ServerExceptionHandler - Failed while establishing secure connection, cause: Received fatal alert: certificate_unknown` from ZAP logs to avoid confusion (!667)

## v3.0.43
- Upgrade browserker to version `0.0.121` (!681)
  - Log verification of authentication tokens after login [browserker!954](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/954)

## v3.0.42
- Upgrade browserker to version `0.0.120` (!680)
  - Remove certificate error logging and ignore all certificate errors [browserker!950](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/950)

## v3.0.41
- Upgrade browserker to version `0.0.119` (!679)
  - Automatically resume when JavaScript debugger pauses authentication or scan [browserker!944](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/944)

## v3.0.40
- Add experimental configuration `DAST_FF_BYPASS_ZAP` to allow browser-based scans to bypass ZAP (!674)

## v3.0.39
- Upgrade browserker to version `0.0.117` (!673)
  - Standardize logging during authentication verification [browserker!929](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/929)
  - Log page stability indicators [browserker!940](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/940)
- Upgrade browserker to version `0.0.118` (!675)
  - Record nanoseconds in timestamps in log [browserker!939](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/939)
  - Optimize likelihood of finding implicit forms when the DOM refreshes [browserker!924](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/924)
  - Log new navigations [browserker!941](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/941)
- Disable all ZAP passive rules for Browser Based scans (!672)
- Redact `CustomHeaders` logging from browserker startup [CVE-2022-4056](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2022-4056) (!676)

## v3.0.38
- Upgrade browserker to version `0.0.116` (!671)
  - Enable active check 943.1 NoSQL Injection [browserker!927](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/927)
  - Fixes timing attack with multiple affixes [browserker!935](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/935)

## v3.0.37
- Upgrade browserker to version `0.0.115` (!670)
  - Check active scan timeout not reached in navigation result database querying [browserker!915](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/915)

## v3.0.36
- Enable caching for Browser Based scans by default (!669)
- Only write Browserker log when requested (!668)

## v3.0.35
- Upgrade browserker to version `0.0.114` (!666)
  - Update dast-chromium to `22.04-105.0.5195.102-1-20221108005751` [browserker!921](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/921)
    - Upgrade all packages before installing Chromium [dast-chromium!15](https://gitlab.com/gitlab-org/security-products/dast-chromium/-/merge_requests/15)

## v3.0.34
- Remove count of URLs scanned by DAST in the console output (!662)

## v3.0.33
- Disable `https://www.zaproxy.org/docs/alerts/90034` for all scans (!663)
- Remove extra text from URLs in report (!665)

## v3.0.32
- Upgrade browserker to version `0.0.113` (!661)
  - Update dast-chromium to `22.04-105.0.5195.102-1-` [browserker!914](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/914)
    - Upgrade `openssl` and `libssl3` to version `3.0.2-0ubuntu1.7` [dast-chromium!13](https://gitlab.com/gitlab-org/security-products/dast-chromium/-/merge_requests/13)

## v3.0.31
- Upgrade browserker to version `0.0.112` (!660)
  - Suppress errors when retrieving attributes from nodes that no longer exist on the DOM [browserker!910](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/910)
  - Improve accuracy when finding form elements on new DOMs  [browserker!907](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/907)
  - Enable active check `611.1` External XML Entity Injection (XXE) [browserker!879](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/879)
  - Upgrade vulnerability checks to version `1.0.53` [browserker!879](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/879)
    - Update regular expression escaping in 611.1 [dast-cwe-checks!198](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/198)
  - Update dast-chromium to `22.04-105.0.5195.102-1` [browserker!909](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/909)
    - Update Ubuntu version to long-term release 22.04 [dast-chromium!9](https://gitlab.com/gitlab-org/security-products/dast-chromium/-/merge_requests/9)
  - Standardize the way forms and other elements are matched when searching new DOMs [browserker!901](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/901)
  - Enable active check `113.1` Improper Neutralization of CRLF Sequences in HTTP Headers [browserker!902](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/902)
  - Enable active check `94.1` Server-side code injection (PHP) [browserker!895](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/895)
  - Check active scan timeout not reached before running next active check [browserker!899](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/899)
  - Check active scan timeout not reached within an active check [browserker!905](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/905)
  - Check active scan timeout not reached in web server gateway [browserker!908](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/908)
- Enable browser-based active check `611.1` External XML Entity Injection (XXE) (!660)
- Enable browser-based active check `94.1` Server-side code injection (PHP) (!660)
- Enable browser-based active check `113.1` Improper Neutralization of CRLF Sequences in HTTP Headers (!660)

## v3.0.30
- Upgrade browserker to version `0.0.111` (!658)
  - AttackRequests with different Authorization or Cookie headers should be considered as duplicates [browserker!893](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/893)
  - Update timing attacks so that duplicate attack requests are not made [browserker!892](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/892)
  - Use the query selector that found an element when searching for the element in a new DOM [browserker!895](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/894)
  - Update match response attacks so that duplicate attack requests are not made [browserker!888](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/888)

## v3.0.29
- Upgrade browserker to version `0.0.110` (!656)
  - Handle error when Browserker fails to start [browserker!886](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/886)
  - Upgrade vulnerability checks to version `1.0.51` [browserker!877](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/877)
    - Update severity in active checks `74.1`, `94.4` to allow for parsing [dast-cwe-checks!192](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/192)
    - Remove trailing new line in attack `78.1` payload [dast-cwe-checks!191](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/191)
    - Remove trailing new line in attack `94.1` payload [dast-cwe-checks!190](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/190)
    - Standardize encoding used in YAML definitions [dast-cwe-checks!193](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/193)
    - Extend `passive_location_matcher` with `binary` and `byte_limit` properties [dast-cwe-checks!194](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/194)
  - Upgrade vulnerability checks to version `1.0.52` [browserker!877](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/877)
    - Use standardized encoding for payloads in YAML definitions [dast-cwe-checks!197](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/197)
  - Don't trim payloads so that active checks can include new lines in an attack [browserker!877](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/877)
- Upgrade browserker to version `0.0.109` (!656)
  - Add request path injection location [browserker!829](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/829)
  - Add support for binary matching in response bodies [browserker!873](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/873)
- Upgrade browserker to version `0.0.108` (!653)
  - Add support for multipart form data injection location [browserker!819](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/819)
  - Enable active check `94.3` Server-side code injection (Python) [browserker!863](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/863)
  - Enable active check `94.2` Server-side code injection (Ruby) [browserker!867](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/867)
  - Upgrade minimum supported version of Go to version `1.18` [browserker!865](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/865)
  - Upgrade Chromium to version `105.0.5195.102-1` [browserker!872](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/872)
  - Upgrade Chromium DevTools protocol to version `105.0.5195.102-1` [browserker!872](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/872)
- Enable browser-based active check `94.2` Server-side code injection (Ruby) (!653)
- Enable browser-based active check `94.3` Server-side code injection (Python) (!653)
- Update ChromeDriver to version `105.0.5195.52` to support Chromium 105 (!465)
- Revert ZAP to the latest stable version `2.11.1` to resolve proxy issues while running browser-based or authenticated scans (!657)

## v3.0.28
- Upgrade browserker to version `0.0.107` (!651)
  - Active checks find injection locations in all crawled HTTP messages [browserker!840](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/840)
  - Upgrade vulnerability checks to version `1.0.48` [browserker!853](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/853)
    - Update expressions for `78.1` [dast-cwe-checks!184](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/184)
    - Convert double brackets `{{...}}` to single brackets `{...}` for consistency [dast-cwe-checks!185](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/185)
    - Fix broken matching regular expressions for check `22.1` [dast-cwe-checks!186](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/186)
  - Upgrade vulnerability checks to version `1.0.49` [browserker!853](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/853)
    - Update etc/passwd expression for `22.1` [dast-cwe-checks!187](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/187)
  - Upgrade vulnerability checks to version `1.0.50` [browserker!853](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/853)
    - Add `943.1` Improper Neutralization of Special Elements in Data Query Logic MongoDB [dast-cwe-checks!166](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/166)
    - Check `94.4` is resilient to mirrors being used as the source URL [dast-cwe-checks!188](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/188)
    - Check `94.4` uses self-executing functions to maximize likelihood of execution when injected into `eval` [dast-cwe-checks!188](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/188)
  - HTTP messages loaded for active checks are cached for fast retrieval [browserker!855](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/855)
  - Enable active check `94.4` [browserker!839](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/839)
  - Ignore callback attacks when parsing active check definitions [browserker!839](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/839)
  - Log which injection location detector failed when there is an error [browserker!854](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/854)
- Enable browser-based active check `78.1` OS Command Injection (!651)
- Enable browser-based active check `94.4` Server-side code injection (NodeJS) (!651)

## v3.0.27
- Upgrade browserker to version `0.0.106` (!650)
  - Add remaining attacks to check `22.1` [browserker!822](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/822)
  - Add remaining attacks to check `78.1` [browserker!820](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/820)
  - Use single brackets when interpolating active check injection templates [browserker!823](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/823)

## v3.0.26
- Upgrade browserker to version `0.0.103` (!648)
  - Add JSON value injection location for active attacks [browserker!768](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/768)
  - Upgrade vulnerability checks to version `1.0.45` [browserker!784](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/784)
    - Add 22.1 Improper limitation of a pathname to a restricted directory (Path traversal) [dast-cwe-checks!175](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/175)
  - Match response attacks severity depends on which matcher matched the attack response [browserker!779](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/779)
  - Add support for `response_status` matcher in vulnerability checks [browserker!785](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/785)
  - Bump go-csp-evaluator to version 1.0.2 [browserker!790](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/790)
  - Active timing check finding summaries include time waiting for response [browserker!787](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/787)
  - Active timing checks define the finding severity [browserker!792](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/792)
  - Active timing checks process injection templates in groups to find vulnerabilities [browserker!791](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/791)
  - Active check injections are generated in the same order as they are defined [browserker!791](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/791)
  - Log active check requests with the `WEBGW` log module [browserker!789](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/789)
  - Active check requests have a default timeout of 30 seconds [browserker!800](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/800)
- Upgrade browserker to version `0.0.104` (!648)
  - Add support for Array values to JSON value injection location for active attacks [browserker!805](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/805)
  - Upgrade vulnerability checks to version `1.0.47` [browserker!813](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/813)
    - Update `287.1` to match on `Authorization` header [dast-cwe-checks!182](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/182)
  - Upgrade vulnerability checks to version `1.0.46` [browserker!813](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/813)
    - Add `94.1` Server-side code injection (PHP) [dast-cwe-checks!158](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/158)
    - Add `94.2` Server-side code injection (Ruby) [dast-cwe-checks!158](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/158)
    - Add `94.3` Server-side code injection (Python) [dast-cwe-checks!158](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/158)
    - Add `94.4` Server-side code injection (NodeJS) [dast-cwe-checks!158](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/158)
    - Update the `16.8` description for clarity [dast-cwe-checks!181](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/181)
    - Add `384.1` Session fixation [dast-cwe-checks!151](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/151)
    - Add `79.1` Cross Site Scripting [dast-cwe-checks!138](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/138)
    - Add `79.2` Persistent Cross Site Scripting [dast-cwe-checks!138](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/138)
    - Add `89.1` SQL Injection [dast-cwe-checks!122](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/122)
    - Add `74.1` XSLT Injection [dast-cwe-checks!149](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/149)
    - Add `113.1` Improper Neutralization of CRLF Sequences in HTTP Headers [dast-cwe-checks!140](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/140)
    - Add `552.1` SQL database dump file is publicly accessible [dast-cwe-checks!156](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/156)
    - Add `552.2` SQL database dump file is publicly accessible [dast-cwe-checks!156](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/156)
    - Add `552.3` Backup archive file is publicly accessible [dast-cwe-checks!156](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/156)
    - Add `552.4` Lazy File Manager is publicly accessible [dast-cwe-checks!156](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/156)
    - Add `552.5` IntelliJ IDEA deployment configuration file is publicly accessible [dast-cwe-checks!156](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/156)
    - Add `552.6` Symfony database configuration file is publicly accessible [dast-cwe-checks!156](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/156)
    - Add `552.7` Ruby On Rails database configuration file is publicly accessible [dast-cwe-checks!156](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/156)
    - Add `552.8` Git metadata directory is publicly accessible [dast-cwe-checks!156](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/156)
    - Add `552.9` SVN metadata directory is publicly accessible [dast-cwe-checks!156](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/156)
    - Add `552.10` Apache status page is publicly accessible [dast-cwe-checks!156](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/156)
    - Add `552.11` Core dump file is publicly accessible [dast-cwe-checks!156](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/156)
    - Add `552.12` Sublime Text SFTP configuration file is publicly accessible [dast-cwe-checks!156](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/156)
    - Add `552.13` WS_FTP client configuration file is publicly accessible [dast-cwe-checks!156](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/156)
    - Add `552.14` FileZilla client configuration file is publicly accessible [dast-cwe-checks!156](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/156)
    - Add `552.15` WinSCP client configuration file is publicly accessible [dast-cwe-checks!156](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/156)
    - Add `552.16` MacOS DS_Store file is publicly accessible [dast-cwe-checks!156](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/156)
    - Add `552.17` PHP Coding Standards Fixer cache is publicly accessible [dast-cwe-checks!156](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/156)
    - Add `552.18` JOE Editor crash file is publicly accessible [dast-cwe-checks!156](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/156)
    - Add `552.19` Drupal Backup Migrate directory is publicly accessible [dast-cwe-checks!156](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/156)
    - Add `552.20` Magento configuration file is publicly accessible [dast-cwe-checks!156](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/156)
    - Add `552.21` Cryptographic private key is publicly accessible [dast-cwe-checks!156](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/156)
    - Add `552.22` vBulletin test script is publicly accessible [dast-cwe-checks!156](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/156)
    - Add `552.23` Drupal database file is publicly accessible [dast-cwe-checks!156](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/156)
    - Add `552.24` Composer manifest file is publicly accessible [dast-cwe-checks!156](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/156)
    - Add `552.25` JetBrains Vim plugin configuration file is publicly accessible [dast-cwe-checks!156](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/156)
    - Add `552.26` PHP Info page is publicly accessible [dast-cwe-checks!156](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/156)
    - Add `552.27` Apache Server Information page is publicly accessible [dast-cwe-checks!156](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/156)
- Upgrade browserker to version `0.0.105` (!648)
  - Timing attacks are run multiple times to be more resilient to false-positives [browserker!805](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/805)

## v3.0.25
- Upgrade browserker to version `0.0.102` (!645)
  - Upgrade vulnerability checks to version `1.0.44` [browserker!781](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/781)
    - Update schema to constrain timing payload constraints to `>`, `<`, `>=`, `<=`, or `==` [dast-cwe-checks!172](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/172)
    - Add masked tokens to `798` passive checks [dast-cwe-checks!174](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/174)

## v3.0.24
- Do not throw when `DAST_OPEN_API` environment variable is set additionally (!643)

## v3.0.23
- Remove support for `DAST_API_OPENAPI` environment variable in DAST version 4 (!642)
- Upgrade browserker to version `0.0.101` (!644)
  - Match response attacks have multiple matchers [browserker!765](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/765)
  - Improve error message readability by removing capital letters [browserker!770](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/770)
  - Upgrade Golang to `v1.18.4` [browserker!776](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/776)

## v3.0.22
- Upgrade Python to version `3.10` (!639)
- Upgrade browserker to version `0.0.98` (!639)
  - Redact target authentication password from secure report [browserker!717](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/717)
- Upgrade browserker to version `0.0.99` (!638)
  - Add header value injection location for active attacks [browserker!757](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/757)
  - Dynamically generate passive check summary from template [browserker!748](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/748)
  - Upgrade vulnerability checks to version 1.0.43 [browserker!748](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/748)
    - Fix 200.1, 798.111 and 798.117 summaries [dast-cwe-checks!171](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/171)
  - Upgrade Go to version 1.17.13 to include the latest security patches [browserker!759](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/759)
- Upgrade browserker to version `0.0.100` (!638)
  - Upgrade base Ubuntu version to 22.10 [browserker!769](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/769)
- Re-add support for `DAST_API_SPECIFICATION` environment variable (!641)

## v3.0.21
- Update ZAP to [w2022-08-08](https://github.com/zaproxy/zaproxy/releases/tag/w2022-08-08/) (!596)

## v3.0.20
- Upgrade browserker to version `0.0.97` (!637)
  - Upgrade vulnerability checks to version `1.0.41` [browserker!744](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/744)
    - Remove `report_uniqueness` field [dast-cwe-checks!163](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/163)
  - Add query parameter value injection location for active attacks [browserker!745](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/745)
  - Upgrade vulnerability checks to version `1.0.42` [browserker!747](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/747)
    - Update `693.1` match condition requirement to have `has_response_body` [dast-cwe-checks!170](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/170)
    - Replace placeholder summary for `798.x` passive checks [dast-cwe-checks!165](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/165)
    - Replace placeholder summary for `16.x` passive checks [dast-cwe-checks!167](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/167)
    - Replace placeholder summary for `200.1`, `287.1`, `287.2`, `319.1`, `359.1`, `359.2` passive checks [dast-cwe-checks!168](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/168)
    - Replace placeholder summary for `209.1`, `209.2`, `352.1` passive checks [dast-cwe-checks!169](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/169)
    - Replace placeholder summary for `548.1`, `598.1`, `598.2`, `598.3` passive checks [dast-cwe-checks!169](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/169)
    - Replace placeholder summary for `601.1`, `693.1`, `829.1`, `829.2`, `1004.1` passive checks [dast-cwe-checks!169](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/169)

## v3.0.19
- Update URL Scan to us ZAP addon [import-export](https://www.zaproxy.org/docs/desktop/addons/import-export/) (!634)
- Upgrade ZAP add-on `Active scanner rules (beta)` to [41.0.0](https://github.com/zaproxy/zap-extensions/releases/ascanrulesBeta-v41) (!635)
- Upgrade ZAP add-on `Automation Framework` to [0.16.0](https://github.com/zaproxy/zap-extensions/releases/automation-v0.16.0) (!635)
- Upgrade ZAP add-on `Call Home` to [0.4.0](https://github.com/zaproxy/zap-extensions/releases/callhome-v0.4.0) (!635)
- Upgrade ZAP add-on `Import/Export` to [0.2.0](https://github.com/zaproxy/zap-extensions/releases/exim-v0.2.0) (!635)
- Upgrade ZAP add-on `Form Handler` to [5.0.0](https://github.com/zaproxy/zap-extensions/releases/formhandler-v5) (!635)
- Upgrade ZAP add-on `Passive scanner rules` to [42.0.0](https://github.com/zaproxy/zap-extensions/releases/pscanrules-v42) (!635)
- Upgrade ZAP add-on `Report Generation` to [0.15.0](https://github.com/zaproxy/zap-extensions/releases/reports-v0.15.0) (!635)
- Upgrade ZAP add-on `Retire.js` to [0.12.0](https://github.com/zaproxy/zap-extensions/releases/retire-v0.12.0) (!635)
- Upgrade ZAP add-on `Selenium` to [15.9.0](https://github.com/zaproxy/zap-extensions/releases/selenium-v15.9.0) (!635)
- Upgrade ZAP add-on `Linux WebDrivers` to [41.0.0](https://github.com/zaproxy/zap-extensions/releases/webdriverlinux-v41) (!635)
- Upgrade ZAP add-on `WebSockets` to [26.0.0](https://github.com/zaproxy/zap-extensions/releases/websocket-v26) (!635)
- Enable the browser-based active check `22.1` using the feature toggle `DAST_FF_ENABLE_BROWSER_BASED_ATTACKS` (!632)

## v3.0.18
- Upgrade browserker to version `0.0.96` (!633)
  - Optimize `ResponseBodyMatcher` regexps when `OptimizeRegexps` feature flag is enabled [browserker!597](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/597)
  - Upgrade vulnerability checks to version `1.0.37` [browserker!740](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/740)
    - Standardize active check definitions [dast-cwe-checks!159](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/159)
  - Upgrade vulnerability checks to version `1.0.38` [browserker!740](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/740)
    - Add templates field to passive checks, duplicating existing `report_uniqueness` value [dast-cwe-checks!160](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/160)
  - Upgrade vulnerability checks to version `1.0.39` [browserker!742](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/742)
    - Update all 798 checks to only match on `text`, `JSON`, and `javascript` content types [dast-cwe-checks!162](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/162)
  - Upgrade vulnerability checks to version `1.0.40` [browserker!742](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/742)
    - Fix `798` checks to match on the ld-json content types [dast-cwe-checks!164](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/164)
    - Update `798.111` to only match on `text`, `JSON`, and `javascript` content types [dast-cwe-checks!164](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/164)
    - Update `798.117` to only match on `text`, `JSON`, and `javascript` content types [dast-cwe-checks!164](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/164)

## v3.0.17
- Upgrade browserker to version `0.0.95` (!629)
  - Upgrade vulnerability checks to version `1.0.36` [browserker!736](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/736)
    - Remove `798.45` Exposure of confidential secret or token Finicity Public Key [dast-cwe-checks!155](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/157)
    - Remove `798.71` Exposure of confidential secret or token Lob Publishable API Key [dast-cwe-checks!155](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/157)
    - Remove `798.73` Exposure of confidential secret or token Mailgun public validation key [dast-cwe-checks!155](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/157)
    - Remove `798.76` Exposure of confidential secret or token MapBox API token [dast-cwe-checks!155](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/157)
    - Remove `798.79` Exposure of confidential secret or token MessageBird client ID [dast-cwe-checks!155](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/157)
    - Remove `798.85` Exposure of confidential secret or token Nytimes Access Token [dast-cwe-checks!155](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/157)
    - Remove `798.51` Exposure of confidential secret or token GCP API key [dast-cwe-checks!155](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/157)
    - Update `798.111` Exposure of confidential secret or token Stripe, so public keys do not produce findings [dast-cwe-checks!155](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/157)
    - Update `798.117` Exposure of confidential secret or token Twilio API Key, to reduce false positives [dast-cwe-checks!155](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/157)
- Enable `798.1` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.1.html) in browser based scan (!629)
- Enable `798.2` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.2.html) in browser based scan (!629)
- Enable `798.3` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.3.html) in browser based scan (!629)
- Enable `798.4` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.4.html) in browser based scan (!629)
- Enable `798.5` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.5.html) in browser based scan (!629)
- Enable `798.6` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.6.html) in browser based scan (!629)
- Enable `798.7` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.7.html) in browser based scan (!629)
- Enable `798.8` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.8.html) in browser based scan (!629)
- Enable `798.9` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.9.html) in browser based scan (!629)
- Enable `798.10` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.10.html) in browser based scan (!629)
- Enable `798.11` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.11.html) in browser based scan (!629)
- Enable `798.12` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.12.html) in browser based scan (!629)
- Enable `798.13` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.13.html) in browser based scan (!629)
- Enable `798.14` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.14.html) in browser based scan (!629)
- Enable `798.15` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.15.html) in browser based scan (!629)
- Enable `798.16` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.16.html) in browser based scan (!629)
- Enable `798.17` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.17.html) in browser based scan (!629)
- Enable `798.18` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.18.html) in browser based scan (!629)
- Enable `798.19` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.19.html) in browser based scan (!629)
- Enable `798.20` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.20.html) in browser based scan (!629)
- Enable `798.21` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.21.html) in browser based scan (!629)
- Enable `798.22` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.22.html) in browser based scan (!629)
- Enable `798.23` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.23.html) in browser based scan (!629)
- Enable `798.24` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.24.html) in browser based scan (!629)
- Enable `798.25` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.25.html) in browser based scan (!629)
- Enable `798.26` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.26.html) in browser based scan (!629)
- Enable `798.27` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.27.html) in browser based scan (!629)
- Enable `798.28` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.28.html) in browser based scan (!629)
- Enable `798.29` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.29.html) in browser based scan (!629)
- Enable `798.30` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.30.html) in browser based scan (!629)
- Enable `798.31` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.31.html) in browser based scan (!629)
- Enable `798.32` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.32.html) in browser based scan (!629)
- Enable `798.33` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.33.html) in browser based scan (!629)
- Enable `798.34` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.34.html) in browser based scan (!629)
- Enable `798.35` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.35.html) in browser based scan (!629)
- Enable `798.36` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.36.html) in browser based scan (!629)
- Enable `798.37` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.37.html) in browser based scan (!629)
- Enable `798.38` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.38.html) in browser based scan (!629)
- Enable `798.39` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.39.html) in browser based scan (!629)
- Enable `798.40` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.40.html) in browser based scan (!629)
- Enable `798.41` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.41.html) in browser based scan (!629)
- Enable `798.42` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.42.html) in browser based scan (!629)
- Enable `798.43` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.43.html) in browser based scan (!629)
- Enable `798.44` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.44.html) in browser based scan (!629)
- Enable `798.46` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.46.html) in browser based scan (!629)
- Enable `798.47` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.47.html) in browser based scan (!629)
- Enable `798.48` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.48.html) in browser based scan (!629)
- Enable `798.49` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.49.html) in browser based scan (!629)
- Enable `798.50` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.50.html) in browser based scan (!629)
- Enable `798.52` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.52.html) in browser based scan (!629)
- Enable `798.53` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.53.html) in browser based scan (!629)
- Enable `798.54` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.54.html) in browser based scan (!629)
- Enable `798.55` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.55.html) in browser based scan (!629)
- Enable `798.56` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.56.html) in browser based scan (!629)
- Enable `798.57` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.57.html) in browser based scan (!629)
- Enable `798.58` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.58.html) in browser based scan (!629)
- Enable `798.59` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.59.html) in browser based scan (!629)
- Enable `798.60` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.60.html) in browser based scan (!629)
- Enable `798.61` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.61.html) in browser based scan (!629)
- Enable `798.62` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.62.html) in browser based scan (!629)
- Enable `798.63` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.63.html) in browser based scan (!629)
- Enable `798.64` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.64.html) in browser based scan (!629)
- Enable `798.65` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.65.html) in browser based scan (!629)
- Enable `798.66` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.66.html) in browser based scan (!629)
- Enable `798.67` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.67.html) in browser based scan (!629)
- Enable `798.68` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.68.html) in browser based scan (!629)
- Enable `798.69` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.69.html) in browser based scan (!629)
- Enable `798.70` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.70.html) in browser based scan (!629)
- Enable `798.72` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.72.html) in browser based scan (!629)
- Enable `798.74` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.74.html) in browser based scan (!629)
- Enable `798.75` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.75.html) in browser based scan (!629)
- Enable `798.77` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.77.html) in browser based scan (!629)
- Enable `798.78` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.78.html) in browser based scan (!629)
- Enable `798.80` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.80.html) in browser based scan (!629)
- Enable `798.81` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.81.html) in browser based scan (!629)
- Enable `798.82` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.82.html) in browser based scan (!629)
- Enable `798.83` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.83.html) in browser based scan (!629)
- Enable `798.84` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.84.html) in browser based scan (!629)
- Enable `798.86` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.86.html) in browser based scan (!629)
- Enable `798.87` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.87.html) in browser based scan (!629)
- Enable `798.88` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.88.html) in browser based scan (!629)
- Enable `798.89` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.89.html) in browser based scan (!629)
- Enable `798.90` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.90.html) in browser based scan (!629)
- Enable `798.91` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.91.html) in browser based scan (!629)
- Enable `798.92` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.92.html) in browser based scan (!629)
- Enable `798.93` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.93.html) in browser based scan (!629)
- Enable `798.94` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.94.html) in browser based scan (!629)
- Enable `798.95` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.95.html) in browser based scan (!629)
- Enable `798.96` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.96.html) in browser based scan (!629)
- Enable `798.97` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.97.html) in browser based scan (!629)
- Enable `798.98` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.98.html) in browser based scan (!629)
- Enable `798.99` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.99.html) in browser based scan (!629)
- Enable `798.100` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.100.html) in browser based scan (!629)
- Enable `798.101` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.101.html) in browser based scan (!629)
- Enable `798.102` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.102.html) in browser based scan (!629)
- Enable `798.103` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.103.html) in browser based scan (!629)
- Enable `798.104` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.104.html) in browser based scan (!629)
- Enable `798.105` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.105.html) in browser based scan (!629)
- Enable `798.106` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.106.html) in browser based scan (!629)
- Enable `798.107` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.107.html) in browser based scan (!629)
- Enable `798.108` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.108.html) in browser based scan (!629)
- Enable `798.109` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.109.html) in browser based scan (!629)
- Enable `798.110` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.110.html) in browser based scan (!629)
- Enable `798.111` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.111.html) in browser based scan (!629)
- Enable `798.112` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.112.html) in browser based scan (!629)
- Enable `798.113` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.113.html) in browser based scan (!629)
- Enable `798.114` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.114.html) in browser based scan (!629)
- Enable `798.115` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.115.html) in browser based scan (!629)
- Enable `798.116` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.116.html) in browser based scan (!629)
- Enable `798.117` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.117.html) in browser based scan (!629)
- Enable `798.118` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.118.html) in browser based scan (!629)
- Enable `798.119` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.119.html) in browser based scan (!629)
- Enable `798.120` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.120.html) in browser based scan (!629)
- Enable `798.121` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.121.html) in browser based scan (!629)
- Enable `798.122` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.122.html) in browser based scan (!629)
- Enable `798.123` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.123.html) in browser based scan (!629)
- Enable `798.124` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.124.html) in browser based scan (!629)
- Enable `798.125` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.125.html) in browser based scan (!629)
- Enable `798.126` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.126.html) in browser based scan (!629)
- Enable `798.127` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.127.html) in browser based scan (!629)
- Enable `798.128` (https://docs.gitlab.com/ee/user/application_security/dast/checks/798.128.html) in browser based scan (!629)

## v3.0.16
- Upgrade browserker to version `0.0.94` (!630)
  - Upgrade vulnerability checks to version `1.0.35` [browserker!728](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/728)
    - Remove realm restriction from `287.1` when looking for www-authenticate: basic [dast-cwe-checks!155](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/155)
- Replace [ZAP rule 10105](https://www.zaproxy.org/docs/alerts/10105/) with:
  - [287.1](https://docs.gitlab.com/ee/user/application_security/dast/checks/287.1.html) in browser based scan (!630)
  - [287.2](https://docs.gitlab.com/ee/user/application_security/dast/checks/287.2.html) in browser based scan (!630)

## v3.0.15
- Upgrade browserker to version `0.0.93` (!631)
  - Fix checks to only scan in scope URLs during authentication [browserker!724](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/724)
  - Upgrade vulnerability checks to version `1.0.33` [browserker!723](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/723)
    - Add `287.1` Insecure authentication over HTTP (Basic Authentication) [dast-cwe-checks!96](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/96)
    - Add `287.2` Insecure authentication over HTTP (Digest Authentication) [dast-cwe-checks!96](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/96)
  - Upgrade vulnerability checks to version `1.0.34` [browserker!723](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/723)
    - Remove placeholders from description and remediation [dast-cwe-checks!152](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/152)

## v3.0.14
- Upgrade browserker to version `0.0.91` (!628)
  - Upgrade vulnerability checks to version `1.0.30` [browserker!711](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/711)
    - Add `798.129` Exposure of confidential secret or token Generic API Key [dast-cwe-checks!143](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/143)
  - Upgrade vulnerability checks to version `1.0.31` [browserker!711](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/711)
    - Remove `798.129` Exposure of confidential secret or token Generic API Key to reduce false positives [dast-cwe-checks!146](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/146)
- Upgrade browserker to version `0.0.92` (!628)
  - Upgrade vulnerability checks to version `1.0.32` [browserker!712](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/712)
    - Update multiple check report uniqueness to be keyed off `request_method` [dast-cwe-checks!147](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/147)

## v3.0.13
- Aggregate findings found by the [Aggregate Cookie No HttpOnly Flag check](https://www.zaproxy.org/docs/alerts/10010) (!627)

## v3.0.12
- Upgrade browserker to version `0.0.90` (!625)
  - Update `601.1` to only match requests that are http(s) and in scope [browserker!696](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/696)

## v3.0.11
- Upgrade browserker to version `0.0.89` (!624)
  - Enable `209.2` passive check [browserker!684](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/684)
  - Update `829.1` to only match requests that are http(s) and in scope [browserker!689](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/689)
  - Update `359.1` to only match requests that are http(s) and in scope [browserker!690](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/690)
  - Update `359.2` to only match requests that are http(s) and in scope [browserker!691](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/691)
  - Upgrade vulnerability checks to version `1.0.28` [browserker!694](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/694)
    - Update `209.2` uniqueness template to request method and request path [dast-cwe-checks!134](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/134)
  - Upgrade vulnerability checks to version `1.0.29` [browserker!694](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/694)
    - Remove all `798` checks [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.1` Exposure of confidential secret or token Adafruit API Key [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.2` Exposure of confidential secret or token Adobe Client ID (Oauth Web) [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.3` Exposure of confidential secret or token Adobe Client Secret [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.4` Exposure of confidential secret or token Age secret key [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.5` Exposure of confidential secret or token Airtable API Key [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.6` Exposure of confidential secret or token Algolia API Key [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.7` Exposure of confidential secret or token Alibaba AccessKey ID [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.8` Exposure of confidential secret or token Alibaba Secret Key [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.9` Exposure of confidential secret or token Asana Client ID [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.10` Exposure of confidential secret or token Asana Client Secret [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.11` Exposure of confidential secret or token Atlassian API token [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.12` Exposure of confidential secret or token AWS [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.13` Exposure of confidential secret or token BitBucket Client ID [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.14` Exposure of confidential secret or token BitBucket Client Secret [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.15` Exposure of confidential secret or token Bittrex Access Key [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.16` Exposure of confidential secret or token Bittrex Secret Key [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.17` Exposure of confidential secret or token Beamer API token [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.18` Exposure of confidential secret or token Codecov Access Token [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.19` Exposure of confidential secret or token Coinbase Access Token [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.20` Exposure of confidential secret or token Clojars API token [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.21` Exposure of confidential secret or token Confluent Access Token [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.22` Exposure of confidential secret or token Confluent Secret Key [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.23` Exposure of confidential secret or token Contentful delivery API token [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.24` Exposure of confidential secret or token Databricks API token [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.25` Exposure of confidential secret or token Datadog Access Token [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.26` Exposure of confidential secret or token Discord API key [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.27` Exposure of confidential secret or token Discord client ID [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.28` Exposure of confidential secret or token Discord client secret [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.29` Exposure of confidential secret or token Doppler API token [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.30` Exposure of confidential secret or token Dropbox API secret [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.31` Exposure of confidential secret or token Dropbox long lived API token [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.32` Exposure of confidential secret or token Dropbox short lived API token [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.33` Exposure of confidential secret or token Droneci Access Token [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.34` Exposure of confidential secret or token Duffel API token [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.35` Exposure of confidential secret or token Dynatrace API token [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.36` Exposure of confidential secret or token EasyPost API token [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.37` Exposure of confidential secret or token EasyPost test API token [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.38` Exposure of confidential secret or token Etsy Access Token [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.39` Exposure of confidential secret or token facebook [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.40` Exposure of confidential secret or token Fastly API key [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.41` Exposure of confidential secret or token Finicity Client Secret [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.42` Exposure of confidential secret or token Finicity API token [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.43` Exposure of confidential secret or token Flickr Access Token [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.44` Exposure of confidential secret or token Finnhub Access Token [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.45` Exposure of confidential secret or token Finicity Public Key [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.46` Exposure of confidential secret or token Flutterwave Secret Key [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.47` Exposure of confidential secret or token Flutterwave Encryption Key [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.48` Exposure of confidential secret or token Frame.io API token [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.49` Exposure of confidential secret or token Freshbooks Access Token [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.50` Exposure of confidential secret or token GoCardless API token [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.51` Exposure of confidential secret or token GCP API key [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.52` Exposure of confidential secret or token GitHub Personal Access Token [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.53` Exposure of confidential secret or token GitHub OAuth Access Token [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.54` Exposure of confidential secret or token GitHub App Token [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.55` Exposure of confidential secret or token GitHub Refresh Token [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.56` Exposure of confidential secret or token Gitlab Personal Access Token [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.57` Exposure of confidential secret or token Gitter Access Token [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.58` Exposure of confidential secret or token HashiCorp Terraform user/org API token [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.59` Exposure of confidential secret or token Heroku API Key [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.60` Exposure of confidential secret or token HubSpot API Token [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.61` Exposure of confidential secret or token Intercom API Token [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.62` Exposure of confidential secret or token Kraken Access Token [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.63` Exposure of confidential secret or token Kucoin Access Token [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.64` Exposure of confidential secret or token Kucoin Secret Key [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.65` Exposure of confidential secret or token Launchdarkly Access Token [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.66` Exposure of confidential secret or token Linear API Token [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.67` Exposure of confidential secret or token Linear Client Secret [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.68` Exposure of confidential secret or token LinkedIn Client ID [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.69` Exposure of confidential secret or token LinkedIn Client secret [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.70` Exposure of confidential secret or token Lob API Key [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.71` Exposure of confidential secret or token Lob Publishable API Key [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.72` Exposure of confidential secret or token Mailchimp API key [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.73` Exposure of confidential secret or token Mailgun public validation key [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.74` Exposure of confidential secret or token Mailgun private API token [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.75` Exposure of confidential secret or token Mailgun webhook signing key [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.76` Exposure of confidential secret or token MapBox API token [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.77` Exposure of confidential secret or token Mattermost Access Token [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.78` Exposure of confidential secret or token MessageBird API token [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.79` Exposure of confidential secret or token MessageBird client ID [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.80` Exposure of confidential secret or token Netlify Access Token [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.81` Exposure of confidential secret or token New Relic user API Key [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.82` Exposure of confidential secret or token New Relic user API ID [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.83` Exposure of confidential secret or token New Relic ingest browser API token [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.84` Exposure of confidential secret or token npm access token [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.85` Exposure of confidential secret or token Nytimes Access Token [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.86` Exposure of confidential secret or token Okta Access Token [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.87` Exposure of confidential secret or token Plaid Client ID [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.88` Exposure of confidential secret or token Plaid Secret key [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.89` Exposure of confidential secret or token Plaid API Token [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.90` Exposure of confidential secret or token PlanetScale password [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.91` Exposure of confidential secret or token PlanetScale API token [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.92` Exposure of confidential secret or token PlanetScale OAuth token [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.93` Exposure of confidential secret or token Postman API token [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.94` Exposure of confidential secret or token Private Key [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.95` Exposure of confidential secret or token Pulumi API token [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.96` Exposure of confidential secret or token PyPI upload token [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.97` Exposure of confidential secret or token Rubygem API token [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.98` Exposure of confidential secret or token RapidAPI Access Token [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.99` Exposure of confidential secret or token Sendbird Access ID [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.100` Exposure of confidential secret or token Sendbird Access Token [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.101` Exposure of confidential secret or token SendGrid API token [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.102` Exposure of confidential secret or token Sendinblue API token [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.103` Exposure of confidential secret or token Sentry Access Token [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.104` Exposure of confidential secret or token Shippo API token [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.105` Exposure of confidential secret or token Shopify access token [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.106` Exposure of confidential secret or token Shopify custom access token [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.107` Exposure of confidential secret or token Shopify private app access token [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.108` Exposure of confidential secret or token Shopify shared secret [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.109` Exposure of confidential secret or token Slack token [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.110` Exposure of confidential secret or token Slack Webhook [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.111` Exposure of confidential secret or token Stripe [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.112` Exposure of confidential secret or token Square Access Token [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.113` Exposure of confidential secret or token Squarespace Access Token [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.114` Exposure of confidential secret or token SumoLogic Access ID [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.115` Exposure of confidential secret or token SumoLogic Access Token [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.116` Exposure of confidential secret or token Travis CI Access Token [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.117` Exposure of confidential secret or token Twilio API Key [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.118` Exposure of confidential secret or token Twitch API token [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.119` Exposure of confidential secret or token Twitter API Key [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.120` Exposure of confidential secret or token Twitter API Secret [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.121` Exposure of confidential secret or token Twitter Access Token [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.122` Exposure of confidential secret or token Twitter Access Secret [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.123` Exposure of confidential secret or token Twitter Bearer Token [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.124` Exposure of confidential secret or token Typeform API token [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.125` Exposure of confidential secret or token Yandex API Key [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.126` Exposure of confidential secret or token Yandex AWS Access Token [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.127` Exposure of confidential secret or token Yandex Access Token [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
    - Add `798.128` Exposure of confidential secret or token Zendesk Secret Key [dast-cwe-checks!136](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/136)
- Replace ZAP rules [10023](https://www.zaproxy.org/docs/alerts/10023/) and [90022](https://www.zaproxy.org/docs/alerts/90022/) with [209.2](https://docs.gitlab.com/ee/user/application_security/dast/checks/209.2.html) in browser based scan (!624)

## v3.0.10
- Users can configure feature flags for browser based scans (!621)

## v3.0.9
- Upgrade browserker to version `0.0.87` (!617)
  - Fetch all headers for HTTP responses that are redirects [browserker!668](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/668)
  - Update the evidence on the `601.1` check to be the request that contains a redirect parameter [browserker!668](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/668)
  - Enable `16.10` passive check [browserker!670](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/670)
- Replace [ZAP rule 10038](https://www.zaproxy.org/docs/alerts/10038/) with:
  - [16.8](https://docs.gitlab.com/ee/user/application_security/dast/checks/16.8.html) in browser based scan (!617)
- Replace [ZAP rule 10055](https://www.zaproxy.org/docs/alerts/10055/) with:
  - [16.9](https://docs.gitlab.com/ee/user/application_security/dast/checks/16.9.html) in browser based scan (!617)
  - [16.10](https://docs.gitlab.com/ee/user/application_security/dast/checks/16.81.html) in browser based scan (!617)
- Upgrade browserker to version `0.0.88` (!620)
  - Users can configure feature flags [browserker!671](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/671)
  - Upgrade vulnerability checks to version `1.0.25` [browserker!679](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/679)
    - Update `798` checks to remove requirement for secrets or tokens to be encased in `\"` [dast-cwe-checks!126](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/126)
  - Upgrade vulnerability checks to version `1.0.26` [browserker!679](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/679)
    - Remove check `798.92` to avoid false positives [dast-cwe-checks!129](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/129)
  - Upgrade vulnerability checks to version `1.0.27` [browserker!679](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/679)
    - Update description for check `16.8` [dast-cwe-checks!130](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/130)
- Replace ZAP rules [10028](https://www.zaproxy.org/docs/alerts/10028/) and [20019](https://www.zaproxy.org/docs/alerts/20019/) with [601.1](https://docs.gitlab.com/ee/user/application_security/dast/checks/601.1.html) in browser based scan (!619)

## v3.0.8
- Upgrade browserker to version `0.0.86` (!616)
  - Enable 16.9 passive check [browserker!654](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/654)
  - Limit check 601.1 to GET requests [browserker!660](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/660)
  - Find attack vectors in URL-encoded form submit requests [browserker!661](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/661)
  - Ignore `16.1` check for responses without a body [browserker!662](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/662)

## v3.0.7
- Upgrade Browserker to version `0.0.84` (!611)
  - Upgrade vulnerability checks to version `1.0.20` [browserker!643](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/643)
    - Update `16.8` uniqueness template to reference matcher name [dast-cwe-checks!117](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/117)
    - Update checks `16.7` and `352.1` descriptions [dast-cwe-checks!114](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/114)
  - Enable `16.8` passive check [browserker!643](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/643)
  - Persist HTTP messages independently of navigation results [browserker!647](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/647)
- Upgrade Browserker to version `0.0.85` (!611)
  - Enable 209.1 passive check [browserker!657](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/657)
- Replace [ZAP rule 10023](https://www.zaproxy.org/docs/alerts/10023/) and [ZAP rule 90022](https://www.zaproxy.org/docs/alerts/90022/) with [209.1](https://docs.gitlab.com/ee/user/application_security/dast/checks/209.1.html) in browser based scan (!611)


## v3.0.6
- Upgrade Browserker to version `0.0.83` (!610)
  - Enable 319.1 passive check [browserker!641](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/641)
- Replace [ZAP rule 10040](https://www.zaproxy.org/docs/alerts/10040/) with [319.1](https://docs.gitlab.com/ee/user/application_security/dast/checks/319.1.html) in browser based scan (!610)

## v3.0.5
- Upgrade Browserker to version `0.0.82` (!609)
  - Fix error where scope is not checked when accessing a URL [browserker!645](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/645)

## v3.0.4
- Upgrade Browserker to version `0.0.81` (!608)
  - Upgrade reports to use the Secure Schema `14.1.2` [browserker!644](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/644)
- Remove `null` `vulnerabilities[].evidence.request` and `vulnerabilities[].evidence.response` fields to remove validation warnings (!607)
- Upgrade the Secure schema the outputted report conforms to to remove validation warnings (!607)

## v3.0.3
- Prevent placeholder request and response objects being added to the secure report (!606)
- Handle escape sequences in authentication configuration (!603)
- Upgrade Browserker to version `0.0.80` (!605)
  - Check `829.2` does not check URLs for findings when URL is out of scope [browserker!634](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/634)
  - Check `352.1` only produces findings for requests are a form submit [browserker!637](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/637)
- Replace ZAP rules [10202](https://www.zaproxy.org/docs/alerts/10202/) and [20012](https://www.zaproxy.org/docs/alerts/20012/) with [352.1](https://docs.gitlab.com/ee/user/application_security/dast/checks/352.1.html) in browser based scan (!605)

## v3.0.2
- Upgrade Browserker to version `0.0.79` (!602)
  - Don't check HTTP messages for findings when the request URL is not in scope [browserker!622](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/622)
  - Navigation results are sorted by StartURL and EndURL [browserker!584](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/584)
  - Don't check navigation results for findings when the load message URL is not in scope [browserker!626](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/626)
  - Add check `601.1` to verify URLs in query parameters are not followed [browserker!627](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/627)
  - Checking for a response body should not report an error [browserker!629](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/629)

## v3.0.1
- Upgrade Browserker to version `0.0.78` (!601)
  - Upgrade vulnerability checks to version `1.0.18` [browserker!618](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/618)
    - Update `16.1`, `16.2`, `16.3`, `16.4`, `16.5`, `16.6`, and `693.1` to require exact header names [dast-cwe-checks!106](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/106)
    - Updates `16.2` to be more restrictive on version matching [dast-cwe-checks!107](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/107)
    - Remove unnecessary `has_authentication_cookie` requirement from check `614.1` [dast-cwe-checks!105](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/105)
    - Remove unnecessary `has_authentication_cookie` requirement from check `1004.1` [dast-cwe-checks!105](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/105)
    - Update `352.1` uniqueness template so that findings will be created for each `request_path` and `request_method` [dast-cwe-checks!108](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/108)
    - Update `359.1` and `359.2` to bring inline with GitLab documentation guidelines [dast-cwe-checks!110](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/110)
  - Fix stability issues by caching resources when the entire HTTP message is parsed [browserker!621](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/621)
  - Upgrade vulnerability checks to version `1.0.19` [browserker!620](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/620)
    - Update `359.1` and `359.2` uniqueness template so that findings will be created for each `request_method` [dast-cwe-checks!112](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/112)
- Replace [ZAP rule 10010](https://www.zaproxy.org/docs/alerts/10010/) with [1004.1](https://docs.gitlab.com/ee/user/application_security/dast/checks/1004.1.html) in browser based scan (!588)

## v3.0.0
- Upgrade ZAP add-on `Active scanner rules` to [46.0.0](https://github.com/zaproxy/zap-extensions/releases/ascanrules-v46) (!598)
- Upgrade ZAP add-on `Active scanner rules (beta)` to [40.0.0](https://github.com/zaproxy/zap-extensions/releases/ascanrulesBeta-v40) (!598)
- Upgrade ZAP add-on `Automation Framework` to [0.15.0](https://github.com/zaproxy/zap-extensions/releases/automation-v0.15.0) (!598)
- Upgrade ZAP add-on `Common Library` to [1.9.0](https://github.com/zaproxy/zap-extensions/releases/commonlib-v1.9.0) (!598)
- Upgrade ZAP add-on `GraphQL Support` to [0.9.0](https://github.com/zaproxy/zap-extensions/releases/graphql-v0.9.0) (!598)
- Upgrade ZAP add-on `Network` to [0.2.0](https://github.com/zaproxy/zap-extensions/releases/network-v0.2.0) (!598)
- Upgrade ZAP add-on `OpenAPI Support` to [27.0.0](https://github.com/zaproxy/zap-extensions/releases/openapi-v27) (!598)
- Upgrade ZAP add-on `Passive scanner rules` to [40.0.0](https://github.com/zaproxy/zap-extensions/releases/pscanrules-v40) (!598)
- Upgrade ZAP add-on `Passive scanner rules (beta)` to [29.0.0](https://github.com/zaproxy/zap-extensions/releases/pscanrulesBeta-v29) (!598)
- Upgrade ZAP add-on `Report Generation` to [0.13.0](https://github.com/zaproxy/zap-extensions/releases/reports-v0.13.0) (!598)
- Upgrade ZAP add-on `Retire.js` to [0.11.0](https://github.com/zaproxy/zap-extensions/releases/retire-v0.11.0) (!598)
- Upgrade ZAP add-on `Script Console` to [30.0.0](https://github.com/zaproxy/zap-extensions/releases/scripts-v30) (!598)
- Upgrade ZAP add-on `Selenium` to [15.8.0](https://github.com/zaproxy/zap-extensions/releases/selenium-v15.8.0) (!598)
- Upgrade ZAP add-on `Linux WebDrivers` to [38.0.0](https://github.com/zaproxy/zap-extensions/releases/webdriverlinux-v38) (!598)
- Upgrade ZAP add-on `WebSockets` to [25.0.0](https://github.com/zaproxy/zap-extensions/releases/websocket-v25) (!598)

## v2.28.0
- Upgrade Browserker to version `0.0.77` (!599)
  - Don't create a finding when there is no HTTP message [browserker!617](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/617)
  - Update `352.1` passive check logic [browserker!616](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/616)

## v2.27.0
- Upgrade Browserker to version `0.0.76` (!597)
  - Upgrade vulnerability checks to version `1.0.17` [browserker!602](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/602)
    - Checks `16.2`, `16.3`, `16.4`, `16.5`, and `16.6` use a named matcher for report uniqueness [dast-cwe-checks!104](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/104)
  - Fix bug in response handling of cached headers [browserker!599](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/599)
  - Upgrade GCD to version `2.2.5` [browserker!606](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/606)
  - Enable `359.1` passive check [browserker!590](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/590)
  - Sort headers in vulnerabilities evidence to encourage deterministic results [browserker!610](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/610)
  - User can print orphan DevTools event summary when `LogRequestErrorReport` is turned on [browserker!605](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/605)
  - Fix error where building HTTP messages from resources prints a warning when DevTools events are not present [browserker!611](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/611)
  - Enable `359.2` passive check [browserker!609](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/609)
  - Fix `829.1` passive check to only match `link` tags with the `rel` attribute of `stylesheet` and `script` [browserker!604](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/604)
- Replace [ZAP rule 10062](https://www.zaproxy.org/docs/alerts/10062/) with [359.1](https://docs.gitlab.com/ee/user/application_security/dast/checks/359.1.html) and [359.2](https://docs.gitlab.com/ee/user/application_security/dast/checks/359.2.html) in browser based scan (!586)

## v2.26.0
- Upgrade Browserker to version `0.0.75` (!594)
  - Upgrade vulnerability checks to version `1.0.16` [browserker!596](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/596)
  - Update `1004.1` uniqueness template to fix incorrect variable from `request_url` to `request_path` [dast-cwe-checks!103](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/103)
  - Upgrade vulnerability checks to version `1.0.15` [browserker!596](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/596)
    - Add `16.8` Content-Security-Policy analysis [dast-cwe-checks!101](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/101)
    - Add `16.9` Content-Security-Policy-Report-Only analysis [dast-cwe-checks!101](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/101)
    - Add `16.10` Content-Security-Policy violations [dast-cwe-checks!101](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/101)
    - Update `1004.1` uniqueness template so that findings will be created for each `request_url` and `request_method` [dast-cwe-checks!102](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/102)
  - Upgrade vulnerability checks to version `1.0.14` [browserker!581](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/581)
    - Add `319.1` Mixed Content [dast-cwe-checks!97](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/97)
    - Update `829.1` uniqueness template so that findings will be created for each matching tag [dast-cwe-checks!99](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/99)
  - Matchers can return a non-applicable state to reduce false positives [browserker!587](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/587)
  - Add `request_method` placeholder to allow `Findings` to be created per request method [browserker!594](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/594)
  - Fix error where building HTTP messages from cached resources prints a warning to the log [browserker!600](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/600)
  - Fix error that caused a panic when persisting navigations captured during authentication [browserker!601](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/601)
  - User can configure `LogRequestErrorReport` to output a report containing missing DevTool events [browserker!598](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/598)
- Add `DAST_BROWSER_LOG_REQUEST_ERROR_REPORT` to allows users to output report containing requests that did not load correctly (!594)
- Reinstate check `614.1` as the false positive issue has been resolved (!595)

## v2.25.0
- Upgrade Browserker to version `0.0.73` (!589)
  - Persist navigation results captured during the authentication phase [browserker!558](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/558)
  - Enable `829.2` passive check [browserker!573](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/573)
  - Associate all response headers with a HTTP response [browserker!576](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/576)
  - Check navigations captured during authentication for vulnerability findings [browserker!577](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/577)
  - Enable `829.1` passive check [browserker!569](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/569)
  - Upgrade vulnerability checks to version `1.0.13` [browserker!579](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/579)
    - Remove unnecessary matchers from `598.1` [dast-cwe-checks!95](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/95)
  - Upgrade vulnerability checks to version `1.0.12` [browserker!579](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/579)
    - Add `metadata` to schema to allow for arbitrary key-values on matchers [dast-cwe-checks!91](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/91)
    - Add `209.1` Generation of error message containing sensitive information [dast-cwe-checks!82](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/82)
    - Add `209.2` Generation of database error message containing sensitive information [dast-cwe-checks!82](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/82)
    - Change `829.2` uniqueness template to use console text [dast-cwe-checks!93](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/93)
  - Authentication cookie attribute matches only match for responses that set the authentication cookie [browserker!579](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/579)
- Fix issue where the request headers were used as response headers in finding evidence (!591)
- Upgrade Browserker to version `0.0.74` (!590)
  - Sort aggregated vulnerabilities by summary for more deterministic results [browserker!578](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/578)
- Replace [ZAP rule 10017](https://www.zaproxy.org/docs/alerts/10017/) and [ZAP rule 90003](https://www.zaproxy.org/docs/alerts/90003/) with [829.1](https://docs.gitlab.com/ee/user/application_security/dast/checks/829.1.html) and [829.2](https://docs.gitlab.com/ee/user/application_security/dast/checks/829.2.html) in browser based scan (!590)

## v2.24.0
- Split out `598.1` passive check from into two distinct checks (`598.1` and `598.3`)
- Upgrade Browserker to version `0.0.72` (!587)
  - Add support for `request_url_contains_password` matcher in vulnerability checks [browserker!561](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/561)
  - Add support for `response_has_valid_strict_transport_security_header` matcher in vulnerability checks [browserker!561](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/561)
  - Enable `598.3` passive check [browserker!560](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/560)
  - Upgrade vulnerability checks to version `1.0.11` [browserker!560](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/560)
    - Convert `16.7` and `598.{1,2,3}` to standard checks [dast-cwe-checks!90](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/90)

## v2.23.0
- Replace [ZAP rule 10024](https://www.zaproxy.org/docs/alerts/10024/) with [598.2](https://docs.gitlab.com/ee/user/application_security/dast/checks/598.2.html) in browser based scan (!586)
- Upgrade Browserker to version `0.0.71` (!586)
  - Enable `598.2` passive check [browserker!553](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/553)
  - Upgrade vulnerability checks to version `1.0.10` [browserker!553](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/553)
    - Add `598.3` Use of GET request method with sensitive query strings (Authorization header details) [dast-cwe-checks!86](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/86)
    - Convert `598.2` to implementer defined check [dast-cwe-checks!87](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/87)

## v2.22.0
- Replace [ZAP rule 3](https://www.zaproxy.org/docs/alerts/3/) with [598.1](https://docs.gitlab.com/ee/user/application_security/dast/checks/598.1.html) in browser based scan (!543)

## v2.21.0
- Upgrade Browserker to version `0.0.70` (!583)
  - Upgrade vulnerability checks to version `1.0.9` [browserker!542](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/542)
    - Update 614.1 uniqueness template so that findings will be created for each matching cookie [dast-cwe-checks!80](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/80)
    - Update 1004.1 uniqueness template so that findings will be created for each matching cookie [dast-cwe-checks!80](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/80)
  - User can add debug details of vulnerability findings to the Secure report [browserker!549](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/549)
- Add `DAST_BROWSER_FILE_LOG_PATH` to allows users to configure where the browser-based scan file log is written (!583)
- Add `DAST_BROWSER_INCLUDE_ONLY_RULES` to allows users to configure which rules are run by the browser-based scan (!583)
- Add `DAST_BROWSER_SECURE_REPORT_EXTRA_INFO` to allows users to add debug information to vulnerability findings (!583)
- Upgrade JPype to version `1.3.0` (!581)

## v2.20.0
- Add additional logging around JVM actions (!579)
- Log the Python segmentation fault stack trace when `DAST_DEBUG` is `true` (!580)
- Add `DAST_BROWSER_CRAWL_GRAPH` to generate a svg (!560)

## v2.19.0
- Disable check `614.1` as it is creating a false positive on every request (!578)

## v2.18.0
- Users can configure the names of authentication cookies for a browser-based scan using `DAST_AUTH_COOKIES` (!575)
- Upgrade Browserker to version 0.0.69 (!577)
  - A passive check can create multiple findings [browserker!524](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/524)
  - Add support for `has_authorization_header` matcher in vulnerability checks [browserker!530](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/530)
  - Add support for `is_authenticated` matcher in vulnerability checks [browserker!526](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/526)
  - Add support for `request_url_contains_session_id` matcher in vulnerability checks [browserker!532](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/532)
  - Add support for `request_url_contains_authorization_header_value` matcher in vulnerability checks [browserker!535](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/535)
  - Each finding is saved in a separate transaction to avoid large transaction errors [browserker!533](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/533)
  - Limit the number of vulnerability findings in `gl-dast-report.json` to 100,000 [browserker!527](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/527)
  - Log when the number of vulnerability findings is limited in the `gl-dast-report.json` [browserker!528](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/528)
  - Add support for `request_form_value` matcher in vulnerability checks [browserker!539](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/539)
  - Matchers can partially match content so they can be combined with `not` matchers to create findings [browserker!537](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/537)
  - Add support for `has_request_body` matcher in vulnerability checks [browserker!538](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/538)
  - Add support for `has_request_query_parameters` matcher in vulnerability checks [browserker!538](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/538)
  - Enable `598.1` passive check [browserker!536](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/536)
  - Upgrade vulnerability checks to version `1.0.8` [browserker!536](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/536)
    - Convert 598.1 from active to passive check [dast-cwe-checks!77](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/77)
  - User can generate the crawl graph as an SVG [browserker!507](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/507)

## v2.17.0
- Limit the number of reported vulnerabilities in `gl-dast-report.json` to 100,000 (!573)
- Sort browser-based detected vulnerabilities and ZAP detected vulnerabilities together (!572)
- Upgrade ZAP add-on `Active scanner rules` to [44.0.0](https://github.com/zaproxy/zap-extensions/releases/ascanrules-v44) (!571)
- Upgrade ZAP add-on `Automation Framework` to [0.12.0](https://github.com/zaproxy/zap-extensions/releases/automation-v0.12.0) (!571)
- Upgrade ZAP add-on `Call Home` to [0.3.0](https://github.com/zaproxy/zap-extensions/releases/callhome-v0.3.0) (!571)
- Upgrade ZAP add-on `Common Library` to [1.7.0](https://github.com/zaproxy/zap-extensions/releases/commonlib-v1.7.0) (!571)
- Upgrade ZAP add-on `Fuzzer` to [13.6.0](https://github.com/zaproxy/zap-extensions/releases/fuzz-v13.6.0) (!571)
- Upgrade ZAP add-on `GraphQL Support` to [0.8.0](https://github.com/zaproxy/zap-extensions/releases/graphql-v0.8.0) (!571)
- Upgrade ZAP add-on `Import files containing URLs` to [9.0.0](https://github.com/zaproxy/zap-extensions/releases/importurls-v9) (!571)
- Upgrade ZAP add-on `Network` to [0.1.0](https://github.com/zaproxy/zap-extensions/releases/network-v0.1.0) (!571)
- Upgrade ZAP add-on `OAST Support` to [0.10.0](https://github.com/zaproxy/zap-extensions/releases/oast-v0.10.0) (!571)
- Upgrade ZAP add-on `OpenAPI Support` to [26.0.0](https://github.com/zaproxy/zap-extensions/releases/openapi-v26) (!571)
- Upgrade ZAP add-on `Passive scanner rules` to [38.0.0](https://github.com/zaproxy/zap-extensions/releases/pscanrules-v38) (!571)
- Upgrade ZAP add-on `Report Generation` to [0.12.0](https://github.com/zaproxy/zap-extensions/releases/reports-v0.12.0) (!571)
- Upgrade ZAP add-on `Retire.js` to [0.10.0](https://github.com/zaproxy/zap-extensions/releases/retire-v0.10.0) (!571)
- Upgrade ZAP add-on `Save Raw Message` to [7.0.0](https://github.com/zaproxy/zap-extensions/releases/saverawmessage-v7) (!571)
- Upgrade ZAP add-on `Save XML Message` to [0.3.0](https://github.com/zaproxy/zap-extensions/releases/savexmlmessage-v0.3.0) (!571)
- Upgrade ZAP add-on `Selenium` to [15.7.0](https://github.com/zaproxy/zap-extensions/releases/selenium-v15.7.0) (!571)
- Upgrade ZAP add-on `SOAP Support` to [13.0.0](https://github.com/zaproxy/zap-extensions/releases/soap-v13) (!571)
- Upgrade ZAP add-on `Linux WebDrivers` to [35.0.0](https://github.com/zaproxy/zap-extensions/releases/webdriverlinux-v35) (!571)
- Upgrade Browserker to version 0.0.68 (!574)
  - Upgrade vulnerability checks to version `1.0.7` [browserker!520](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/520)
    - Update capitalization based matchers to use `(?i)` [dast-cwe-checks!74](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/74)
    - Add `request_body_parameters` to `match_locations` [dast-cwe-checks!72](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/72)
    - Add `request_body_parameter_name` to `match_locations` [dast-cwe-checks!72](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/72)
    - Add `request_body_parameter_value` to `match_locations` [dast-cwe-checks!72](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/72)
    - Field `report_uniqueness.template` is required in the CWE checks schema [dast-cwe-checks!75](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/75)
    - Add uniqueness template of request path to check `16.7` [dast-cwe-checks!75](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/75)
    - Add uniqueness template of request path to check `601.1` [dast-cwe-checks!75](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/75)
    - Add uniqueness template of request path to check `829.1` [dast-cwe-checks!75](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/75)
    - Add uniqueness template of request path to check `829.2` [dast-cwe-checks!75](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/75)
  - Restrict `response_body` matcher by disallowing more binary content types [browserker!474](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/474)
  - Allow user to configure the name of authentication cookies using `Cookies` in `AuthDetails` TOML [browserker!509](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/509)
  - Run passive checks for HTTP messages where there is no response [browserker!517](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/517)
- Upgrade Browserker to version 0.0.67 (!570)
  - Fetch cookies from all domains [browserker!500](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/500)
  - Add support for passive check `16.7` [browserker!506](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/506)
- Upgrade Browserker to version 0.0.66 (!568)
  - Upgrade vulnerability checks to version `1.0.6` [browserker!501](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/501)
    - Define the matcher logic for the `200.1` check [dast-cwe-checks!73](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/73)
  - Change Debian repository to snapshot.debian.org to pin libraries and versions for ubuntu images [browserker!508](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/508)
- Upgrade Browserker to version 0.0.65 (!568)
  - Add support for basic and digest authentication, configurable via `AuthType: \"basic-digest\"` in `AuthDetails` TOML [browserker!481](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/481)
  - Update Chromium to 98.0.4758.80-1 [browserker!496](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/496)
  - Parse matcher `name` when reading check definitions [browserker!491](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/491)
  - Use matcher `name` to generate uniqueness templates [browserker!495](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/495)
  - Upgrade vulnerability checks to version `1.0.5` [browserker!467](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/467)
    - Add uniqueness template using request path and private IP address to check 200.1 [dast-cwe-checks!70](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/70)
    - Add uniqueness template of request path to check 352.1 [dast-cwe-checks!70](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/70)
    - Remove requirements from check 1004.1 [dast-cwe-checks!71](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/71)
    - Update check 1004.1 to use has_authentication_cookie instead of requirements [dast-cwe-checks!71](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/71)
    - Update check 1004.1 to use authentication_cookie_attribute [dast-cwe-checks!71](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/71)

## v2.16.0
- Upgrade Browserker to version 0.0.64 (!566)
  - Remove requirement for the cookie to be in the HTTP request in the `has_authentication_cookie` matcher [browserker!490](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/490)
  - Parse `UniqueBy` templates prior to the scan starting [browserker!492](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/492)
  - Upgrade Go to version `1.17.7` to include latest security patches [browserker!494](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/494)

## v2.15.0
- Upgrade Browserker to version 0.0.63 (!565)
  - Add support for `request_url` matcher in vulnerability checks [browserker!475](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/475)
  - Add support for `session_cookie_attribute` matcher in vulnerability checks [browserker!456](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/456)
  - Remove `session_cookie_attribute` matcher in favor of `authentication_cookie_attribute` [browserker!466](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/465) [browserker!482](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/482)
  - Remove `has-session-cookie` matcher in favor of `has_authentication_cookie` [browserker!465](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/465) [browserker!467](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/467) [browserker!482](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/482)
  - Remove unused `cookie_attribute` matcher [browserker!460](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/460)
  - Upgrade vulnerability checks to version `1.0.3` [browserker!463](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/463)
  - Upgrade vulnerability checks to version `1.0.4` [browserker!476](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/476)
- Replace [ZAP rule 10011](https://www.zaproxy.org/docs/alerts/10011/) with [614.1](https://docs.gitlab.com/ee/user/application_security/dast/checks/614.1.html) in browser based scan (!565)

## v2.14.0
- Add `DAST_BROWSER_FILE_LOG` to allows users to configure the log written to file (!563)
- Add `DAST_BROWSER_DEVTOOLS_LOG` to allows users to log Chrome DevTools messages (!563)

## v2.14.0
- Upgrade Browserker to version 0.0.62 (!561)
  - Log when the Crawl Report is skipped [browserker!446](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/446)
  - Sort the `vulnerabilities[].evidence.summary` field in the Secure report [browserker!450](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/450)
  - Add support for `has_session_cookie` matcher in vulnerability checks [browserker!452](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/452)
  - Add support for passive check `201.1` [browserker!426](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/426)
  - Add support for `request_body` matcher in vulnerability checks [browserker!453](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/453)
- Replace [ZAP rule 10033](https://www.zaproxy.org/docs/alerts/10033/) with [548.1](https://docs.gitlab.com/ee/user/application_security/dast/checks/548.1.html) in browser based scan (!561)
- Replace [ZAP rule 2](https://www.zaproxy.org/docs/alerts/2/) with [200.1](https://docs.gitlab.com/ee/user/application_security/dast/checks/200.1.html) in browser based scan (!562)

## v2.13.0
- Upgrade Browserker to version 0.0.60 (!555)
  - Upgrade vulnerability checks to version `1.0.1` [browserker!440](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/440)
    - Add `requirement` matcher [dast-cwe-checks!58](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/58)
    - Add `has_response` `requirement` to `16.1` [dast-cwe-checks!58](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/58)
  - Rename `has-response` requirement matcher to `has_response` (!440)
- Upgrade Browserker to version 0.0.61 (!555)
  - Associate all request headers and non-blocked cookies with a HTTP request [browserker!442](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/442)
  - Add support for `request_method` matcher in vulnerability checks [browserker!438](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/438)
  - Add support for `request_parameter_name` matcher in vulnerability checks [browserker!438](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/438)
  - All DevTools messages for a DevTools Domain can be logged using a single line of configuration [browserker!444](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/444)
  - Upgrade vulnerability checks to version `1.0.2` [browserker!445](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/445)
    - Remove markdown from titles [dast-cwe-checks!55](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/55)
    - Don't match `16.1` on `OPTIONS` [dast-cwe-checks!60](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/60)
- Replace [ZAP rule 10019](https://www.zaproxy.org/docs/alerts/10019/) with [16.1](https://docs.gitlab.com/ee/user/application_security/dast/checks/16.1.html) in browser based scan (!555)

## v2.12.0
- User must supply Mutual TLS client certificate as a base64 encoded variable `DAST_PKCS12_CERTIFICATE_BASE64` (!556)
- Aggregate findings found by the [Content Security Policy rule](https://www.zaproxy.org/docs/alerts/10055) (!558)
- Mutual TLS client certificates can have an empty password (!557)
- Upgrade Browserker to version 0.0.58 (!559)
  - Remove duplicate URLs in Secure report `vulnerabilities[].details.urls.items[]` [browserker!424](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/424)
  - Ensure Secure report field `vulnerabilities[]` is never null [browserker!424](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/424)
  - Ensure Secure report field `vulnerabilities[].links` is never null [browserker!424](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/424)
  - Sort findings prior to Secure report generation for more deterministic results [browserker!429](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/429)
  - Upgrade vulnerability checks to version `1.0.0` [browserker!418](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/418)
    - Add `548.1` [dast-cwe-checks!45](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/45)
    - Add execution mode `once_per_path_excluding_last_segment` [dast-cwe-checks!49](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/49)
    - Remove execution modes `once_per_file` and `once_per_request` [dast-cwe-checks!49](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/49)
    - Add `request_path_excluding_last_segment` as match location [dast-cwe-checks!49](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/49)
    - Add `200.1` [dast-cwe-checks!50](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/merge_requests/50)
- Upgrade Browserker to version 0.0.59 (!559)
  - Add support for requirement matchers in vulnerability checks [browserker!437](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/437)
  - Add support for `has-response` requirement matcher in vulnerability checks [browserker!437](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/437)
  - Add Crawl Report [browserker!384](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/384)
- Add `DAST_BROWSER_CRAWL_REPORT` to generate a report showing the screenshots visited during crawl phase (!559)

## v2.11.0
- Update ZAP to [w2022-01-04](https://github.com/zaproxy/zaproxy/releases/tag/w2022-01-04/) (!547) (!552) (!553)
- Enable client certificates for mutual TLS in legacy scans (!540)
- Upgrade ZAP add-on `Active scanner rules (beta)` to [32.0.0](https://github.com/zaproxy/zap-extensions/releases/ascanrulesBeta-v32) (!352)
- Upgrade ZAP add-on `Access Control Testing` to [7](https://github.com/zaproxy/zap-extensions/releases/accessControl-v7/) (!549)
- Upgrade ZAP add-on `Alert Filters` to [13](https://github.com/zaproxy/zap-extensions/releases/alertFilters-v13/) (!549)
- Upgrade ZAP add-on `Active scanner rules` to [43](https://github.com/zaproxy/zap-extensions/releases/ascanrules-v43/) (!549)
- Upgrade ZAP add-on `Active scanner rules (beta)` to [v39](https://github.com/zaproxy/zap-extensions/releases/ascanrulesBeta-v39/) (!549)
- Upgrade ZAP add-on `Forced Browse` to [11](https://github.com/zaproxy/zap-extensions/releases/bruteforce-v11/) (!549)
- Upgrade ZAP add-on `Call Home` to [0.1.0](https://github.com/zaproxy/zap-extensions/releases/callhome-v0.1.0/) (!549) (!553)
- Upgrade ZAP add-on `Common Library` to [1.6.0](https://github.com/zaproxy/zap-extensions/releases/commonlib-v1.6.0/) (!549)
- Upgrade ZAP add-on `Diff` to [11](https://github.com/zaproxy/zap-extensions/releases/diff-v11/) (!549)
- Upgrade ZAP add-on `Directory List v1.0` to [5](https://github.com/zaproxy/zap-extensions/releases/directorylistv1-v5/) (!549)
- Upgrade ZAP add-on `DOM XSS Active scanner rule` to [12](https://github.com/zaproxy/zap-extensions/releases/domxss-v12/) (!549)
- Upgrade ZAP add-on `Encoder` to [0.6.0](https://github.com/zaproxy/zap-extensions/releases/encoder-v0.6.0/) (!549)
- Upgrade ZAP add-on `Form Handler` to [4](https://github.com/zaproxy/zap-extensions/releases/formhandler-v4/) (!549)
- Upgrade ZAP add-on `Fuzzer` to [13.5.0](https://github.com/zaproxy/zap-extensions/releases/fuzz-v13.5.0/) (!549)
- Upgrade ZAP add-on `FuzzDB Files` to [8](https://github.com/zaproxy/zap-extensions/releases/fuzzdb-v8/) (!549)
- Upgrade ZAP add-on `Getting Started with ZAP Guide` to [13](https://github.com/zaproxy/zap-extensions/releases/gettingStarted-v13/) (!549)
- Upgrade ZAP add-on `GraalVM JavaScript` to [0.2.0](https://github.com/zaproxy/zap-extensions/releases/graaljs-v0.2.0/) (!549)
- Upgrade ZAP add-on `GraphQL Support` to [0.7.0](https://github.com/zaproxy/zap-extensions/releases/graphql-v0.7.0/) (!549)
- Upgrade ZAP add-on `Help - English` to [14](https://github.com/zaproxy/zap-core-help/releases/help-v14/) (!549)
- Upgrade ZAP add-on `HUD - Heads Up Display` to [0.13.0](https://github.com/zaproxy/zap-hud/releases/v0.13.0/) (!549)
- Upgrade ZAP add-on `Import files containing URLs` to [8](https://github.com/zaproxy/zap-extensions/releases/importurls-v8/) (!549)
- Upgrade ZAP add-on `Invoke Applications` to [11](https://github.com/zaproxy/zap-extensions/releases/invoke-v11/) (!549)
- Upgrade ZAP add-on `Network` to [0.0.1](https://github.com/zaproxy/zap-extensions/releases/network-v0.0.1/) (!549)
- Upgrade ZAP add-on `OAST Support` to [0.7.0](https://github.com/zaproxy/zap-extensions/releases/oast-v0.7.0/) (!549)
- Upgrade ZAP add-on `Online menus` to [9](https://github.com/zaproxy/zap-extensions/releases/onlineMenu-v9/) (!549)
- Upgrade ZAP add-on `OpenAPI Support` to [24](https://github.com/zaproxy/zap-extensions/releases/openapi-v24/) (!549)
- Upgrade ZAP add-on `Plug-n-Hack Configuration` to [12](https://github.com/zaproxy/zap-extensions/releases/plugnhack-v12/) (!549)
- Upgrade ZAP add-on `Port Scanner` to [9](https://github.com/zaproxy/zap-extensions/releases/portscan-v9/) (!549)
- Upgrade ZAP add-on `Passive scanner rules` to [37](https://github.com/zaproxy/zap-extensions/releases/pscanrules-v37/) (!549)
- Upgrade ZAP add-on `Passive scanner rules (beta)` to [28](https://github.com/zaproxy/zap-extensions/releases/pscanrulesBeta-v28/) (!549)
- Upgrade ZAP add-on `Quick Start` to [33](https://github.com/zaproxy/zap-extensions/releases/quickstart-v33/) (!549)
- Upgrade ZAP add-on `Replacer` to [9](https://github.com/zaproxy/zap-extensions/releases/replacer-v9/) (!549)
- Upgrade ZAP add-on `Report Generation` to [0.10.0](https://github.com/zaproxy/zap-extensions/releases/reports-v0.10.0/) (!549)
- Upgrade ZAP add-on `Retest` to [0.2.0](https://github.com/zaproxy/zap-extensions/releases/retest-v0.2.0/) (!549)
- Upgrade ZAP add-on `Retire.js` to [0.9.0](https://github.com/zaproxy/zap-extensions/releases/retire-v0.9.0/) (!549)
- Upgrade ZAP add-on `Reveal` to [4](https://github.com/zaproxy/zap-extensions/releases/reveal-v4/) (!549)
- Upgrade ZAP add-on `Save Raw Message` to [6](https://github.com/zaproxy/zap-extensions/releases/saverawmessage-v6/) (!549)
- Upgrade ZAP add-on `Save XML Message` to [0.2.0](https://github.com/zaproxy/zap-extensions/releases/savexmlmessage-v0.2.0/) (!549)
- Upgrade ZAP add-on `Script Console` to [29](https://github.com/zaproxy/zap-extensions/releases/scripts-v29/) (!549)
- Upgrade ZAP add-on `Selenium` to [15.6.0](https://github.com/zaproxy/zap-extensions/releases/selenium-v15.6.0/) (!549)
- Upgrade ZAP add-on `SOAP Support` to [12](https://github.com/zaproxy/zap-extensions/releases/soap-v12/) (!549)
- Upgrade ZAP add-on `Ajax Spider` to [23.7.0](https://github.com/zaproxy/zap-extensions/releases/spiderAjax-v23.7.0/) (!549)
- Upgrade ZAP add-on `Tips and Tricks` to [9](https://github.com/zaproxy/zap-extensions/releases/tips-v9/) (!549)
- Upgrade ZAP add-on `Linux WebDrivers` to [34](https://github.com/zaproxy/zap-extensions/releases/webdriverlinux-v34/) (!549) (!553)
- Upgrade ZAP add-on `WebSockets` to [24](https://github.com/zaproxy/zap-extensions/releases/websocket-v24/) (!549)
- Upgrade ZAP add-on `Zest - Graphical Security Scripting Language` to [35](https://github.com/zaproxy/zap-extensions/releases/zest-v35/) (!549)
- Upgrade Browserker to version 0.0.56 (!550)
  - Add support for request_file in vulnerability checks uniqueBy.template [browserker!394](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/394)
  - Remove libcanberra-gtk* python* libpython* from the Docker image [browserker!399](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/399)
- Upgrade Browserker to version 0.0.57 (!550)
  - Add support for the SecureReport option in the TOML configuration [browserker!400](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/400)
  - Fix bug in redirect handling [browserker!410](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/410)
  - Add support for capturing security event logs from browser [browserker!415](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/415)
  - Add support for CWE check cookie_attribute matcher [browserker!396](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/396)
  - Add support for request_path_excluding_last_segment in vulnerability checks uniqueBy.template [browserker!409](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/409)
  - Add support for execution modes in checks [browserker!412](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/412)

## v2.10.0
- Disable Log4j message lookups to mitigate vulnerability CVE-2021-44228 (!541)

## v2.9.0
- Upgrade Browserker to version 0.0.55 (!537)
  - Fail a navigation if the load response is empty [browserker!385](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/385)
  - Add `PageReadySelector` to allows users to define an element that should be visible before the scan continues [browserker!389](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/389)
- Add `DAST_BROWSER_PAGE_READY_SELECTOR` to allows users to define an element that should be visible before the scan continues (!537)

## v2.8.0
- Upgrade Browserker to version 0.0.54 (!536)
  - Add support for `AlwaysRelogin` which when set to `true` forces the browser to relogin for every navigation path [browserker!387](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/387)
- Add `DAST_BROWSER_ALWAYS_RELOGIN` to force browser-based scans to relogin for every navigation path (!536)

## v2.7.0
- Upgrade Browserker to version 0.0.53 (!535)
  - Upgrade vulnerability checks to version `0.0.6` [browserker!382](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/382)
  - Enables new checks [browserker!382](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/382)
    - [16.3](https://docs.gitlab.com/ee/user/application_security/dast/checks/16.3.html)
    - [16.4](https://docs.gitlab.com/ee/user/application_security/dast/checks/16.4.html)
    - [16.5](https://docs.gitlab.com/ee/user/application_security/dast/checks/16.5.html)
    - [16.6](https://docs.gitlab.com/ee/user/application_security/dast/checks/16.6.html)
- Replace [ZAP rule 10037](https://www.zaproxy.org/docs/alerts/10037/) with [16.3](https://docs.gitlab.com/ee/user/application_security/dast/checks/16.3.html) in browser based scan (!535)
- Replace [ZAP rule 10039](https://www.zaproxy.org/docs/alerts/10036/) with [16.4](https://docs.gitlab.com/ee/user/application_security/dast/checks/16.4.html) in browser based scan (!535)
- Replace [ZAP rule 10061](https://www.zaproxy.org/docs/alerts/10036/) with:
  - [16.5](https://docs.gitlab.com/ee/user/application_security/dast/checks/16.5.html) in browser based scan (!535)
  - [16.6](https://docs.gitlab.com/ee/user/application_security/dast/checks/16.6.html) in browser based scan (!535)

## v2.6.0
- Upgrade Browserker to version 0.0.52 (!534)
  - Fix bug in secure report `vulnerabilities[]` sort order [browserker!381](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/381)

## v2.5.0
- Upgrade Browserker to version 0.0.51 (!532)
  - Fix bug that prevented all timeout configuration options from being overridden [browserker!380](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/380)
- Replace [ZAP rule 10036](https://www.zaproxy.org/docs/alerts/10036/) with [16.2](https://docs.gitlab.com/ee/user/application_security/dast/checks/16.2.html) in browser based scan (!526)
- Upgrade Browserker to version 0.0.50 (!529)
  - Update badger dependency to v3 [browserker!376](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/376)
  - Update secure report to sort `vulnerabilities[]` [browserker!379](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/379)
- Upgrade Browserker to version 0.0.49 (!529)
  - Fix authentication bug that prevented login submit button from being clicked [browserker!375](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/375)
- Upgrade Browserker to version 0.0.48 (!529)
  - Add support for `response_header_value` in check template [browserker!363](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/363)

## v2.4.0
- Upgrade Browserker to version 0.0.47 (!525)
  - Upgrade vulnerability checks to version `0.0.4` [browserker!373](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/373)
  - Add support for CWE check `name_value_match` type and `request_headers` matcher [browserker!373](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/373)

## v2.3.0
- Upgrade Browserker to version 0.0.46 (!524)
  - Update vulnerability scanner to only run checks on HTTP requests [browserker!369](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/369)
  - Fix bug that allowed `data:` URLs to be cached [browserker!371](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/371)
  - Update `vulnerabilities[].evidence.summary` to truncate when the field exceeds 20000 characters in length [browserker!372](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/372)

## v2.2.0
- Upgrade Browserker to version 0.0.45 (!522)
  - Fix a bug where the submit button was not found due to dynamically modified elements in login form [browserker!366](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/366)

## v2.1.1
- Upgrade Browserker to version 0.0.44 (!521)
  - Update `vulnerabilities[].identifiers[].url` to link to check documentation on GitLab.com [browserker!361](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/361)
  - Upgrade vulnerability checks to version `0.0.3` [browserker!361](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/361)

## v2.1.0
- Replace ZAP rule [X-Content-Type-Options Header Missing](https://www.zaproxy.org/docs/alerts/10021/) with [Missing X-Content-Type-Options: nosniff](https://gitlab.com/gitlab-org/security-products/dast-cwe-checks/-/blob/2c353e817df629928679bca3d977555777740e95/checks/693/693.1.yaml) for all Browser Based DAST scans (!518)

## v2.0.11
- Upgrade Browserker to version 0.0.43 (!520)
  - Add `vulnerabilities.links` to the secure report [browserker!356](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/356)
  - Add `vulnerabilities.severity` to the secure report [browserker!358](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/358)

## v2.0.10
- Upgrade Browserker to version 0.0.42 (!517)
  - Add `vulnerabilities[].discovered_at` to the secure report [browserker!346](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/346)
  - Add `vulnerabilities[].id` to the secure report [browserker!350](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/350)
  - Fix a potential race closing the browser [browserker!351](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/351)
  - Add `vulnerabilities.evidence.summary` to secure report [browserker!348](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/348)
  - Add `scan.analyzer` to the secure report [browserker!349](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/349)
  - Add `vulnerabilities.solution` to the secure report [browserker!354](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/354)
  - Add vulnerabilities.message to the secure report [browserker!355](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/355)

## v2.0.9
- Upgrade Browserker to version 0.0.41 (!505)
  - Add `vulnerabilities[].location.hostname` to the secure report [browserker!337](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/337)
  - Add `vulnerabilities[].location.method` to the secure report [browserker!337](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/337)
  - Add `vulnerabilities[].location.param` to the secure report [browserker!337](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/337)
  - Add `vulnerabilities[].location.path` to the secure report [browserker!337](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/337)
  - Add `scan.status` to the secure report [browserker!338](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/338)
  - Add `scan.type` to the secure report [browserker!338](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/338)
  - Sort `scan.scanned_resources[]` by `scan.scanned_resources[].url` and `scan.scanned_resources[].method` [browserker!341](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/341)
  - Report noisy vulnerabilities as a single finding [browserker!345](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/345)
- Upgrade ZAP add-on `Linux WebDrivers` to [29.0.0](https://github.com/zaproxy/zap-extensions/releases/webdriverlinux-v29) (!506)
- Treat HTTP `401` response as successful during probe (!511)

## v2.0.8
- Remove Firefox and Gecko from DAST image (!502)
- Update `vulnerabilities[].discovered_at` to be empty when reported time is unknown (!500)
- Add `jq` to DAST image (!503)

## v2.0.7
- Upgrade Browserker to version 0.0.38 (!499)
  - Escape backslash characters when setting local/session storage [browserker!281](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/281)
  - Allow user to configure logging for Chrome Dev Tool interactions [browserker!282](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/282)
  - Fix a bug where network messages were coming in out of order [browserker!280](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/280)
  - Add an HTTP response caching mechanism on by default [browserker!278](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/278)
  - Allow user to configure disabling cache mechanism with `--disablecache` [browserker!278](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/278)
  - Update GCD to version `2.2.1` for supporting ChromeResponseErrors as go errors [browserker!289](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/289)
- Upgrade Browserker to version 0.0.39 (!499)
  - Incorporate GitLab vulnerability checks into Browserker for future parsing and execution [browserker!290](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/290)
  - Log vulnerability definitions that are not supported [browserker!298](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/298)
  - Add `vulnerabilities[].confidence` hardcoded to `Medium` to the secure report [browserker!300](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/300)
  - Allow user to log the GoRoutineID with each log message [browserker!305](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/305)
  - Add `vulnerabilities[].cve` to the secure report [browserker!302](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/302)
  - Add `vulnerabilities[].evidence.request.url` to the secure report [browserker!304](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/304)
  - Add `vulnerabilities[].evidence.request.method` to the secure report [browserker!304](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/304)
  - Add `vulnerabilities[].evidence.request.headers.name` to the secure report [browserker!304](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/304)
  - Add `vulnerabilities[].evidence.request.headers.value` to the secure report [browserker!304](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/304)
  - Add `vulnerabilities[].evidence.response.reason_phrase` to the secure report [browserker!304](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/304)
  - Add `vulnerabilities[].evidence.response.status_code` to the secure report [browserker!304](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/304)
  - Add `vulnerabilities[].evidence.response.headers.name` to the secure report [browserker!304](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/304)
  - Add `vulnerabilities[].evidence.response.headers.value` to the secure report [browserker!304](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/304)
  - Fix a bug where elements were not properly found due to document updates [browserker!307](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/307)
  - Allow users to configure which vulnerability checks run with `OnlyIncludeChecks` [browserker!312](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/312)
  - Fix a bug where `DisableCache` was not being read [browserker!319](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/319)
- Upgrade Browserker to version 0.0.40 (!499)
  - Add support for `request_path` in vulnerability checks `uniqueBy.template` [browserker!318](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/318)
  - Add `version` to the secure report [browserker!328](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/328)
  - Add `vulnerabilities[].scanner.id` to the secure report [browserker!328](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/328)
  - Add `vulnerabilities[].scanner.name` to the secure report [browserker!328](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/328)
  - Process HTTP messages when they are confirmed loaded by Chromium [browserker!325](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/325)
  - Added `ng-click` to the list of attributes to hash on for determining uniqueness in crawling [browserker!329](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/329)
  - Allow users to configure which element attributes should be used for determining uniqueness in crawling with `CustomHashAttributes` or command line `--customhashattributes` [browserker!329](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/329)
  - Upgrade GCD to version `2.2.3` [browserker!331](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/331)
  - Allow user to log the STDOUT and STDERR of the chromium process with `LogChromiumProcessOutput` [browserker!331](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/331)
  - Add `vulnerabilities[].identifiers[].type` to the secure report [browserker!333](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/333)
  - Add `vulnerabilities[].identifiers[].name` to the secure report [browserker!333](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/333)
  - Add `vulnerabilities[].identifiers[].url` to the secure report [browserker!333](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/333)
  - Add `vulnerabilities[].identifiers[].value` to the secure report [browserker!333](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/333)
- Users can configure the browser-based cache with `DAST_BROWSER_CACHE` (!499)
- Users can configure logging browser-based Chromium output with `DAST_BROWSER_LOG_CHROMIUM_OUTPUT` (!499)

## v2.0.6
- Aggregate noisy vulnerability 10054 "Cookie set without SameSite attribute" findings and report as a single finding (!496)

## v2.0.5
- Update DAST to cancel the scan and fail when the Target site does not respond (!495)

## v2.0.4
- Include the `scan.analyzer` metadata in the JSON report output and updated schema version to 14.0.3 (!492)

## v2.0.3
- Users can configure the browser-based scans to wait for a navigation to complete with `DAST_BROWSER_NAVIGATION_TIMEOUT` (!486)
- Users can configure the browser-based scans to wait for an action to complete with `DAST_BROWSER_ACTION_TIMEOUT` (!486)
- Users can configure the browser-based scans to wait for the DOM to be stable with `DAST_BROWSER_STABILITY_TIMEOUT` (!486)
- Users can configure the browser-based scans to wait for the DOM to be stable after a navigation with `DAST_BROWSER_NAVIGATION_STABILITY_TIMEOUT` (!486)
- Users can configure the browser-based scans to wait for the DOM to be stable after a browser action executes with `DAST_BROWSER_ACTION_STABILITY_TIMEOUT` (!486)
- Users can configure the browser-based scans to restrict how long to spend searching for elements for analysis with `DAST_BROWSER_SEARCH_ELEMENT_TIMEOUT` (!486)
- Users can configure the browser-based scans to restrict how long to spend extracting for elements for analysis with `DAST_BROWSER_EXTRACT_ELEMENT_TIMEOUT` (!486)
- Users can configure the browser-based scans to wait for an element to be considered ready for analysis with `DAST_BROWSER_ELEMENT_TIMEOUT` (!486)
- Upgrade Browserker to version 0.0.36 (!486)
  - Return an internal error to user in the event a scan gets stuck for more than 15 minutes [browserker!264](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/264)
  - Allow user to configure how long to wait for a navigation to complete with `--navigationtimeout` [browserker!264](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/264)
  - Allow user to configure how long to wait for searching elements with `--searchelementtimeout` [browserker!264](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/264)
  - Allow user to configure how long to wait for extracting elements with `--extractelementtimeout` [browserker!264](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/264)
  - Allow user to configure how long to wait after executing a browser action with `--actiontimeout` [browserker!264](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/264)
  - Allow user to configure how long to wait for elements to be considered ready for analysis with `--elementreadytimeout` [browserker!264](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/264)
  - Allow user to configure how long to wait for the DOM to be stable with `--stabilitytimeout` [browserker!264](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/264)
  - Allow user to configure how long to wait after a navigation with `--waitafternavigation` [browserker!264](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/264)
  - Allow user to configure how long to wait after an action with `--waitafteraction` [browserker!264](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/264)
  - Add `vulnerabilities[].evidence.request.url` to the secure report [browserker!260](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/260)
- Upgrade Browserker to version 0.0.37 (!487)
  - Selectors use an ID, name or css selector by default when the selector type is not set [browserker!271](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/271)

## v2.0.2
- Update DAST to handle Browserker errors gracefully (!476)
- Don't advertise DAST scans with the 'Via'/'Via-Scanner' headers by default (!482)
- User can choose to advertise DAST scans using `DAST_ADVERTISE_SCAN` (!482)
- Search for the first submit button or button when using manual authentication and `DAST_SUBMIT_FIELD` is not present (!483)

## v2.0.1
- Upgrade Browserker to version 0.0.35 (!473)
  - Add `vulnerabilities[].category` to the secure report [browserker!253](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/253)
  - Add `vulnerabilities[].confidence` to the secure report [browserker!253](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/253)
  - Add `vulnerabilities[].cve` to the secure report [browserker!253](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/253)
  - Add `vulnerabilities[].description` to the secure report [browserker!253](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/253)
  - Redact the password from the authentication report [browserker!258](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/258)
- Upgrade ZAP to weekly version 2020-09-14 (!478)

## v2.0.0
- Upgrade Browserker to version 0.0.33 (!465)
  - Add the `PathToLoginForm` navigations to the authentication report [browserker!236](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/236)
  - Redact the username and password from all logs [browserker!239](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/239)
  - The `auth` command saves the cookie report when cookie report path is configured  [browserker!242](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/242)
  - User can configure browser viewport size [browserker!241](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/241)
  - Selectors use a name selector by default when the selector type is not set [browserker!252](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/252)
  - Update to Chromium browser 90.0.4430.212, fix for scans can sometimes fail to exit [browserker!200](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/200)
  - User can export the Chromium browser log [browserker!251](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/251)
  - Stability fixes for the browser resource pool [browserker!251](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/251)
  - Add support for GitLab Secure Report format generation using `--secure-report` [browserker!244](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/244)
- Upgrade ZAP add-on `Linux WebDrivers` to [28.0.0](https://github.com/zaproxy/zap-extensions/releases/webdriverlinux-v28) (!465)
- Remove ZAP add-on `MacOS WebDrivers` as it is not used (!465)
- Remove ZAP add-on `Windows WebDrivers` as it is not used (!465)
- Replace DAST selenium-based authentication with browser-based authentication (!465)
- Redact the logged password when using Browserker authentication (!465)
- Update ChromeDriver to version `90.0.4430.24` to support Chromium 90 (!465)
- Remove `-n`, `-s`, `-p`, `-D`, and `--auth-display` config options (!460)
- Remove `@generated`, `@version`, `site` and `spider` fields from DAST report (!460)
- Remove `DAST_AUTH_EXCLUDE_URLS`, `AUTH_EXCLUDE_URLS`, `AUTH_URL`, `AUTH_USERNAME`, `AUTH_PASSWORD`, `AUTH_USERNAME_FIELD`, `AUTH_PASSWORD_FIELD`, `AUTH_SUBMIT_FIELD`, `AUTH_FIRST_SUBMIT_FIELD`, `AUTH_AUTO`, and `DAST_REQUEST_HEADER` config options (!460)
- Remove domain validation (!460)
- Replace `-T` argument with `--zap-max-connection-attempts` and `--passive-scan-max-wait-time` (!460)
- Replace Firefox with Chrome for ZAP Crawljax (!460)
- Set `DAST_SPIDER_START_AT_HOST` default to false (!460)
- Add environment variable alias `DAST_AUTH_REPORT` for `DAST_BROWSER_AUTH_REPORT` (!468)
- Add environment variable alias `DAST_AUTH_VERIFICATION_LOGIN_FORM` for `DAST_BROWSER_AUTH_VERIFICATION_LOGIN_FORM` (!468)
- Add environment variable alias `DAST_AUTH_VERIFICATION_SELECTOR` for `DAST_BROWSER_AUTH_VERIFICATION_SELECTOR` (!468)
- Aggregate noisy vulnerabilities findings and report as a single finding (!466)
- Show Browserker log output in real time (!465)
- Users can configure the browser-based chrome debug log directory using `DAST_CHROME_DEBUG_LOG_DIR` (!469)
- Users can configure the browser-based maximum accepted response size using `DAST_MAX_RESPONSE_SIZE_MB` (!469)
- Upgrade Browserker to version 0.0.34 (!469)
  - Allow configuration of response body limit, defaulting to 10MB [browserker!254](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/254)
  - Selectors use a name or ID selector by default when the selector type is not set [browserker!255](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/255)
  - Allow user to set `--proxy`, `--customheaders`, and `--customcookies` via command line [browserker!256](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/256)

## v1.54.0
- Add `DAST_BROWSER_PATH_TO_LOGIN_FORM` to click on elements prior to filling in a Browserker login form (!454)
- Exclude several rules by default (!456)
- Upgrade Browserker to version 0.0.32 (!459)
  - Styled the authentication report to look more like a GitLab artifact [browserker!234](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/234)
- Raise an error when authentication is incorrectly configured (!462)

## v1.53.0
- Requests sent by DAST include a `Via: GitLab DAST/ZAP v[version]` header (!450)
- Add `DAST_ONLY_INCLUDE_RULES` to allow users to restrict the rules that are run to the given list (!444)

## v1.52.0
- Upgrade Browserker to version 0.0.31 (!452)
  - Fix a bug where cached response bodies were empty [browserker!229](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/229)
  - Users can click on elements to show the login form when authenticating using the PathToLoginForm configuration [browserker!222](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/222)
  - Redact the username and password from the JSON report [browserker!224](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/224)

## v1.51.0
- Upgrade ZAP to weekly version 2020-08-26 (!445)
- Upgrade ZAP add-on `Forced Browse` to [10.0.0](https://github.com/zaproxy/zap-extensions/releases/bruteforce-v10) (!445)
- Upgrade ZAP add-on `Common Library` to [1.2.0](https://github.com/zaproxy/zap-extensions/releases/commonlib-v1.2.0) (!445)
- Upgrade ZAP add-on `Fuzzer` to [13.1.0](https://github.com/zaproxy/zap-extensions/releases/fuzz-v13.1.0) (!445)
- Upgrade ZAP add-on `Getting Started with ZAP Guide` to [12.0.0](https://github.com/zaproxy/zap-extensions/releases/gettingStarted-v12) (!445)
- Upgrade ZAP add-on `Help - English` to [11.0.0](https://github.com/zaproxy/zap-core-help/releases/help-v11) (!445)
- Upgrade ZAP add-on `Online menus` to [8.0.0](https://github.com/zaproxy/zap-extensions/releases/onlineMenu-v8) (!445)
- Upgrade ZAP add-on `Open API Support` to [17.0.0](https://github.com/zaproxy/zap-extensions/releases/openapi-v17) (!445)
- Upgrade ZAP add-on `Quick Start` to [29.0.0](https://github.com/zaproxy/zap-extensions/releases/quickstart-v29) (!445)
- Upgrade ZAP add-on `Script Console` to [27.0.0](https://github.com/zaproxy/zap-extensions/releases/scripts-v27) (!445)
- Upgrade ZAP add-on `Selenium` to [15.3.0](https://github.com/zaproxy/zap-extensions/releases/selenium-v15.3.0) (!445)
- Upgrade ZAP add-on `Websockets` to [23.0.0](https://github.com/zaproxy/zap-extensions/releases/websocket-v23) (!445)
- Upgrade Browserker to version 0.0.30 (!451)
  - Fix a bug where manual authentication was not reporting selector failures [browserker!209](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/209)
  - Fix a bug where large response bodies cause navigation failures [browserker!212](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/212)
  - Authentication report will output result message [browserker!215](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/215)
  - Enhance auto login to check cookie and storage value lengths [browserker!217](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/217)
  - Enhance auto login to check cookie and storage value randomness [browserker!220](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/220)
  - Advertise Browserker scanner by adding a request header to all requests [browserker!216](https://gitlab.com/gitlab-org/security-products/analyzers/browserker/-/merge_requests/216)
- Update Browserker scans to aggregate vulnerabilities by default (!449)

## v1.50.0
- The report field `vulnerabilities[].evidence.summary` is truncated to 20,000 characters (!432)
- Remove all references to WASC from the DAST report (!442)
- Browserker scans no longer ping the target as part of the scan (!443)
- Redact `DAST_PASSWORD` from Selenium logs when debug mode is enabled (!431)
- Disable `https://www.zaproxy.org/docs/alerts/10109` for all scans (!446)
- Add `DAST_API_OPENAPI` configuration variable (!441)
- Add ZAP add-on `Access Control Testing` [6.0.0](https://github.com/zaproxy/zap-extensions/releases/accessControl-v6) (!445)
- Add ZAP add-on `Form Handler` [3.0.0](https://github.com/zaproxy/zap-extensions/releases/formhandler-v3) (!445)

## v1.49.0
- Remove auto-tagging of requests to reduce scan time (!424)
- Upgrade Browserker to version `0.0.29` (!438)
- Users can create a report to help identify authentication issues for Browserker scans (!436)

## v1.48.0
- Load external resources when running the AJAX spider (!415)
- Add `DAST_MAX_URLS_PER_VULNERABILITY` to allow users to adjust and turn off limiting for reported aggregated vulnerabilities (!429)
- Update aggregated vulnerabilities `cve` to include `-aggregated` to enable the vulnerability dashboard to differentiate the two types (!434)

## v1.47.0
- Browserker configuration options can be configured using the naming convention to `DAST_BROWSER_*` (!425)
- Redact `DAST_PASSWORD` from evidence summary (!428)

## v1.46.0
- Upgrade Browserker to version `0.0.27` (!413)
- Support `DAST_PATHS_FILE` for Browserker scans (!413)
- Add support for encoded environment variables (`DAST_PASSWORD_BASE64` and `DAST_REQUEST_HEADERS_BASE64`) for DAST on-demand scans (!418)
- User can verify authentication using a URL using `DAST_AUTH_VERIFICATION_URL` for Browserker scans (!422)
- User can verify authentication using a selector using `DAST_BROWSERKER_AUTH_VERIFICATION_SELECTOR` for Browserker scans (!422)
- User can verify authentication by detecting login forms using `DAST_BROWSERKER_AUTH_VERIFICATION_LOGIN_FORM` for Browserker scans (!422)
- Raise errors for invalid API scan configuration (!420)

## v1.45.0
- Upgrade Browserker to version `0.0.25` (!402)
- Support `DAST_PATHS` for Browserker scans (!402)
- Check `CI_PROJECT_DIR` for the file specified in `DAST_PATHS_FILE` (!399)

## v1.44.0
- Updates field `vulnerabilities.scanner` within the `gl-dast-report.json` to show as Browserker when a Browserker scan runs (!406)
- User can configure the Browserker log using `DAST_BROWSERKER_LOG` (!405)

## v1.43.0
- Update the `BrowserkerScan` to use the cookies created when Browserker authenticates when scanning (!398)
- Update Browserker to use AuthDetails instead of FormData for auth (!397)
- Enable Code Source Disclosure API rules (!403)

## v1.42.0
- Updated DAST to scan all URLs found by Browserker (!387)

## v1.41.0
- Upgrade Browserker to version `0.0.20` (!385)

## v1.40.0
- Build the next version of DAST as an alpha release (!373)
- Updated API Scanning policy rules to include higher risk checks and disable lower risk checks (!372)
- Update `gl-dast-report.json` schema version to `13.1.0` (!378)
- Upgrade Browserker to version `0.0.17` including renaming `DisableHeadless` to `Headless` (!380)

## v1.39.0
- Improved the readability of `/analyze --help` by displaying the environment variable name next to each configuration option (!374)
- Update Browserker scan to only scan hosts included in `DAST_BROWSERKER_ALLOWED_HOSTS` (!366)

## v1.38.0
- Add `DAST_EXCLUDE_URLS` which is an alias of `DAST_AUTH_EXCLUDE_URLS` (!367)

## v1.37.0
- Update `analyze` script to error if the user has insufficient permissions (!360)
- Add `DAST_AUTH_VERIFICATION_URL` to verify that DAST authentication succeeded (!364)
- Abort the scan and exit if the target returns a 500 during the access check (!363)
- Add `DAST_SKIP_TARGET_CHECK` to allow for skipping the target check (!365)

## v1.36.0
- Fix issue in Dockerfile where user was asked for input during build (!358)
- Upgrade Python to version `3.9.1` (!358)
- Upgrade Browserker to version `0.0.16` (!356)
- Browserker runs as the `gitlab` user (!356)
- Add URL validation to `DAST_WEBSITE` (!354)

## v1.35.0
- Upgrade ZAP add-on `Active scanner rules (beta)` to [32.0.0](https://github.com/zaproxy/zap-extensions/releases/ascanrulesBeta-v32) (!352)
- Upgrade ZAP add-on `Passive scanner rules` to [30.0.0](https://github.com/zaproxy/zap-extensions/releases/pscanrules-v30) (!352)
- Upgrade ZAP add-on `Passive scanner rules (beta)` to [23.0.0](https://github.com/zaproxy/zap-extensions/releases/pscanrulesBeta-v23) (!352)
- Upgrade ZAP add-on `Ajax Spider` to [23.2.0](https://github.com/zaproxy/zap-extensions/releases/spiderAjax-v23.2.0) (!352)
- Upgrade ZAP add-on `Linux WebDrivers` to [23.0.0](https://github.com/zaproxy/zap-extensions/releases/webdriverlinux-v23) (!352)
- Upgrade ZAP add-on `MacOS WebDrivers` to [22.0.0](https://github.com/zaproxy/zap-extensions/releases/webdrivermacos-v22) (!352)
- Upgrade ZAP add-on `Windows WebDrivers` to [23.0.0](https://github.com/zaproxy/zap-extensions/releases/webdriverwindows-v23) (!352)
- Upgrade ZAP add-on `Zest - Graphical Security Scripting Language` to [33.0.0](https://github.com/zaproxy/zap-extensions/releases/zest-v33) (!352)

## v1.34.0
- Add `scan.scanner.vendor.name` to the JSON report output (!334)
- Make the zap and browserker user's uid configurable at build time (!350)
- Improve `identify-addon-updates.sh` to display the changes required in the Changelog, Dockerfile and MR description (!343)
- Update release script to prevent failure when unexpected characters are included in the Changelog (!345)
- Document the new process of updating the ZAP plugins (!353)

## v1.33.0
- Improve ability to find the login button when running authenticated scans (!342)

## v1.32.1
- Update the help text for `DAST_SUBMIT_FIELD`, `DAST_FIRST_SUBMIT_FIELD`, `DAST_USERNAME_FIELD`, and `DAST_PASSWORD_FIELD` (!338)

## v1.32.0
- Revert: `Remove the duplicate target check prior to a scan (!323)` (!336)

## v1.31.0
- Remove the duplicate target check prior to a scan (!323)
- Upgrade ZAP add-on `ascanrules` to [v36.0.0](https://github.com/zaproxy/zap-extensions/releases/ascanrules-v36) (!333)
- Upgrade ZAP add-on `ascanrulesBeta` to [v31.0.0](https://github.com/zaproxy/zap-extensions/releases/ascanrulesBeta-v31) (!333)
- Upgrade ZAP add-on `commonlib` to [v1.1.0](https://github.com/zaproxy/zap-extensions/releases/commonlib-v1.1.0) (!333)
- Upgrade ZAP add-on `fuzz` to [v13.0.1](https://github.com/zaproxy/zap-extensions/releases/fuzz-v13.0.1) (!333)
- Upgrade ZAP add-on `hud` to [v0.12.0](https://github.com/zaproxy/zap-hud/releases/v0.12.0) (!333)
- Upgrade ZAP add-on `retire` to [v0.5.0](https://github.com/zaproxy/zap-extensions/releases/retire-v0.5.0) (!333)
- Upgrade ZAP add-on `webdriverlinux` to [v21.0.0](https://github.com/zaproxy/zap-extensions/releases/webdriverlinux-v21) (!333)
- Upgrade ZAP add-on `webdrivermacos` to [v20.0.0](https://github.com/zaproxy/zap-extensions/releases/webdrivermacos-v20) (!333)
- Upgrade ZAP add-on `webdriverwindows` to [v21.0.0](https://github.com/zaproxy/zap-extensions/releases/webdriverwindows-v21) (!333)
- Upgrade ZAP add-on `websocket` to [v22.0.0](https://github.com/zaproxy/zap-extensions/releases/websocket-v22) (!333)
- Disable the DAST Unix Timestamp Vulnerability Check on all scans (!335)

## v1.30.0
- Log active scan rules that were run if `DAST_DEBUG=true` (!315)
- Add `DAST_SPIDER_START_AT_HOST` to allow scans to start at given target (!317)
- Run a passive or active scan with predetermined URLs using the environment variable `DAST_PATHS_FILE` (!319)
- Prevent DAST from accessing API specification files outside of the /zap/wrk directory (!322)
- Optimize DAST image to be smaller in size (!320)
- Upgrade Firefox to version 81 (!327)
- Upgrade Geckodriver to version 0.27.0 (!320)
- Firefox runs headless when authenticating user using Selenium WebDriver (!320)

## v1.29.0
- Improve error messaging when target website check fails (!314)
- DAST can be run as a user other than `zap` for RedHat OpenShift support (!316)

## v1.28.0
- Paths added to `DAST_PATHS` are excluded if they are contained in `DAST_AUTH_EXCLUDE_URLS` (!311)
- Remove ZAP config options logging statement as DAST no longer passes control to the ZAP scripts (!313)

## v1.27.0
- Reset target to host without trailing slash to prevent duplicate vulnerabilities (!304)
- Throw an error if both the AJAX Spider and an API scan are configured (!303)
- Use a consistent strategy for handling and communicating errors (!302)
- Print the version of DAST at the start of the log (!306)
- Remove unused JAR dependencies from the Docker image that flag as an issue in some vulnerability scanners (!307)
- Promote spider logging statements to INFO (!304)

## v1.26.0
- Run a passive scan with predetermined URLs using the environment variable `DAST_PATHS` (!284)
- Run an active scan with predetermined URLs using the environment variable `DAST_PATHS` (!293) (!300)

## v1.25.0
- Add deprecation notice to -D (!291)
- Output an error when required configuration parameters have not been set (!280)
- Passive Scan rules excluded with `DAST_EXCLUDE_RULES` are marked as skipped in the log summary (!290)
- Fix bug where headers that contain colons cause DAST to crash (!296)
- Set site availability timeout using `DAST_TARGET_AVAILABILITY_TIMEOUT` environment variable (!287)

## v1.24.0
- Fix bug where `DAST_DEBUG` is ignored (!285)
- Improve logging output during active scan (!273)
- Log warning when `ZAP_API_HOST_OVERRIDE` used when importing an API specification from a file (!274)
- Results printed at the end of a scan only include rules that were run (!268)
- DAST exits with a non-zero exit code when ZAP crashes (!265)
- Errors thrown during a scan cause the scan to abort (!264)
- Remove tagging of URLs because they take a long time to execute and don't appear to be used (!229)
- Show skipped rules in log output (!269)

## v1.23.0
- Set the directory location of custom ZAP scripts using `DAST_SCRIPT_DIRS` (!234)
- Add deprecation notice to `-p` configuration option (!246)
- Add deprecation notice to the `-n` configuration option (!252)
- Set how long for DAST to wait for a passive scan to execute with the environment variable `DAST_PASSIVE_SCAN_MAX_WAIT_TIME` (!252)
- Set how many attempts DAST should make to connect to the ZAP Server with the environment variable `DAST_ZAP_MAX_CONNECTION_ATTEMPTS` (!252)
- Set how many seconds DAST should sleep in between ZAP server connection attempts with the environment variable `DAST_ZAP_CONNECT_SLEEP_SECONDS` (!252)

## v1.22.1
- Use an explicit session name to ensure the correct ZAP database is used (!241)

## v1.22.0
- Remove duplicate request for ZAP alerts to reduce memory usage and time to build report (!239)
- Retrieve ZAP messages for alerts from the ZAP database to reduce the memory required to run DAST (!239)
- Retrieve ZAP messages for scanned resources from the ZAP database to reduce the memory required to run DAST (!240)

## v1.21.1
- Add deprecation notice to `-s` configuration option (!219)
- HSQLDB logs are logged at `WARN` to reduce noise in the log file (!222)

## v1.20.0
- Rules excluded using the `DAST_EXCLUDE_RULES` environment variable do not run (!216)

## v1.19.0
- Update DAST to use the ZAP Docker base image `owasp/zap2docker-weekly:w2020-06-30` (!208)
- Upgrade Python to version `3.8.2` (!208)
- Prevent ZAP from failing when running in other container engines than Docker (!197)
- Upgrade ZAP add-on `ascanrules` to [v35.0.0](https://github.com/zaproxy/zap-extensions/releases/ascanrules-v35) (!211)
- Upgrade ZAP add-on `ascanrulesBeta` to [v28.0.0](https://github.com/zaproxy/zap-extensions/releases/ascanrulesBeta-v28) (!211)
- Upgrade ZAP add-on `commonlib` to [v1.0.0](https://github.com/zaproxy/zap-extensions/releases/commonlib-v1.0.0) (!211)
- Upgrade ZAP add-on `fuzzdb` to [v7.0.0](https://github.com/zaproxy/zap-extensions/releases/fuzzdb-v7) (!211)
- Upgrade ZAP add-on `openapi` to [v16.0.0](https://github.com/zaproxy/zap-extensions/releases/openapi-v16) (!211)
- Upgrade ZAP add-on `pscanrules` to [v29.0.0](https://github.com/zaproxy/zap-extensions/releases/pscanrules-v29) (!211)
- Upgrade ZAP add-on `pscanrulesBeta` to [v22.0.0](https://github.com/zaproxy/zap-extensions/releases/pscanrulesBeta-v22) (!211)
- Upgrade ZAP add-on `webdriverlinux` to [v18.0.0](https://github.com/zaproxy/zap-extensions/releases/webdriverlinux-v18) (!211)
- Upgrade ZAP add-on `webdrivermacos` to [v17.0.0](https://github.com/zaproxy/zap-extensions/releases/webdrivermacos-v17) (!211)
- Upgrade ZAP add-on `webdriverwindows` to [v18.0.0](https://github.com/zaproxy/zap-extensions/releases/webdriverwindows-v18) (!211)

## v1.18.1
- Reduce memory usage by excluding HTTP request and response bodies from alerts (!201)

## v1.18.0
- Remove support for the ZAP config file because it doesn't work as intended (!185)
- Remove environment variables `DAST_ZAP_CONFIG_FILE`, `DAST_ZAP_CONFIG_URL`, `DAST_ZAP_GENERATE_CONFIG` (!185)
- Run DAST as the `zap` user, not as `root` (!186)
- Update Firefox to version `77.0` and Geckodriver to version `0.26.0` (!106)
- Update Python Selenium to version `3.141.0` (!191)

## v1.17.1
- Default spidering in a full scan to have an unlimited maximum duration (!184)

## v1.17.0
- Remove `INFO` log4j properties from base logging configuration for ZAP Server (!182)
- Include request and response headers in the vulnerability evidence (!168)
- Mask sensitive request and response header values with environment variable DAST_MASK_HTTP_HEADERS (!179)

## v1.16.0
- Set ZAP Server log4j log levels using `DAST_ZAP_LOG_CONFIGURATION` (!166)
- Remove GitLab Runner style `x-x-stable` tags from future DAST Docker images (!174)
- Remove legacy DAST entrypoints, please use `/analyze` instead (!172)

## v1.15.0
- Include context of the scan in the DAST JSON Report (!165)

## v1.14.0
- The DAST JSON report is created using information from the ZAP REST API, not the ZAP JSON report (!142)
- Set the maximum duration of the spider scan with environment variable `DAST_SPIDER_MINS` (!153)
- Include alpha passive and active scan rules with environment variable `DAST_INCLUDE_ALPHA_VULNERABILITIES` (!153)
- Set the ZAP config URL to configure vulnerability finding risk levels with environment variable `DAST_ZAP_CONFIG_URL` (!153)
- Set the name of the ZAP config file to configure vulnerability finding risk levels with environment variable `DAST_ZAP_CONFIG_FILE` (!163)
- Generate sample config file with environment variable `DAST_ZAP_GENERATE_CONFIG` (!163)
- Set the ZAP Server command-line options with environment variable  `DAST_ZAP_CLI_OPTIONS` (!163)
- Enable DAST debug messages with environment variable `DAST_DEBUG` (!163)
- Set the file name of the ZAP HTML report written at the end of a scan using `DAST_HTML_REPORT` (!159)
- Set the file name of the ZAP Markdown report written at the end of a scan using `DAST_MARKDOWN_REPORT` (!159)
- Set the file name of the ZAP XML report written at the end of a scan using `DAST_XML_REPORT` (!159)
- Copy contents of `/zap/wrk` to the working directory in order to make them available as CI job artifacts (!160)

## v1.13.3
- Enable Ajax spider using the environment variable `DAST_ZAP_USE_AJAX_SPIDER` (!147)

## v1.13.2
- ZAP does not make external HTTP requests when `auto-update-addons` is false (!146)
- Upgrade ZAP add-on `fuzzdb` to [v6](https://github.com/zaproxy/zap-extensions/releases/tag/fuzzdb-v6) (!144)
- Upgrade ZAP add-on `pscanrules-release` to [v28](https://github.com/zaproxy/zap-extensions/releases/tag/pscanrules-v28) (!144)
- Upgrade ZAP add-on `selenium-release` to [v15.2.0](https://github.com/zaproxy/zap-extensions/releases/tag/selenium-v15.2.0) (!144)

## v1.13.1
- Disable auto-updating ZAP addons by default (!143)

## v1.13.0
- Each Vulnerability is now identifiable by a unique id in the JSON report (!128)

## v1.12.1
- Rename `CommonReportFormatter` to `SecurityReportFormatter` (!138)

## v1.12.0
- `vulnerabilities[].evidence.summary` is added to the report to provide more context about the vulnerability (!132)

## v1.11.1
- `severity` and `confidence` report values are title case to match the DAST schema (!131)

## v1.11.0
- OpenAPI specifications can be referenced using the `DAST_API_SPECIFICATION` environment variable to trigger an API scan (!92)(!112)(!125)(!126)
- Hosts defined in API specifications can be overridden using the `DAST_API_HOST_OVERRIDE` environment variable (!112)(!123)
- Validation rules can be excluded from the scan using the `DAST_EXCLUDE_RULES` environment variable (!115)
- Request headers can be added to every request using the `DAST_REQUEST_HEADERS` environment variable (!124)

## v1.10.0
- Limit URLs displayed in console output to those captured during spider scan (!113)
- Output URLs spidered by DAST JSON report as `scan.scanned_resources` (!113)
- Logging is `INFO` level by default (!110)

## v1.9.0
- Include URLs scanned by DAST in the console output (!112)

## v1.8.0
- DAST depends on ZAP weekly release w2020-02-10 (!101)
- DAST Python scripts run using Python 3.6.9 in anticipation of Python 2 EOL (!102)
- Upgrade pinned add-on versions: alertFilters to [v10](https://github.com/zaproxy/zap-extensions/releases/tag/alertFilters-v10), ascanrulesBeta to [v27](https://github.com/zaproxy/zap-extensions/releases/tag/ascanrulesBeta-v27) and pscanrules to [v27](https://github.com/zaproxy/zap-extensions/releases/tag/pscanrules-v27) (!107)

## v1.7.0
- Include recent ZAP add-ons into the DAST Docker image so they don't have to be downloaded every scan (!105)
- Auto-update of ZAP add-ons can be disabled with the `--auto-update-addons false` command line argument (!105)

## v1.6.1
- Fix issue where AJAX spider scans could not start Firefox (!87)

## v1.6.0
- Add initial Secure Common Report Format fields to DAST report (!81)

## v1.5.4
- Validate URL command line arguments (!69)
- Exit code is zero when a scan runs successfully, non zero when a scan fails or arguments are invalid (!69)
- The DAST report is deterministic, keys in the JSON document are in alphabetical order (!70)
- Ajax scans start from the target URL (!71)
- Don't verify HTTPS certificates when determining if a target site is ready for scanning (!76)

## v1.5.3
- Fixed support for `--auth-exclude-urls` parameter (!52)

## v1.5.2
- DAST depends on ZAP weekly release w2019-09-24, re-enabling `ascanrulesBeta` rules
- Removed Python 3 to fix Full Scans

## v1.5.1
- DAST depends on ZAP release 2.8.0 (!50)

## v1.5.0
- Running `/analyze --help` shows all options and environment variables supported by DAST (!39)
- Expose ZAP logs while executing scans (!42)

## v1.4.0
- Implement [domain validation](https://docs.gitlab.com/ee/user/application_security/dast/index.html#domain-validation) option for full scans (!35)

## v1.3.0
- Report which URLs were scanned (!24)

## v1.2.7
- Fix passing of optional params to ZAP (!27)

## v1.2.6
- Fix max. curl timeout to be longer than 150 seconds (!26)

## v1.2.5
- Fix curl timeout (!25)

## v1.2.4
- Fix timeout when $DAST_TARGET_AVAILABILITY_TIMEOUT is used (!21)

## v1.2.3
- Fix auto login functionality. Auto login is used if the HTML elements for username, password, or submit button have not been specified.

## v1.2.2
- Fix a bug where `analyze` would fail if only `DAST_WEBSITE` was used. https://gitlab.com/gitlab-org/gitlab-ee/issues/11744

## v1.2.1
- Accept $DAST_WEBSITE env var instead of `-t` parameter (still supported for backward compatibility)

## v1.2.0
- Add [ZAP Full Scan](https://github.com/zaproxy/zaproxy/wiki/ZAP-Full-Scan) support (!14)

## v1.1.2
- Add workaround for supporting long CLI auth options (!9)

## v1.1.1
- Fix a problem with multiple login buttons on the login page.

## v1.1.0
- First release of the DAST GitLab image.
