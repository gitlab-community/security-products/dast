from invoke import Collection, task

from lib.tests import Tests
from lib.tests.e2e_refresher import EndToEndRefresher


@task
def unit(_, file='', test=''):
    """Run unit tests (options: --file, --test)."""
    Tests('Unit', 'test/unit', file, test).execute()


@task
def integration(_, file='', test=''):
    """Run integration tests (options: --file, --test)."""
    Tests('Integration', 'test/integration', file, test).execute()


@task
def regenerate_e2e_expected_json(context, image='dast', future_image='dast-future', skip_build=False, skip_tests=False):
    """Refresh the e2e expectations (options: --image --future-image --skip-build --skip-tests)."""
    EndToEndRefresher(image, future_image).execute(skip_build, skip_tests)


test = Collection(unit, integration, regenerate_e2e_expected_json)
