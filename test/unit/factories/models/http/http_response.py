from src.http_headers import HttpHeaders
from src.models.http import HttpResponse


def http_response(status: int = 200,
                  reason_phrase: str = 'OK',
                  headers: HttpHeaders = None) -> HttpResponse:
    http_headers = headers if headers else HttpHeaders()

    return HttpResponse(status, reason_phrase, http_headers)
