from src.models import Rule


def f_rule(name='rule.name',
           rule_id='rule.id',
           is_enabled=True) -> Rule:
    return Rule(rule_id, is_enabled, name)
