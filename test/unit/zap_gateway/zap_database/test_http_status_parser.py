from unittest import TestCase

from src.zap_gateway.zap_database.http_status_parser import HttpStatusParser


class TestHttpStatusParser(TestCase):

    def test_should_parse_200_ok(self):
        response_header = '\r\n'.join(['HTTP/1.1 200 OK',
                                       'Server: nginx/1.22.0',
                                       ''])

        status_code, reason_phrase = HttpStatusParser().parse(response_header)

        self.assertEqual(200, status_code)
        self.assertEqual('OK', reason_phrase)

    def test_should_parse_404_not_found(self):
        status_code, reason_phrase = HttpStatusParser().parse('HTTP/1.1 404 Not Found')

        self.assertEqual(404, status_code)
        self.assertEqual('Not Found', reason_phrase)

    def test_should_parse_status_when_there_is_no_reason_phrase(self):
        status_code, reason_phrase = HttpStatusParser().parse('HTTP/1.1 200')

        self.assertEqual(200, status_code)
        self.assertEqual('', reason_phrase)

    def test_should_parse_status_when_there_is_empty_input(self):
        status_code, reason_phrase = HttpStatusParser().parse('')

        self.assertEqual(0, status_code)
        self.assertEqual('', reason_phrase)

    def test_should_parse_status_when_there_is_no_input(self):
        status_code, reason_phrase = HttpStatusParser().parse(None)

        self.assertEqual(0, status_code)
        self.assertEqual('', reason_phrase)

    def test_should_parse_status_when_not_an_integer(self):
        status_code, reason_phrase = HttpStatusParser().parse('HTTP/1.1 Not Found')

        self.assertEqual(0, status_code)
