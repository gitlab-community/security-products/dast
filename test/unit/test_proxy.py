import socket
from contextlib import closing
from typing import Any, Callable, Dict, List, Optional, Tuple  # noqa: F401
from unittest import TestCase
from unittest.mock import MagicMock, Mock, patch

from src.dast import DAST
from src.models.errors import ProxyFailedToStartError
from src.proxy import Proxy as DastProxy
from src.scan_script_wrapper import ScanScriptWrapper
from src.system import System
from test.unit.mock_config import ToConfig
from test.unit.utilities import httpserver


class Proxy(TestCase):
    system: System

    def __init__(self, *args, **kwargs):
        super(Proxy, self).__init__(*args, **kwargs)
        self.system = System()

    def check_port_closed(self, host: str = '127.0.0.1', port: int = 3128) -> bool:
        """Check if a TCP is closed

        Args:
            host (str, optional): Host to find a port on. Defaults to '127.0.0.1'.
            port (int, optional): TCP port to check. Defaults to 3128.

        Returns:
            bool: True if closed, False if open
        """

        with closing(socket.socket(socket.AF_INET, socket.SOCK_STREAM)) as sock:
            sock.setblocking(False)
            if sock.connect_ex((host, port)) != 0:
                return True
            else:
                return False

    def setUp(self) -> None:
        self._dast_error_handler = MagicMock()

        self._config = ToConfig(target='http://website', exclude_rules=[], fips_mode=True)

        self._container = MagicMock()
        self._container.config = self._config
        self._container.proxy = DastProxy()

        self._system_DAST_VERSION = System.DAST_VERSION
        System.DAST_VERSION = '0.0.0'

        self._scan = ScanScriptWrapper(
            self._config,
            MagicMock(),
            MagicMock,
            MagicMock(),
            self._container.proxy)
        self._container.scan = self._scan

        self.dast = DAST(lambda: self._container)

    def tearDown(self) -> None:
        System.DAST_VERSION = self._system_DAST_VERSION

    def test_should_exception_when_port_closed(self) -> None:
        closed_port = self.system.get_free_port()

        proxy = DastProxy()
        self.assertRaises(
            ProxyFailedToStartError,
            lambda: proxy.wait_for_proxy(port=closed_port, timeout=0.1, wait_between_checks=0.1))

    def test_should_return_when_port_open(self) -> None:
        with httpserver.new().respond(status=200).build() as server:
            proxy = DastProxy()
            proxy.wait_for_proxy(port=server.port(), timeout=0.3, wait_between_checks=0.1)

    @patch('src.dast.DASTErrorHandler')
    @patch('src.dast.exit')
    def test_should_exit_when_port_closed(self, mock_exit, dast_error_handler_class) -> None:
        self.assertTrue(
            self.check_port_closed(port=3128),
            'Expect port 3128 to be closed')

        dast_error_handler = MagicMock()
        dast_error_handler_class.return_value = dast_error_handler
        dast_error_handler.handle = Mock(
            side_effect=lambda x: self.assertIsInstance(
                x, ProxyFailedToStartError, 'Expect ProxyFailedToStartError exception'),
            return_value=1)

        with patch.object(DastProxy.wait_for_proxy, '__defaults__', ('127.0.0.1', 3128, 0.1, 0.1)):
            self.dast.start()

        dast_error_handler.handle.assert_called_once()
        mock_exit.assert_called_once_with(1)
