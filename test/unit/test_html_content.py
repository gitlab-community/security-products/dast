from unittest import TestCase

from src.html_content import HTMLContent


class TestHtmlContent(TestCase):

    # text
    def test_should_return_empty_when_there_is_no_text(self):
        html = HTMLContent('')
        self.assertEqual(html.text(), '')

    def test_should_return_text_when_content_is_text(self):
        html = HTMLContent('snippet')
        self.assertEqual(html.text(), 'snippet')

    def test_should_return_text_for_a_snippet(self):
        html = HTMLContent('<p>snippet</p>')
        self.assertEqual(html.text(), 'snippet')

    def test_should_return_text_for_multiple_snippets(self):
        html = HTMLContent('<p>snippet.a</p><p>snippet.b</p>')
        self.assertEqual(html.text(), 'snippet.a snippet.b')

    def test_should_return_text_for_an_entire_html_page(self):
        html = HTMLContent('<html><body><div>content</div></body></html>')
        self.assertEqual(html.text(), 'content')

    def test_should_ignore_non_text_elements(self):
        content = """ <!DOCTYPE html>
                      <html>
                        <head>
                            <meta property="og:url" content="https://url">
                            <link rel="shortcut icon" href="https://favicon">
                            <script src="https://script.js"></script>
                            <style>h1 {color:red;}</style>
                        </head>
                        <body>
                            <div><p><span>content.a</span></p></div>
                            <script>console.log('kangaroos!');</script>
                            <noscript>Javascript is not enabled</noscript>
                            <div><p><span>content.b</span></p></div>
                        </body>
                      </html>  """
        html = HTMLContent(content)
        self.assertEqual(html.text(), 'content.a content.b')

    # list_of_text_in_tag
    def test_should_return_no_content_when_tag_not_found(self):
        html = HTMLContent('<html><body><div>content</div></body></html>')
        self.assertEqual(html.list_of_text_in_tag('p'), [])

    def test_should_return_content_when_tag_found(self):
        html = HTMLContent('<html><body><div>content</div></body></html>')
        self.assertEqual(html.list_of_text_in_tag('div'), ['content'])

    def test_should_return_content_from_snippet(self):
        html = HTMLContent('<p>http://url.a</p><p>http://url.b</p>')
        self.assertEqual(html.list_of_text_in_tag('p'), ['http://url.a', 'http://url.b'])

    def test_should_ignore_empty_tags(self):
        html = HTMLContent('<div><p></p><p></p><p></p></div>')
        self.assertEqual(html.list_of_text_in_tag('p'), [])

    def test_should_return_only_list_of_text_in_tag(self):
        html = HTMLContent('<div><p>content</p></div>')
        self.assertEqual(html.list_of_text_in_tag('div'), ['content'])
