import os
from unittest import TestCase

from src.services.browserker import BrowserkerEnvironment


class TestBrowserkerEnvironment(TestCase):

    def test_removes_dast_variables(self):
        env = os.environ.copy()
        env['DAST_CHECKS_TO_EXCLUDE'] = '10096, 10050, 10027'
        env['DAST_BROWSER_SCAN'] = 'true'
        env['DAST_SCOPE_EXCLUDE_HOSTS'] = 'fonts.googleapis.com'

        updated_env = BrowserkerEnvironment(env).removeDASTConfig()

        self.assertIn('PATH', updated_env)
        self.assertNotIn('DAST_CHECKS_TO_EXCLUDE', updated_env)
        self.assertNotIn('DAST_BROWSER_SCAN', updated_env)
        self.assertNotIn('DAST_SCOPE_EXCLUDE_HOSTS', updated_env)
