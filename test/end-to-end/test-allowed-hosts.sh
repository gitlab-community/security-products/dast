#!/bin/bash
# Testing framework: https://github.com/pgrange/bash_unit


# shellcheck disable=SC1091
source "./end-to-end-test-helpers.sh"

BUILT_IMAGE=${BUILT_IMAGE:-dast}

setup_suite() {
  setup_test_dependencies
  run_echo_site
  run_out_of_scope_site
  true
}

teardown_suite() {
  docker rm --force nginx >/dev/null 2>&1
  docker rm --force echo-site >/dev/null 2>&1
  docker network rm test >/dev/null 2>&1
  true
}

test_custom_headers_are_also_sent_to_allowed_hosts() {
  skip_if_fips "Browser scan is not sending custom headers"

  ./docker_run_dast --rm \
    -v "${PWD}":/output \
    --network test \
    --env DAST_ALLOWED_HOSTS="echo-site" \
    --env DAST_DEBUG=1 \
    --env DAST_ZAP_LOG_CONFIGURATION="rootLogger.level=debug" \
    --env DAST_REQUEST_HEADERS='Authorization: Bearer secret-token' \
    --env DAST_ZAP_CONNECT_SLEEP_SECONDS='5' \
    "${BUILT_IMAGE}" \
    /analyze -j -t http://nginx -d \
    >output/test_custom_headers_are_also_sent_to_allowed_hosts.log 2>&1
  assert_equals "0" "$?" "Expected to exit without errors"

  docker run --rm --network test -v "${PWD}":/output "${BUILT_IMAGE}" curl --silent http://echo-site/recorded-requests >output/recorded-requests-allowed-hosts.txt
  assert_equals "0" "$?" "Expected recorded requests to be fetched without errors"

  grep -q "http://echo-site/styles.css was filtered by a filter with reason: OUT_OF_CONTEXT" output/test_custom_headers_are_also_sent_to_allowed_hosts.log
  assert_equals "1" "$?" "Expected allowed host to be part of the ZAP context"

  grep -q "authorization: Bearer secret-token  (from host echo-site)" output/recorded-requests-allowed-hosts.txt
  assert_equals "0" "$?" "Secret token should be exposed to allowed hosts"
}
