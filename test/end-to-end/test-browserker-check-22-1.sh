#!/bin/bash
# Testing framework: https://github.com/pgrange/bash_unit

# shellcheck disable=SC1091
source "./end-to-end-test-helpers.sh"

BUILT_IMAGE=${BUILT_IMAGE:-dast}

setup_suite() {
  setup_test_dependencies
  docker network create test >/dev/null

  docker run \
    --name check-22-1 \
    -v "${PWD}/fixtures/check-22-1/nginx.conf":/etc/nginx/conf.d/default.conf \
    -v "${PWD}/fixtures/check-22-1/index.html:/usr/share/nginx/html/index.html" \
    --network test -d openresty/openresty:1.19.9.1-9-buster-fat >/dev/null 2>&1
}

teardown_suite() {
  docker rm --force check-22-1  >/dev/null 2>&1
  docker network rm test >/dev/null 2>&1
  true
}

test_browserker_active_check_22_1() {
  # This test verifies the inclusion of browserker check 22.1 and the exclusion of ZAP check 6.
  ./docker_run_dast --rm \
    -v "${PWD}":/output \
    --network test \
    --env DAST_BROWSER_SCAN=true \
    --env DAST_FULL_SCAN=true \
    --env DAST_CHECKS_TO_EXCLUDE=40012,90020,10095,10106,693.1,16.2,16.8 \
    --env DAST_LOG_CONFIG="brows:debug,chrom:trace" \
    --env DAST_LOG_DEVTOOLS_CONFIG="Default:suppress; Fetch:messageAndBody,truncate:2000; Network:messageAndBody,truncate:2000" \
    --env DAST_CRAWL_GRAPH="True" \
    "${BUILT_IMAGE}" \
    /analyze -d -t http://check-22-1 >output/test_browserker_active_check_22_1.log 2>&1
  assert_equals "0" "$?" "Expected to exit without errors"

  jq . <gl-dast-report.json >output/report_test_browserker_active_check_22_1.json
  rm -rf ./browserker_data
  mv ./gl-dast-crawl-graph.svg output/report_test_browserker_active_check_22_1-crawl-graph.svg

  assert_output test_browserker_active_check_22_1 output/report_test_browserker_active_check_22_1

  ./verify-dast-schema.py output/report_test_browserker_active_check_22_1.json
}

test_browserker_active_checks_can_be_disabled() {
  skip_if_fips "Test asserts presence of ZAP finding, ZAP does not run when in FIPS mode"

  # This test verifies the exclusion of browserker check 22.1 and the inclusion of ZAP check 6.
  ./docker_run_dast --rm \
    -v "${PWD}":/output \
    --network test \
    --env DAST_BROWSER_SCAN=true \
    --env DAST_FULL_SCAN_ENABLED=true \
    --env DAST_EXCLUDE_RULES=40012,90020,10095,10106,693.1,16.2,16.8 \
    --env DAST_FF_ENABLE_BROWSER_BASED_ATTACKS="false" \
    --env DAST_BROWSER_LOG="brows:debug,chrom:trace" \
    --env DAST_BROWSER_DEVTOOLS_LOG="Default:suppress; Fetch:messageAndBody,truncate:2000; Network:messageAndBody,truncate:2000" \
    --env DAST_BROWSER_CRAWL_GRAPH="True" \
    "${BUILT_IMAGE}" \
    /analyze -d -t http://check-22-1 >output/test_browserker_active_checks_can_be_disabled.log 2>&1
  assert_equals "0" "$?" "Expected to exit without errors"

  jq . <gl-dast-report.json >output/report_test_browserker_active_checks_can_be_disabled.json
  rm -rf ./browserker_data
  mv ./gl-dast-crawl-graph.svg output/report_test_browserker_active_checks_can_be_disabled-crawl-graph.svg

  assert_output test_browserker_active_checks_can_be_disabled output/report_test_browserker_active_checks_can_be_disabled

  ./verify-dast-schema.py output/report_test_browserker_active_checks_can_be_disabled.json
}
