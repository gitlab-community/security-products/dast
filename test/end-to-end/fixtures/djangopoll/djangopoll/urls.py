from django.urls import include, path
from django.views.generic.base import TemplateView

urlpatterns = [
    path('', include('home.urls')),
    path('polls/', include('polls.urls')),
    path('accounts/', include('django.contrib.auth.urls')),
    path('styles.css', TemplateView.as_view(template_name='styles.css', content_type='text/css')),
]
