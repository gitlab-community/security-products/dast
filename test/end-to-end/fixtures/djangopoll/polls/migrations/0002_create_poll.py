from django.db import migrations
from django.utils import timezone


def create_question(apps, schema_editor):
    Question = apps.get_model('polls', 'Question')

    results = Question.objects.filter(question_text="What's new?")

    if len(results) == 0:
        question = Question(question_text="What's new?", pub_date=timezone.now())
        question.save()
        question.choice_set.create(choice_text='Not much', votes=10)
        question.choice_set.create(choice_text='The sky', votes=5)
        question.choice_set.create(choice_text='Just hacking again', votes=3)


class Migration(migrations.Migration):
    initial = True

    dependencies = [('polls', '0001_initial')]
    operations = [migrations.RunPython(create_question)]
