#!/bin/bash

set -e

PROJECT_DIR="$(dirname "$(realpath "$0")")/../.."

# Lint Python
git ls-files -z "$PROJECT_DIR/**/*.py" | xargs -0 flake8 --show-source --config="$PROJECT_DIR/.flake8"
