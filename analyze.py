#!/usr/bin/env python

from src.dast import DAST
from src.dependencies import initialize_container

if __name__ == '__main__':
    dast = DAST(initialize_container)
    dast.start()
