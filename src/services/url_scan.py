import logging
import re
from typing import List

from src.models import Target
from src.services import UrlListFileWriter
from src.zap_gateway import ZAProxy


class URLScan:

    URLS_TO_SCAN_PATH = '/zap/urls_to_scan.txt'

    def __init__(self, zap: ZAProxy, target: Target, urls_to_scan: List[str], exclude_urls: List[str]):
        self._target = target
        self._zap = zap
        self._urls_to_scan = urls_to_scan
        self._exclude_urls = exclude_urls

    def run(self) -> None:
        logging.info(f'URL Scan {self._target}')

        UrlListFileWriter(self._filtered_urls_to_scan(), self.URLS_TO_SCAN_PATH).write()

        self._zap.run_url_scan(self.URLS_TO_SCAN_PATH)

        logging.info('URL Scan complete')

    def _filtered_urls_to_scan(self) -> List:
        all_urls = self._urls_to_scan

        if not self._exclude_urls:
            return all_urls

        return [url for url in all_urls if not self._exclude_url(url)]

    def _exclude_url(self, url: str) -> bool:
        for exclude_url in self._exclude_urls:
            exp = re.compile(exclude_url)
            if exp.match(url):
                return True

        return False
