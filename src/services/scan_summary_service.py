from src import System, models
from src.models import Alerts, Rule


class ScanSummaryService:

    def __init__(self,
                 rules: models.Rules,
                 alerts: Alerts,
                 system: System,
                 max_rules_to_print=5):
        self._rules = rules
        self._alerts = alerts
        self._system = system
        self._max_rules_to_print = max_rules_to_print

    def print_results(self) -> None:
        failed = 0

        for rule in self._rules:
            alerts = self._alerts.find_by_rule_id(rule.rule_id())

            if alerts:
                failed += 1

            self._print_rule(rule, alerts)

        skipped = len(self._rules.disabled())
        passed = len(self._rules) - skipped - failed
        self._system.notify(f'SUMMARY - PASS: {passed} | WARN: {failed} | SKIP: {skipped}')

    def _print_rule(self, rule: Rule, alerts: Alerts):
        self._system.notify(self._rule_info(rule, alerts))

        if alerts:
            for alert in alerts.sample(self._max_rules_to_print):
                self._system.notify(f'\t{alert.url} ({alert.response_code})')

    def _rule_info(self, rule: Rule, alerts: Alerts) -> str:
        info = f'{self._status(rule, alerts)}: {rule.name()} [{rule.rule_id()}]'

        if alerts:
            info = f'{info} x {len(alerts)}'

        return info

    def _status(self, rule: Rule, alerts: Alerts) -> str:
        if not rule.is_enabled():
            return 'SKIP'

        if alerts:
            return 'WARN'

        return 'PASS'
