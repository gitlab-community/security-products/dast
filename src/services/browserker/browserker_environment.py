from typing import Dict


class BrowserkerEnvironment:

    def __init__(self, env: Dict[str, str]):
        self.env = env

    def removeDASTConfig(self) -> Dict[str, str]:
        env = self.env.copy()
        dast_vars = [key for key in env if key.startswith('DAST_')]

        for dast_var in dast_vars:
            env.pop(dast_var)

        return env
