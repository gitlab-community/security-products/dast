from typing import Any, Dict, List


class BrowserkerScanResult:

    def __init__(self, scanned_resources: List[Dict[str, Any]], vulnerabilities: List[Dict[str, Any]]):
        self._scanned_resources = scanned_resources
        self._vulnerabilities = vulnerabilities

    @property
    def scanned_resources(self) -> List[str]:
        return self._scanned_resources

    @property
    def vulnerabilities(self) -> List[Dict[str, Any]]:
        return self._vulnerabilities
