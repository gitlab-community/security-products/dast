import re
from typing import Dict

from src import System
from src.configuration import Configuration


class RequestHeadersConfigurationBuilder:

    def __init__(self, config: Configuration):
        self._config = config

    def build(self, redacted: bool = False) -> str:
        rules = []

        for index, (name, value) in enumerate(self._headers().items()):
            if not self._config.api_specification:
                hosts = '|'.join(map(re.escape, self._config.allowed_hosts))
                regexp = rf'^(?:(?=https?:\/\/({hosts})).*).$'
                rules.extend(['-config', f'replacer.full_list({index}).url={regexp}'])

            rules.extend([
                '-config', f'replacer.full_list({index}).description=header_{index}',
                '-config', f'replacer.full_list({index}).enabled=true',
                '-config', f'replacer.full_list({index}).matchtype=REQ_HEADER',
                '-config', f'replacer.full_list({index}).matchstr={name}',
                '-config', f'replacer.full_list({index}).regex=false',
            ])

            if redacted:
                rules.extend(['-config', f'replacer.full_list({index}).replacement=********'])
            else:
                rules.extend(['-config', f'replacer.full_list({index}).replacement={value}'])

        return rules

    def _headers(self) -> Dict[str, str]:
        user_headers = self._config.request_headers or {}
        via_headers = {'Via': f'GitLab DAST/ZAP v{System().dast_version()}',
                       'Via-scanner': f'GitLab DAST v{System().dast_version()}'}
        dast_headers = via_headers if self._config.advertise_scan else {}
        return {**user_headers, **dast_headers}
