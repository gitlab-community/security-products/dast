import re
from collections import OrderedDict
from datetime import datetime
from typing import Any, Dict, List, Union

from src import Configuration, System
from src.models.http import HttpMessages

ScannedResource = Dict[str, str]


class ScanReportFormatter:

    def __init__(self, start_date_time: datetime, config: Configuration):
        self._config = config
        self._start_date_time = start_date_time

    def format_report(self,
                      scanned_resources: HttpMessages,
                      browserker_scanned_resources: List[Dict[str, Any]],
                      zap_version: str,
                      end_date_time: datetime) -> Dict[str, Any]:
        formatted = OrderedDict()
        formatted['analyzer'] = self._format_analyzer()
        formatted['end_time'] = self._format_date_time(end_date_time)
        formatted['messages'] = []
        formatted['options'] = self._format_scan_options()
        formatted['scanned_resources'] = self._format_scanned_resources(scanned_resources,
                                                                        browserker_scanned_resources)
        formatted['scanner'] = self._format_scanner(zap_version)
        formatted['start_time'] = self._format_date_time(self._start_date_time)
        formatted['status'] = 'success'
        formatted['type'] = 'dast'
        return formatted

    def _format_date_time(self, date_time: datetime) -> str:
        return date_time.strftime('%Y-%m-%dT%H:%M:%S')

    def _format_analyzer(self) -> Dict[str, Union[str, Dict[str, str]]]:
        formatted = OrderedDict()
        formatted['id'] = 'gitlab-dast'
        formatted['name'] = 'GitLab DAST'
        formatted['vendor'] = OrderedDict()
        formatted['vendor']['name'] = 'GitLab'
        formatted['version'] = System().dast_version()
        return formatted

    def _format_scan_options(self) -> list:
        scan_options = []
        scan_options.append({'name': 'auth_url', 'value': bool(self._config.auth_url)})
        scan_options.append({'name': 'full_scan', 'value': bool(self._config.full_scan)})

        if self._config.feature_flags:
            for name, value in self._config.feature_flags.items():
                normalized = re.sub(r'(?<!^)(?=[A-Z])', '_', name).lower()
                scan_options.append({'name': f'ff_{normalized}', 'value': value})

        return scan_options

    def _format_scanner(self, zap_version: str) -> Dict[str, str]:
        formatted = OrderedDict()

        if self._config.zap_enabled and self._config.browserker_scan:
            formatted['id'] = 'zaproxy-browser-based-dast'
            if self._config.feature_flags and self._config.feature_flags.get('enableBas', False):
                formatted['id'] += '-bas'
            formatted['name'] = 'OWASP Zed Attack Proxy (ZAP) and Browser-based DAST'
            formatted['url'] = 'https://www.zaproxy.org'
            formatted['version'] = zap_version

        elif self._config.browserker_scan:
            formatted['id'] = 'browser-based-dast'
            if self._config.feature_flags and self._config.feature_flags.get('enableBas', False):
                formatted['id'] += '-bas'
            formatted['name'] = 'Browser-based DAST'
            formatted['url'] = 'https://gitlab.com'
            formatted['version'] = str(self._config.dast_major_version)

        else:
            formatted['id'] = 'zaproxy'
            formatted['name'] = 'OWASP Zed Attack Proxy (ZAP)'
            formatted['url'] = 'https://www.zaproxy.org'
            formatted['version'] = zap_version

        formatted['vendor'] = OrderedDict()
        formatted['vendor']['name'] = 'GitLab'
        return formatted

    def _format_scanned_resources(self,
                                  scanned_resources: HttpMessages,
                                  browserker_scanned_resources: List[Dict[str, Any]]) -> List[ScannedResource]:

        if browserker_scanned_resources:
            scanned_resources = [self._format_scanned_resource(resource) for resource in browserker_scanned_resources]
        else:
            resources = scanned_resources.request_summaries()
            scanned_resources = [self._format_scanned_resource(resource) for resource in resources]

        sorted_scanned_resources = sorted(scanned_resources, key=lambda resource: (resource['url'], resource['method']))
        return sorted_scanned_resources

    def _format_scanned_resource(self, scanned_resource: Dict[str, str]) -> ScannedResource:
        formatted = OrderedDict()
        formatted['method'] = scanned_resource['method']
        formatted['type'] = 'url'
        formatted['url'] = scanned_resource['url']
        return formatted
