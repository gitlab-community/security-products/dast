from __future__ import annotations

from typing import Optional

from src.configuration import Configuration
from src.proxy import Proxy
from src.scan_script_wrapper import ScanScriptWrapper


class Container:
    CACHE: Optional[Container] = None

    config: Configuration
    proxy: Proxy
    scan: ScanScriptWrapper
