from __future__ import annotations

import logging
from argparse import ArgumentParser, ArgumentTypeError, Namespace, RawDescriptionHelpFormatter
from base64 import standard_b64decode
from collections import namedtuple
from os import path
from typing import Any, Callable, Dict, List, MutableMapping

from src.config.argument_type import ArgumentType
from src.models import APIScanPolicy, Target
from src.utilities import is_url
from src.zap_gateway import Settings
from .browserker_active_checks import BROWSERKER_ACTIVE_CHECKS
from .fallback_to_environment import FallbackToEnvironment
from .gitlab_exclude_rules import gitlab_exclude_rules
from .invalid_configuration_error import InvalidConfigurationError
from .url_scan_configuration_parser import URLScanConfigurationParser
from .. import Configuration
from .. import System

BrowserkerOption = namedtuple('BrowserkerOption', 'name type nargs default env_names help')


class ConfigurationParser:
    AJAX_SPIDER_COMMAND_ARG = '--use-ajax-spider'
    AJAX_SPIDER_COMMAND_ARG_ABBR = '-j'
    AJAX_SPIDER_ENV_VAR = 'DAST_USE_AJAX_SPIDER'
    AJAX_SPIDER_ZAP_ENV_VAR = 'DAST_ZAP_USE_AJAX_SPIDER'

    API_SPEC_COMMAND_ARG = '--api-specification'

    DAST_API_SCAN_SCRIPTS_DIR = '/app/resources/scripts/api_scan'
    DAST_NORMAL_SCAN_SCRIPTS_DIR = '/app/resources/scripts/normal_scan'
    DAST_PATHS_ENV_VAR = URLScanConfigurationParser.DAST_PATHS_ENV_VAR
    DAST_PATHS_FILE_ENV_VAR = URLScanConfigurationParser.DAST_PATHS_FILE_ENV_VAR
    IS_FUTURE_BUILD_FILE = '/app/building_for.future'
    PKCS12_CERT_ENV_VAR = 'DAST_PKCS12_CERTIFICATE_BASE64'
    PKCS12_PASS_ENV_VAR = 'DAST_PKCS12_PASSWORD'

    TARGET_ENV_VAR = 'DAST_WEBSITE'

    EXCLUDE_RULES_ENV_VAR = 'DAST_EXCLUDE_RULES'
    INCLUDE_RULES_ENV_VAR = 'DAST_ONLY_INCLUDE_RULES'

    def parse(self, argv: List[str], environment: MutableMapping) -> Configuration:
        dast_major_version = self.determine_dast_major_version()

        dast_api_openapi = 'DAST_API_OPENAPI'
        dast_api_specification = 'DAST_API_SPECIFICATION'

        api_spec_environment_variables = [dast_api_openapi, dast_api_specification]

        if dast_major_version >= 4:
            api_spec_environment_variables = [dast_api_specification]

        description = """
Run GitLab DAST against a target website of your choice.

Arguments can be supplied via command line and will fallback to using the outlined environment variable.
"""

        parser = ArgumentParser(description=description,
                                formatter_class=RawDescriptionHelpFormatter)

        # DAST configuration options
        parser.add_argument('-t', dest='target', action=FallbackToEnvironment, environment=environment,
                            type=self._empty_or_url,
                            environment_variables=[self.TARGET_ENV_VAR, 'DAST_TARGET_URL'],
                            help='The URL of the website to scan')

        parser.add_argument('--spider-start-at-host', dest='spider_start_at_host',
                            action=FallbackToEnvironment, environment=environment, type=self._truthy,
                            default=False,
                            environment_variables=['DAST_SPIDER_START_AT_HOST'],
                            help='Reset the target to its host before scanning')
        parser.add_argument('--paths-to-scan', dest='paths_to_scan_list', action=FallbackToEnvironment,
                            environment=environment, type=self._list_of_strings,
                            default=[],
                            environment_variables=[self.DAST_PATHS_ENV_VAR, 'DAST_TARGET_PATHS'],
                            help='A list of URL paths to scan. '
                                 'The target is prepended to each path')
        parser.add_argument('--paths-to-scan-file', dest='paths_to_scan_file_path', action=FallbackToEnvironment,
                            environment=environment,
                            environment_variables=[self.DAST_PATHS_FILE_ENV_VAR, 'DAST_TARGET_PATHS_FROM_FILE'],
                            help='The file that lists the URL paths to scan. '
                                 'The target is prepended to each path')
        parser.add_argument(self.API_SPEC_COMMAND_ARG, dest='api_specification', action=FallbackToEnvironment,
                            environment=environment,
                            environment_variables=api_spec_environment_variables,
                            help='The file or URL of the API specification'
                                 'NOTE: Use of the DAST.gitlab-ci.yml or DAST-latest.gitlab-ci.yml for API scans is '
                                 'deprecated and will no longer work in GitLab 16.0.'
                                 'Please use DAST-API.gitlab-ci.yml template and refer to '
                                 'https://docs.gitlab.com/ee/user/application_security/dast_api/#configure-dast-api'
                                 '-with-an-openapi-specification, for any configuration changes.')
        parser.add_argument('--auth-type', dest='auth_type', action=FallbackToEnvironment,
                            environment=environment,
                            environment_variables=['DAST_AUTH_TYPE'],
                            help='login authentication type to use defaults to form authentication if not set')
        parser.add_argument('--auth-url', dest='auth_url', type=ArgumentType.url, action=FallbackToEnvironment,
                            environment=environment,
                            environment_variables=['DAST_AUTH_URL'],
                            help='login form URL')
        parser.add_argument('--auth-verification-url', dest='auth_verification_url',
                            action=FallbackToEnvironment,
                            environment=environment,
                            environment_variables=['DAST_AUTH_VERIFICATION_URL', 'DAST_AUTH_SUCCESS_IF_AT_URL'],
                            type=self._empty_or_url,
                            default='',
                            help='A URL that is only accessible when the user is logged in, for verifying that '
                                 'authentication was successful. If this argument is present, the scan will '
                                 'exit if the URL cannot be accessed')
        parser.add_argument('--auth-username', dest='auth_username', action=FallbackToEnvironment,
                            environment=environment,
                            environment_variables=['DAST_USERNAME', 'DAST_AUTH_USERNAME'],
                            help='login form username')
        parser.add_argument('--auth-password', dest='auth_password',
                            action=FallbackToEnvironment,
                            environment=environment,
                            environment_variables=['DAST_PASSWORD', 'DAST_AUTH_PASSWORD'],
                            help='login form password')
        parser.add_argument('--auth-password-base64', dest='auth_password_base64',
                            type=self._base64_string,
                            action=FallbackToEnvironment,
                            environment=environment,
                            environment_variables=['DAST_PASSWORD_BASE64', 'DAST_AUTH_PASSWORD_BASE64'],
                            help='base64 encoded login form password')
        parser.add_argument('--auth-username-field', dest='auth_username_field', action=FallbackToEnvironment,
                            environment=environment,
                            environment_variables=['DAST_USERNAME_FIELD', 'DAST_AUTH_USERNAME_FIELD'],
                            help='login form id or name of the username input field')
        parser.add_argument('--auth-password-field', dest='auth_password_field', action=FallbackToEnvironment,
                            environment=environment,
                            environment_variables=['DAST_PASSWORD_FIELD', 'DAST_AUTH_PASSWORD_FIELD'],
                            help='login form id or name of the password input field')
        parser.add_argument('--auth-submit-field', dest='auth_submit_field', action=FallbackToEnvironment,
                            environment=environment,
                            environment_variables=['DAST_SUBMIT_FIELD', 'DAST_AUTH_SUBMIT_FIELD'],
                            help='login form id or name of submit input')
        parser.add_argument('--auth-first-submit-field', dest='auth_first_submit_field', action=FallbackToEnvironment,
                            environment=environment,
                            environment_variables=['DAST_FIRST_SUBMIT_FIELD', 'DAST_AUTH_FIRST_SUBMIT_FIELD'],
                            help='login form id or name of submit input of first page')
        parser.add_argument('--auth-exclude-urls', dest='exclude_urls', type=self._list_of_urls,
                            action=FallbackToEnvironment,
                            environment=environment,
                            default=[],
                            environment_variables=['DAST_EXCLUDE_URLS', 'DAST_SCOPE_EXCLUDE_URLS'],
                            help='comma separated list of URLs to exclude, no spaces in between, supply all '
                                 'URLs causing logout')
        parser.add_argument('--auth-disable-clear-fields', dest='auth_disable_clear_fields', type=bool,
                            action=FallbackToEnvironment,
                            environment=environment,
                            default=False,
                            environment_variables=['DAST_AUTH_DISABLE_CLEAR_FIELDS', 'DAST_AUTH_CLEAR_INPUT_FIELDS'],
                            help='disable clearing the username and password fields before filling out '
                                 'the login form')
        parser.add_argument('--request-headers', dest='request_headers',
                            type=self._dict_of_key_values('request headers'),
                            action=FallbackToEnvironment,
                            environment=environment,
                            default={},
                            environment_variables=['DAST_REQUEST_HEADERS'],
                            help='comma separated list of name: value request headers, '
                                 'these will be added to every request made by ZAProxy e.g. "Cache-control: no-cache"')
        parser.add_argument('--request-headers-base64', dest='request_headers_base64',
                            type=self._base64_dict_of_key_values('base64 request headers'),
                            action=FallbackToEnvironment,
                            environment=environment,
                            environment_variables=['DAST_REQUEST_HEADERS_BASE64'],
                            help='base64 encoded comma separated list of name: value request headers, '
                                 'these will be added to every request made by ZAProxy e.g. "Cache-control: no-cache"')
        parser.add_argument('--mask-http-headers', dest='http_headers_to_mask', type=self._list_of_strings,
                            action=FallbackToEnvironment,
                            environment=environment,
                            environment_variables=['DAST_MASK_HTTP_HEADERS'],
                            help='comma separated list of header names to be masked when exposed as evidence. This'
                                 ' is recommended for headers whose values may contain secrets')
        parser.add_argument('--exclude-rules', dest='user_exclude_rules', type=self._list_of_strings,
                            action=FallbackToEnvironment,
                            environment=environment,
                            default=[],
                            environment_variables=[self.EXCLUDE_RULES_ENV_VAR, 'DAST_CHECKS_TO_EXCLUDE'],
                            help='comma separated list of ZAP addon IDs to exclude from the scan')
        parser.add_argument('--only-include-rules', dest='only_include_rules', type=self._list_of_strings,
                            action=FallbackToEnvironment,
                            environment=environment,
                            default=[],
                            environment_variables=[self.INCLUDE_RULES_ENV_VAR, 'DAST_CHECKS_TO_RUN'],
                            help='comma separated list of ZAP addon IDs that will be the only rules run in the scan')
        parser.add_argument('--full-scan',
                            dest='full_scan',
                            type=self._truthy,
                            action=FallbackToEnvironment,
                            environment=environment,
                            environment_variables=['DAST_FULL_SCAN_ENABLED', 'DAST_FULL_SCAN'],
                            help='Run a ZAP Full Scan: https://github.com/zaproxy/zaproxy/wiki/ZAP-Full-Scan')
        parser.add_argument('--auto-update-addons',
                            dest='auto_update_addons',
                            type=self._truthy,
                            action=FallbackToEnvironment,
                            environment=environment,
                            environment_variables=['DAST_AUTO_UPDATE_ADDONS'],
                            help='Auto-update ZAP addons before running a scan')
        parser.add_argument('--write-addons-to-update-file',
                            dest='write_addons_to_update_file',
                            type=self._truthy,
                            action=FallbackToEnvironment,
                            environment=environment,
                            environment_variables=[],
                            help=f'Write the addons that are available for update to {Settings.WRK_DIR}addons.json')
        parser.add_argument('--availability-timeout',
                            dest='availability_timeout',
                            type=int,
                            default=60,
                            action=FallbackToEnvironment,
                            environment=environment,
                            environment_variables=['DAST_TARGET_AVAILABILITY_TIMEOUT', 'DAST_TARGET_CHECK_TIMEOUT'],
                            help='Time limit in seconds to wait for target availability')

        parser.add_argument('--pkcs12-cert-base64',
                            dest='pkcs12_cert',
                            type=self._base64_binary_data,
                            default=None,
                            action=FallbackToEnvironment,
                            environment=environment,
                            environment_variables=[self.PKCS12_CERT_ENV_VAR],
                            help='Base64 encoded contents of a PKCS12 client certificate to use for mutual TLS.')

        parser.add_argument('--pkcs12-password',
                            dest='pkcs12_password',
                            default=None,
                            action=FallbackToEnvironment,
                            environment=environment,
                            environment_variables=[self.PKCS12_PASS_ENV_VAR],
                            help='The password to access the PKCS12 client certificate.')

        parser.add_argument('--skip-target-check', dest='skip_target_check', type=self._truthy,
                            default=False,
                            action=FallbackToEnvironment,
                            environment=environment,
                            environment_variables=['DAST_SKIP_TARGET_CHECK', 'DAST_TARGET_CHECK_SKIP'],
                            help='Skip check that confirms the target is accessible')

        parser.add_argument('--script-dirs', dest='script_dirs', type=self._list_of_strings,
                            action=FallbackToEnvironment,
                            environment=environment,
                            default=[],
                            environment_variables=['DAST_SCRIPT_DIRS'],
                            help='Base directory from where custom ZAP scripts should be loaded')

        parser.add_argument('--zap-max-connection-attempts',
                            dest='zap_max_connection_attempts',
                            type=self._positive_int,
                            action=FallbackToEnvironment,
                            environment=environment,
                            environment_variables=['DAST_ZAP_MAX_CONNECTION_ATTEMPTS'],
                            help='The amount of connection attempts to make to the ZAP API before aborting')

        parser.add_argument('--zap-connect-sleep-seconds',
                            dest='zap_connect_sleep_seconds',
                            type=self._positive_int,
                            default=1,
                            action=FallbackToEnvironment,
                            environment=environment,
                            environment_variables=['DAST_ZAP_CONNECT_SLEEP_SECONDS'],
                            help='The amount of seconds to wait between each connection attempt to the ZAP API')

        parser.add_argument('--passive-scan-max-wait-time',
                            dest='passive_scan_max_wait_time',
                            type=self._positive_int,
                            action=FallbackToEnvironment,
                            environment=environment,
                            environment_variables=['DAST_PASSIVE_SCAN_MAX_WAIT_TIME'],
                            help='The amount of minutes to wait for the passive scan to complete')

        parser.add_argument('--aggregate-vulnerabilities',
                            dest='aggregate_vulnerabilities',
                            default='True',
                            type=self._truthy,
                            action=FallbackToEnvironment,
                            environment=environment,
                            environment_variables=['DAST_AGGREGATE_VULNERABILITIES'],
                            help='Group vulnerabilities where the fix is likely to occur in one or few places')
        parser.add_argument('--max-urls-per-vulerability',
                            dest='max_urls_per_vulnerability',
                            type=self._positive_int,
                            default=50,
                            action=FallbackToEnvironment,
                            environment=environment,
                            environment_variables=['DAST_MAX_URLS_PER_VULNERABILITY'],
                            help='Limit the number of URLs listed for aggregated vulnerabilities')
        parser.add_argument('--advertise-scan',
                            dest='advertise_scan',
                            type=self._truthy,
                            nargs=0,
                            default=False,
                            action=FallbackToEnvironment,
                            environment=environment,
                            environment_variables=['DAST_ADVERTISE_SCAN', 'DAST_REQUEST_ADVERTISE_SCAN'],
                            help='Send a Via-Scanner header with every request to advertise the DAST scan. '
                                 'Note that for some applications browsers will not load requests to external domains '
                                 'due to CORS rejecting requests with extra headers.')
        parser.add_argument('--allowed-hosts', dest='allowed_hosts', action=FallbackToEnvironment,
                            type=self._list_of_strings,
                            default=[],
                            environment=environment,
                            environment_variables=['DAST_ALLOWED_HOSTS', 'DAST_SCOPE_ALLOW_HOSTS'],
                            help='pages on these hosts are considered in scope when crawled')

        parser.add_argument('--ci-server-major-version',
                            dest='ci_server_major_version',
                            type=self._positive_int,
                            default=15,
                            action=FallbackToEnvironment,
                            environment=environment,
                            environment_variables=['CI_SERVER_VERSION_MAJOR'],
                            help='The major version of CI server')

        parser.add_argument('--ci-server-minor-version',
                            dest='ci_server_minor_version',
                            type=self._positive_int,
                            default=4,
                            action=FallbackToEnvironment,
                            environment=environment,
                            environment_variables=['CI_SERVER_VERSION_MINOR'],
                            help='The minor version of CI server')

        parser.add_argument('--fips-mode',
                            dest='fips_mode',
                            type=self._truthy,
                            nargs=0,
                            default=False,
                            action=FallbackToEnvironment,
                            environment=environment,
                            environment_variables=['FIPS_MODE', 'CI_GITLAB_FIPS_MODE'],
                            help='Enable FIPS mode')

        parser.add_argument('--after-login-actions',
                            dest='auth_after_login_actions',
                            type=self._list_of_strings,
                            action=FallbackToEnvironment,
                            environment=environment,
                            environment_variables=['DAST_AFTER_LOGIN_ACTIONS', 'DAST_AUTH_AFTER_LOGIN_ACTIONS'],
                            help='comma separated list of actions to perform after logging in e.g. '
                                 'click(on=css:div.input[type=submit])')

        # Browserker configuration options
        browserker_options = [
            BrowserkerOption('scan', self._truthy, 0, False, [],
                             'run a Browserker scan instead of a ZAP spider scan'),
            BrowserkerOption('allowed_hosts', self._list_of_strings, '?', [], [],
                             'pages on these hosts are considered in scope when crawled'),
            BrowserkerOption('excluded_hosts', self._list_of_strings, '?', [], ['DAST_SCOPE_EXCLUDE_HOSTS'],
                             'pages on these hosts are forcibly dropped when crawled'),
            BrowserkerOption('ignored_hosts', self._list_of_strings, '?', [], ['DAST_SCOPE_IGNORE_HOSTS'],
                             'pages on these hosts are accessed but are not reported against'),
            BrowserkerOption('excluded_elements', self._list_of_strings, '?', [], ['DAST_SCOPE_EXCLUDE_ELEMENTS'],
                             'selectors in this list are excluded from the scan. Selectors should start with css: '
                             "or xpath:, for example 'css:.navigation>*'."),
            BrowserkerOption('max_actions', int, '?', 10000, ['DAST_CRAWL_MAX_ACTIONS'],
                             'maximum number of actions to crawl in a scan'),
            BrowserkerOption('max_attack_failures', int, '?', 5, [],
                             'maximum number of errors encountered before a path is removed from testing'),
            BrowserkerOption('max_depth', int, '?', 10, ['DAST_CRAWL_MAX_DEPTH'],
                             'maximum distance of paths that are traversed'),
            BrowserkerOption('max_response_size_mb', int, '?', 0,
                             ['DAST_MAX_RESPONSE_SIZE_MB', 'DAST_PAGE_MAX_RESPONSE_SIZE_MB'],
                             'maximum size of response in MB that can be accepted'),
            BrowserkerOption('chrome_debug_log_dir', str, '?', '',
                             ['DAST_CHROME_DEBUG_LOG_DIR', 'DAST_LOG_BROWSER_OUTPUT_DIR'],
                             'the directory where chrome debug logs should be stored'),
            BrowserkerOption('number_of_browsers', int, '?', 3, ['DAST_CRAWL_WORKER_COUNT'],
                             'number of browsers to use to crawl the target site'),
            BrowserkerOption('path_to_login_form', self._list_of_strings, '?', [], ['DAST_AUTH_BEFORE_LOGIN_ACTIONS'],
                             'selectors in this list are clicked on prior to attempting to fill in the login form. '
                             "Selectors should start with css: or xpath:, for example 'css:.navigation>*'"),
            BrowserkerOption('cookies', self._dict_of_key_values('cookies'), '?', {},
                             ['DAST_REQUEST_COOKIES'],
                             'comma separated list of cookie names/values added to every request'),
            BrowserkerOption('devtools_log', self._dict_of_key_values('log', ';'), '?', {},
                             ['DAST_LOG_DEVTOOLS_CONFIG'],
                             'comma separated list of modules and intended log level'),
            BrowserkerOption('file_log', self._dict_of_key_values('file_log'), '?', {}, ['DAST_LOG_FILE_CONFIG'],
                             'comma separated list of modules and intended log level for the file log'),
            BrowserkerOption('log', self._dict_of_key_values('log'), '?', {}, ['DAST_LOG_CONFIG'],
                             'comma separated list of modules and intended log level'),
            BrowserkerOption('auth_verification_selector', str, '?', '',
                             ['DAST_AUTH_VERIFICATION_SELECTOR', 'DAST_AUTH_SUCCESS_IF_ELEMENT_FOUND'],
                             'a selector that is only accessible when the user is logged in, used to verify '
                             'authentication was successful. If this selector is not found in the page rendered after '
                             'login, the scan will exit'),
            BrowserkerOption('auth_verification_login_form', self._truthy, 0, False,
                             ['DAST_AUTH_VERIFICATION_LOGIN_FORM', 'DAST_AUTH_SUCCESS_IF_NO_LOGIN_FORM'],
                             'whether or not to search for login forms, used to verify '
                             'authentication was successful. If a login form is found in the page rendered after '
                             'login, the scan will exit'),
            BrowserkerOption('auth_report', self._truthy, 0, False, ['DAST_AUTH_REPORT'],
                             'output an html file containing '
                             'authentication details for debugging purposes'),
            BrowserkerOption('auth_cookies', self._list_of_strings, '?', [],
                             ['DAST_AUTH_COOKIES', 'DAST_AUTH_COOKIE_NAMES'],
                             'name of the cookies used by the web application for authentication'),
            BrowserkerOption('navigation_timeout', str, '?', '15s', ['DAST_PAGE_READY_AFTER_NAVIGATION_TIMEOUT'],
                             'amount of time to wait before considering a navigation or page transition failed'),
            BrowserkerOption('action_timeout', str, '?', '7s', [],
                             'amount of time to wait before failing a browser action (click, send keys, etc)'),
            BrowserkerOption('stability_timeout', str, '?', '7s', ['DAST_PAGE_READY_AFTER_ACTION_TIMEOUT'],
                             'amount of time to wait before considering the DOM ready for interaction'),
            BrowserkerOption('navigation_stability_timeout', str, '?', '6s', ['DAST_PAGE_DOM_READY_TIMEOUT'],
                             'amount of time to wait before considering the DOM ready '
                             'for interaction after a navigation'),
            BrowserkerOption('action_stability_timeout', str, '?', '800ms', [],
                             'amount of time to wait after an action before considering the page to be stable '
                             'even if the page DOM is still updating or there are pending requests'),
            BrowserkerOption('dom_ready_after_timeout', str, '?', '500ms', ['DAST_PAGE_DOM_STABLE_WAIT'],
                             'amount of time to wait for new DOM updates before considering the DOM ready for '
                             'interaction '
                             'after a browser action is executed'),
            BrowserkerOption('search_element_timeout', str, '?', '3s', ['DAST_CRAWL_SEARCH_ELEMENT_TIMEOUT'],
                             'amount of time allowed to search for elements on a page'),
            BrowserkerOption('extract_element_timeout', str, '?', '5s', [],
                             'amount of time to wait before considering the DOM ready for interaction '
                             'after a browser action is executed'),
            BrowserkerOption('element_timeout', str, '?', '300ms', ['DAST_PAGE_ELEMENT_READY_TIMEOUT'],
                             'amount of time to wait for an element to be ready for analysis'),
            BrowserkerOption('active_scan_timeout', str, '?', '3h', ['DAST_ACTIVE_SCAN_TIMEOUT'],
                             'amount of time an active scan is allowed to run'),
            BrowserkerOption('crawl_timeout', str, '?', '', ['DAST_CRAWL_TIMEOUT'],
                             'amount of time to wait for the crawl phase of the scan to complete'),
            BrowserkerOption('cache', self._truthy, 0, True, [],
                             'enable global cache of HTTP requests available to all browsers'),
            BrowserkerOption('always_relogin', self._truthy, 0, False, [],
                             'force the browser to relogin for every navigation path'),
            BrowserkerOption('log_chromium_output', self._truthy, 0, False, ['DAST_LOG_BROWSER_OUTPUT'],
                             'log the STDOUT and STDERR of chromium processes for debugging purposes'),
            BrowserkerOption('hash_attributes', self._list_of_strings, '?', [], ['DAST_PAGE_HASH_ATTRIBUTES'],
                             'allows additional attribute names to be used when hashing attributes for element '
                             'equality, which is used to determine which elements have already been interacted with'),
            BrowserkerOption('page_ready_selector', str, '?', None, ['DAST_PAGE_IS_READY_ELEMENT'],
                             'element that when visible, indicates the page has completed loading'),
            BrowserkerOption('page_loading_selector', str, '?', None, ['DAST_PAGE_IS_LOADING_ELEMENT'],
                             'element that when hidden, indicates the page has completed loading'),
            BrowserkerOption('crawl_graph', self._truthy, 0, False, ['DAST_CRAWL_GRAPH'],
                             'output an svg file containing the crawl graph'),
            BrowserkerOption('include_only_rules', self._list_of_strings, '?', [], ['DAST_CHECKS_TO_RUN'],
                             'define the rules that run when running a browser scan'),
            BrowserkerOption('file_log_path', str, '?', '', ['DAST_LOG_FILE_PATH'],
                             'the location to write the browser scan file log path'),
            BrowserkerOption('secure_report_extra_info', self._truthy, 0, False, ['DAST_SECURE_REPORT_EXTRA_INFO'],
                             'whether to print debug information to the browser-based secure report'),
            BrowserkerOption('log_request_error_report', self._truthy, 0, False, ['DAST_LOG_REQUEST_ERROR_REPORT'],
                             'whether to print error information regarding requests that failed to load correctly'),
            BrowserkerOption('db_memory_table_size', int, '?', 0, ['DAST_DB_MEMORY_TABLE_SIZE'],
                             'sets the database memory table size'),
            BrowserkerOption('active_check_workers', int, '?', None,
                             ['DAST_ACTIVE_SCAN_WORKER_COUNT'], 'number of attacks to run at a time'),
            BrowserkerOption('passive_check_workers', int, '?', None, ['DAST_PASSIVE_SCAN_WORKER_COUNT'],
                             'number of passive checks to run concurrently'),
            BrowserkerOption('scan_mode', str, '?', '', ['DAST_SCAN_MODE'],
                             'mode of the scan, active, passive, crawl, none'),
        ]

        for option in browserker_options:
            argument_names = ['browser', 'browserker'] if dast_major_version < 3 else ['browser']
            arguments = [f"--{name}-{option.name.replace('_', '-')}" for name in argument_names]
            dynamic_env_vars = [f'DAST_{name.upper()}_{option.name.upper()}' for name in argument_names]
            environment_variables = dynamic_env_vars + option.env_names

            parser.add_argument(*arguments,
                                dest=f'browserker_{option.name}',
                                type=option.type,
                                nargs=option.nargs,
                                default=option.default,
                                const=option.default,
                                action=FallbackToEnvironment,
                                environment=environment,
                                environment_variables=environment_variables,
                                help=f'{option.help}. This is an beta DAST browser crawler feature and may '
                                     'change significantly in future releases.')

        # ZAP configuration options
        parser.add_argument('-Z', dest='zap_enabled',
                            action=FallbackToEnvironment,
                            environment=environment,
                            type=self._truthy,
                            default=True,
                            environment_variables=['DAST_ZAP_ENABLED'],
                            help='Enable/disable ZAP')
        parser.add_argument('-O', dest='zap_api_host_override',
                            action=FallbackToEnvironment,
                            environment=environment,
                            environment_variables=['DAST_API_HOST_OVERRIDE'],
                            help='zap: Overrides the hostname defined in the API specification')
        parser.add_argument('-m',
                            '--spider-mins',
                            dest='spider_mins',
                            type=int,
                            action=FallbackToEnvironment,
                            environment=environment,
                            environment_variables=['DAST_SPIDER_MINS'],
                            help='the number of minutes to spider for')
        parser.add_argument('-r',
                            '--html-report',
                            dest='zap_report_html',
                            action=FallbackToEnvironment,
                            environment=environment,
                            environment_variables=['DAST_HTML_REPORT'],
                            help='file to write the full ZAP HTML report. '
                                 'This option is deprecated and will be removed in GitLab 16.0.'
                                 'You can view reports in the pipeline page in GitLab.')
        parser.add_argument('-w',
                            '--markdown-report',
                            dest='zap_report_md',
                            action=FallbackToEnvironment,
                            environment=environment,
                            environment_variables=['DAST_MARKDOWN_REPORT'],
                            help='file to write the full ZAP Markdown report. '
                                 'This option is deprecated and will be removed in GitLab 16.0.'
                                 'You can view reports in the pipeline page in GitLab.')
        parser.add_argument('-x',
                            '--xml-report',
                            dest='zap_report_xml',
                            action=FallbackToEnvironment,
                            environment=environment,
                            environment_variables=['DAST_XML_REPORT'],
                            help='file to write the full ZAP XML report. '
                                 'This option is deprecated and will be removed in GitLab 16.0.'
                                 'You can view reports in the pipeline page in GitLab.')
        parser.add_argument('-a',
                            '--include-alpha-vulnerabilities',
                            dest='zap_include_alpha',
                            type=self._truthy,
                            nargs=0,
                            action=FallbackToEnvironment,
                            environment=environment,
                            environment_variables=['DAST_INCLUDE_ALPHA_VULNERABILITIES'],
                            help='include the alpha passive and active vulnerability definitions')
        parser.add_argument('-d',
                            dest='zap_debug',
                            type=self._truthy,
                            nargs=0,
                            action=FallbackToEnvironment,
                            environment=environment,
                            environment_variables=['DAST_DEBUG'],
                            help='zap: show debug messages')
        parser.add_argument('-P',
                            dest='zap_port',
                            type=int,
                            default=System().get_free_port(),
                            help='The port used by the ZAP API to listen for requests')
        parser.add_argument('-i', dest='zap_default_info', action='store_true',
                            help='zap: default rules not in the config file to INFO')
        parser.add_argument('-I', dest='zap_no_fail_on_warn', action='store_true',
                            help='zap: do not return failure on warning')
        parser.add_argument(self.AJAX_SPIDER_COMMAND_ARG_ABBR,
                            self.AJAX_SPIDER_COMMAND_ARG,
                            dest='zap_use_ajax_spider',
                            type=self._truthy,
                            nargs=0,
                            action=FallbackToEnvironment,
                            environment=environment,
                            environment_variables=[self.AJAX_SPIDER_ENV_VAR, self.AJAX_SPIDER_ZAP_ENV_VAR],
                            help='use the AJAX spider in addition to the ZAP spider, useful for crawling '
                                 'sites that require JavaScript')
        parser.add_argument('-l', dest='zap_min_level',
                            help='zap: minimum level to show: PASS, IGNORE, INFO, WARN or FAIL, use '
                                 + 'with -s to hide example URLs')
        parser.add_argument('-z',
                            dest='zap_other_options',
                            action=FallbackToEnvironment,
                            environment=environment,
                            environment_variables=['DAST_ZAP_CLI_OPTIONS'],
                            help='zap: ZAP command line options e.g. -z"-config aaa=bbb -config ccc=ddd". '
                                 'This option is deprecated and will be removed in GitLab 16.0.')
        parser.add_argument('--zap-log-configuration',
                            dest='zap_log_configuration',
                            action=FallbackToEnvironment,
                            environment=environment,
                            environment_variables=['DAST_ZAP_LOG_CONFIGURATION'],
                            help='ZAP additional log configuration e.g.'
                                 '--zap_log_configuration "log4j.logger.org.parosproxy.paros.network.HttpSender=DEBUG".'
                                 'This option is deprecated and will be removed in GitLab 16.0.')

        values = parser.parse_args(argv)

        if values.browserker_page_loading_selector and values.browserker_page_ready_selector:
            raise InvalidConfigurationError('DAST_BROWSER_PAGE_READY_SELECTOR cannot be '
                                            'used with DAST_BROWSER_PAGE_LOADING_SELECTOR')

        if values.pkcs12_cert or values.pkcs12_password is not None:
            if not (values.pkcs12_cert and values.pkcs12_password is not None):
                raise InvalidConfigurationError(f'{self.PKCS12_CERT_ENV_VAR} and {self.PKCS12_PASS_ENV_VAR} must be '
                                                f'set to use a client certificate for mutual TLS')

        if values.target and values.api_specification and not environment.get(dast_api_openapi):
            raise InvalidConfigurationError(
                f'{dast_api_openapi} (deprecated) and {dast_api_specification} cannot be used when '
                f'{self.TARGET_ENV_VAR} is set. If you want to run an API scan, please remove {self.TARGET_ENV_VAR}',
            )

        if values.target and values.api_specification and environment.get(dast_api_openapi):
            values.api_specification = None

        values.urls_to_scan = []
        if not values.browserker_scan:
            if values.paths_to_scan_list or values.paths_to_scan_file_path:
                values.urls_to_scan = URLScanConfigurationParser(
                    values.target, values.paths_to_scan_list,
                    values.paths_to_scan_file_path, self.TARGET_ENV_VAR).parse()

        if values.zap_max_connection_attempts is None:
            values.zap_max_connection_attempts = 600

        if values.passive_scan_max_wait_time is None:
            values.passive_scan_max_wait_time = 600

        if values.auto_update_addons is None:
            values.auto_update_addons = False

        values.silent = self._is_silent(values)

        values.is_api_scan = bool(values.api_specification)

        if values.is_api_scan and values.zap_api_host_override and not is_url(values.api_specification):
            raise InvalidConfigurationError(
                f'DAST_API_HOST_OVERRIDE cannot be used when {dast_api_openapi} (deprecated) '
                f'or {dast_api_specification} is a file. If you want to use '
                f'DAST_API_HOST_OVERRIDE, please use a URL for {dast_api_openapi} or {dast_api_specification}',
            )

        values.feature_flags = self.parse_feature_flags(environment)

        dast_scripts_dir = self.DAST_API_SCAN_SCRIPTS_DIR if values.is_api_scan else self.DAST_NORMAL_SCAN_SCRIPTS_DIR
        values.script_dirs.append(dast_scripts_dir)

        if values.spider_mins is None:
            # a time of 0 represents unlimited maximum duration
            values.spider_mins = 0 if values.full_scan else 1

        # not set, use a sensible default
        if not values.http_headers_to_mask:
            values.http_headers_to_mask = ['Authorization', 'Proxy-Authorization', 'Set-Cookie', 'Cookie']

        # deliberately remove all headers
        if len(values.http_headers_to_mask) == 1 and not values.http_headers_to_mask[0]:
            values.http_headers_to_mask = []

        if not values.target and not values.api_specification and not values.write_addons_to_update_file:
            raise InvalidConfigurationError(
                f'Either {self.TARGET_ENV_VAR}, {dast_api_openapi} (deprecated), or {dast_api_specification} '
                'must be set',
            )

        if values.is_api_scan and values.zap_use_ajax_spider:
            raise InvalidConfigurationError(
                f'The AJAX Spider (configured with {self.AJAX_SPIDER_ENV_VAR}, {self.AJAX_SPIDER_ZAP_ENV_VAR}, '
                f'{self.AJAX_SPIDER_COMMAND_ARG}, or {self.AJAX_SPIDER_COMMAND_ARG_ABBR}) cannot be used with '
                f'an API scan (configured with {dast_api_openapi} (deprecated), {dast_api_specification} '
                f'or {self.API_SPEC_COMMAND_ARG})',
            )

        if values.zap_use_ajax_spider and values.browserker_scan:
            raise InvalidConfigurationError('Browserker cannot be used with an AJAX spider scan')

        if values.is_api_scan and values.browserker_scan:
            raise InvalidConfigurationError('Browserker cannot be used with an API scan')

        if values.target:
            values.allowed_hosts = [Target(values.target).hostname()] + values.allowed_hosts

        if values.browserker_scan or values.auth_url:
            values.browserker_all_allowed_hosts = list(dict.fromkeys(values.allowed_hosts +
                                                                     values.browserker_allowed_hosts +
                                                                     [Target(values.target).hostname()]))
        else:
            values.browserker_all_allowed_hosts = []

        if values.user_exclude_rules and values.only_include_rules:
            raise InvalidConfigurationError(
                f'{self.EXCLUDE_RULES_ENV_VAR} cannot be used when {self.INCLUDE_RULES_ENV_VAR} is set',
            )

        values.exclude_rules = values.user_exclude_rules + gitlab_exclude_rules()

        if values.browserker_scan:
            # These ZAP active check rules are replaced by Browserker passive check rules 352.1 and 601.1
            replaced_zap_passive_check_rules = ['20012', '20019']
            replaced_zap_active_check_rules = BROWSERKER_ACTIVE_CHECKS.replaced_zap_rules(values.feature_flags)
            values.exclude_rules += replaced_zap_passive_check_rules + replaced_zap_active_check_rules

        if values.is_api_scan and values.full_scan:
            values.exclude_rules = values.exclude_rules + APIScanPolicy.exclude_rules()

        values.dast_major_version = dast_major_version

        if values.auth_password_base64:
            values.auth_password = values.auth_password_base64

        if values.request_headers_base64:
            values.request_headers = values.request_headers_base64

        self.verify_authentication_correctly_configured(values)

        if values.is_api_scan and dast_major_version >= 4:
            raise InvalidConfigurationError(
                'DAST API scans were removed on GitLab 16.0. '
                'Please use DAST-API.gitlab-ci.yml template and refer to '
                'https://docs.gitlab.com/ee/user/application_security/dast_api/#configure-dast-api-with-an-openapi'
                '-specification, for the any configuration changes.',
            )

        if self.is_report_set(values):
            logging.warning('Use of the DAST_HTML_REPORT, DAST_XML_REPORT, and DAST_MARKDOWN_REPORT '
                            'are deprecated and will no longer work in GitLab 16.0. You can view '
                            'reports in the pipeline page in GitLab.')

        if values.feature_flags.get('enableBas', False) and dast_major_version < 4:
            logging.warning('DAST_FF_ENABLE_BAS was set to true but the current job configuration '
                            'does not include the latest Breach and Attack Simulation enabled DAST image. '
                            'Please refer to the Breach and Attack Simulation Extend DAST documentation at '
                            'https://docs.gitlab.com/ee/user/application_security/breach_and_attack_simulation/#extend-dynamic-application-security-testing-dast')  # noqa: E501

        values.schema_major_version = self.find_schema_major_version(values)

        return Configuration(values)

    @classmethod
    def _truthy(cls, value):
        return value == 'true' or value == 'True' or value == '1'

    @classmethod
    def _list_of_urls(cls, values):
        urls = [url.strip() for url in values.split(',')]
        return list(map(ArgumentType.url, urls))

    @classmethod
    def _list_of_strings(cls, values):
        return list([value.strip() for value in values.split(',')])

    @classmethod
    def _base64_string(cls, value: str) -> str:
        return standard_b64decode(value).decode('utf-8')

    @classmethod
    def _base64_binary_data(cls, value: Any) -> Any:
        return standard_b64decode(value)

    @classmethod
    def _base64_dict_of_key_values(cls, argument_name: str) -> Callable[[str], Dict[str, str]]:
        def type_parser(value: str) -> Dict[str, str]:
            decoded_value = cls._base64_string(value)

            return cls._dict_of_key_values(argument_name)(decoded_value)

        return type_parser

    @classmethod
    def _dict_of_key_values(cls, argument_name: str, split_on: str = ',') -> Callable[[str], Dict[str, str]]:
        def type_parser(value: str) -> Dict[str, str]:
            headers = [cls._dict_from_string(argument_name, name_value.strip())
                       for name_value in value.split(split_on)]
            return {name: value for header in headers for name, value in header.items()}

        return type_parser

    @classmethod
    def _dict_from_string(cls, argument_name: str, value: str) -> Dict[str, str]:
        parts = value.split(':', 1)

        if len(parts) < 2:
            raise ArgumentTypeError(f'Failed to parse {argument_name}, aborting')

        return {parts[0].strip(): parts[1].strip()}

    @classmethod
    def _is_silent(cls, values):
        if '-silent' in (values.zap_other_options or '') or values.auto_update_addons is False:
            return True

        return False

    @classmethod
    def _positive_int(cls, value):
        number = int(value)

        if number < 0:
            raise ArgumentTypeError('cannot be negative')

        return number

    @classmethod
    def _empty_or_url(cls, value: str) -> str:
        if value:
            return ArgumentType.url(value)

        return value

    def determine_dast_major_version(self) -> int:
        changelog_version = System().dast_version()

        if '.' not in changelog_version or not changelog_version.split('.')[0].isdigit():
            raise RuntimeError(f'Unable to parse CHANGELOG.md version {changelog_version}, aborting.')

        major_version = int(changelog_version.split('.')[0])

        if path.isfile(self.IS_FUTURE_BUILD_FILE):
            return major_version + 1

        return major_version

    def verify_authentication_correctly_configured(self, values: Namespace):
        if not values.auth_password and not values.auth_url and not values.auth_username:
            return

        if values.auth_password and values.auth_url and values.auth_username:
            return

        if not values.auth_url:
            raise InvalidConfigurationError('authentication misconfigured, login URL has not been set')

        if not values.auth_username:
            raise InvalidConfigurationError('authentication misconfigured, username has not been set')

        if not values.auth_password:
            raise InvalidConfigurationError('authentication misconfigured, password has not been set')

    def parse_feature_flags(self, environment) -> Dict[bool]:
        feature_flags = {}

        for name, value in environment.items():
            if name.startswith('DAST_FF_'):
                first, *rest = name.replace('DAST_FF_', '').split('_')
                name = ''.join([first.lower(), *[val.title() for val in rest]])
                feature_flags[name] = self._truthy(value)

        return feature_flags

    def find_schema_major_version(self, values):
        if (values.ci_server_major_version < 15 or
                (values.ci_server_major_version == 15 and values.ci_server_minor_version < 4)):
            return 14

        return 15

    def is_report_set(self, values):
        return values.zap_report_html or values.zap_report_xml or values.zap_report_md
