import logging

from src.config import InvalidConfigurationError
from src.models.errors import BrowserkerError, InvalidAPISpecificationError, InvalidTargetError, \
    NoUrlsError, ProxyFailedToStartError, TargetNotAccessibleError


class DASTErrorHandler:

    ERRORS_TO_PRINT_WITHOUT_STACKTRACE = [
        BrowserkerError,
        InvalidAPISpecificationError,
        InvalidConfigurationError,
        InvalidTargetError,
        NoUrlsError,
        ProxyFailedToStartError,
        TargetNotAccessibleError,
    ]

    def handle(self, error: Exception) -> None:

        if [class_name for class_name in self.ERRORS_TO_PRINT_WITHOUT_STACKTRACE if isinstance(error, class_name)]:
            logging.error(f'{type(error).__name__}: {error}')
            return

        logging.error('Unhandled exception has been thrown, aborting.', exc_info=error)
